﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 9/16/2021
 * Time: 11:20 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using PdfSharp;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using PdfSharp.Charting;
using System.IO;
using System.Xml;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace ShawIndustries
{
    /// <summary>
    /// Description of EBXExecutionMetrics.
    /// </summary>
    [TestModule("260A295D-7334-49FD-B814-040D6E49A456", ModuleType.UserCode, 1)]
    public class EBXExecutionMetrics : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public EBXExecutionMetrics()
        {
            // Do not delete - a parameterless constructor is required!
        }

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
            int Failed = 0;
			int Passed = 0;
			var TestCases = TestSuite.Current.SelectedRunConfig.GetActiveTestContainers();
	        foreach(var testcase in TestCases){
	        {if(testcase.IsTestCase){ //To Handle Smart Folders
	                if(testcase.Status.ToString()=="Failed")
	                    {
	                		Report.Log(ReportLevel.Info, "Testcase :  "+testcase.Name+ " - Failed.");
	                		Failed++;
	                    }
	                		Report.Log(ReportLevel.Info, "Testcase :  "+testcase.Name+ " - Passed.");
	                        Passed++;
	            }
	            }
	        }
	        Report.Log(ReportLevel.Info, "Total Passed Count :  "+Passed);
	        Report.Log(ReportLevel.Info, "Total Failed Count :  "+Failed);
    }
        }
    }
