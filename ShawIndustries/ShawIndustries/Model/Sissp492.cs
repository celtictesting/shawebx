﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 4/30/2021
 * Time: 1:35 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Data;
using IBM.Data.DB2;

namespace ShawIndustries.Model
{
	/// <summary>
	/// Description of Sissp492.
	/// </summary>
	public class Sissp492
	{
		public string procCall { get; set; }
		public List<DB2Parameter> parameters { get; set;}
		public Sissp492(string customerNumber)
		{
			procCall = "CALL SHXP.SISSP492(@IN_CUST_NBR, @IN_TYPE_SELECT, @IN_DATETIME, @OUT_RETURNCODE, @OUT_RETURNERAMC, @OUT_SQLCODE, @OUT_SQLERRMC)";
			parameters = new List<DB2Parameter>();
			DB2Parameter param1 = new DB2Parameter("IN_CUST_NBR", DB2Type.Char, 7);
			param1.Direction = ParameterDirection.Input;
			param1.Value = customerNumber;
			parameters.Add(param1);
			
			DB2Parameter param2 = new DB2Parameter("IN_TYPE_SELECT", DB2Type.Char, 1);
			param2.Direction = ParameterDirection.Input;
			param2.Value = "C";
			parameters.Add(param2);
			
			DB2Parameter param3 = new DB2Parameter("IN_DATETIME", DB2Type.Char, 26);
			param3.Direction = ParameterDirection.Input;
			param3.Value = "";
			parameters.Add(param3);
			
			DB2Parameter param4 = new DB2Parameter("OUT_RETURNCODE", DB2Type.Integer);
			param4.Direction = ParameterDirection.Output;
			parameters.Add(param4);
			
			
			DB2Parameter param5 = new DB2Parameter("OUT_RETURNERAMC", DB2Type.VarChar, 560);
			param5.Direction = ParameterDirection.Output;
			parameters.Add(param5);
			
			DB2Parameter param6= new DB2Parameter("OUT_SQLCODE", DB2Type.Integer);
			param6.Direction = ParameterDirection.Output;
			parameters.Add(param6);
			
			DB2Parameter param7 = new DB2Parameter("OUT_SQLERRMC", DB2Type.VarChar, 560);
			param7.Direction = ParameterDirection.Output;
			parameters.Add(param7);
		}
	}
}
