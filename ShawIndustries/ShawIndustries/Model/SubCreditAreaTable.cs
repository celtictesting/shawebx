﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 5/24/2021
 * Time: 12:34 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;

namespace ShawIndustries.Model
{
	/// <summary>
	/// Description of SubCreditAreaTable.
	/// </summary>
	public class SubCreditAreaTable : IEquatable<SubCreditAreaTable>
	{
		public string AreaNumber { get; set;}
		public string CreditAreaNumber { get; set;}
		public string SalesType { get; set;}
		public string Description { get; set;}
		public string RemitToCode { get; set;}
		public string UserCode { get; set;}
		public string ActiveIndicator { get; set;}
		public SubCreditAreaTable()
		{
		}
		public SubCreditAreaTable(string areaNumber, string creditAreaNumber, string salesType, string description, string remitToCode, string userCode, string activeIndicator)
		{
			AreaNumber = areaNumber;
			CreditAreaNumber = creditAreaNumber;
			SalesType = salesType;
			Description = description;
			RemitToCode = remitToCode;
			UserCode = userCode;
			ActiveIndicator = activeIndicator;
		}
		public bool Equals(SubCreditAreaTable other)
		{
			return this.AreaNumber == other.AreaNumber &&
				this.CreditAreaNumber == other.CreditAreaNumber &&
				this.SalesType == other.SalesType &&
				this.Description == other.Description &&
				this.RemitToCode == other.RemitToCode &&
				this.UserCode == other.UserCode &&
				this.ActiveIndicator == other.ActiveIndicator;
		}
	}
}
