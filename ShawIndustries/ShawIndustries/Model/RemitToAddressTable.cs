﻿/*
 * Created by Ranorex
 * User: fercraw
 * Date: 5/25/2021
 * Time: 3:40 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Data;
using Ranorex;

namespace ShawIndustries.Model
{
	/// <summary>
	/// Description of RemitToAddressTable.
	/// </summary>
	public class RemitToAddressTable : IEquatable<RemitToAddressTable>
	{
		public string Code { get; set; }
		public string BoxNumber { get; set; }
		public string CityStreet { get; set; }
		public RemitToAddressTable()
		{
		}
		public RemitToAddressTable(string code, string boxNumber, string cityStreet)
		{
			Code = code;
			BoxNumber = boxNumber;
			CityStreet = cityStreet;
		}
		public bool Equals(RemitToAddressTable other)
		{
			return this.Code == other.Code && 
				this.BoxNumber == other.BoxNumber && 
				this.CityStreet == other.CityStreet;
		}
		
		public List<string> InventoryTable {get; set;}
		public RemitToAddressTable(DataTable dt)
		{
			InventoryTable = new List<string>();
			foreach(DataRow row in dt.Rows)
			{
				InventoryTable.Add(row[0].ToString());
				InventoryTable.Add(row[1].ToString());
				InventoryTable.Add(row[2].ToString());
			}
        
		}
	}
}
