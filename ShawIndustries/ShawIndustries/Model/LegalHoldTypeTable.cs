﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 5/19/2021
 * Time: 2:14 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;

namespace ShawIndustries.Model
{
	/// <summary>
	/// Description of LegalHoldTypeTable.
	/// </summary>
	public class LegalHoldTypeTable : IEquatable<LegalHoldTypeTable>
	{
		public string Code { get; set;}
		public string Name { get; set;}
		public string ActiveIndicator { get; set;}
		public LegalHoldTypeTable()
		{
		}
		public LegalHoldTypeTable(string code, string name, string activeIndicator)
		{
			Code = code;
			Name = name;
			ActiveIndicator = activeIndicator;
		}
		public bool Equals(LegalHoldTypeTable other)
		{
			return this.Code == other.Code &&
				this.Name == other.Name &&
				this.ActiveIndicator == other.ActiveIndicator;
		}
	}
}
