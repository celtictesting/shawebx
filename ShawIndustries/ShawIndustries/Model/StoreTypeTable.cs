﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 5/20/2021
 * Time: 10:30 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;

namespace ShawIndustries.Model
{
	/// <summary>
	/// Description of ShopTypeTable.
	/// </summary>
	public class StoreTypeTable : IEquatable<StoreTypeTable>
	{
		public string Code { get; set;}
		public string GroupCode { get; set;}
		public string Description { get; set;}
		public string ActiveIndicator { get; set;}
		
		public StoreTypeTable()
		{
		}
		
		public StoreTypeTable(string code, string groupCode, string description, string activeIndicator)
		{
			Code = code;
			GroupCode = groupCode;
			Description = description;
			ActiveIndicator = activeIndicator;
		}
		
		public bool Equals(StoreTypeTable other)
		{
			return this.Code == other.Code &&
				this.GroupCode == other.GroupCode &&
				this.Description == other.Description &&
				this.ActiveIndicator == other.ActiveIndicator;
		}
	}
}
