﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 5/13/2021
 * Time: 3:11 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Data;

namespace ShawIndustries.Model
{
	/// <summary>
	/// Description of ContactTypeReferenceTable.
	/// </summary>
	public class ContactTypeReferenceTable
	{
		public List<string> ebxReferenceTable { get; set;}
		public ContactTypeReferenceTable(DataTable dt, string tableName)
		{
			ebxReferenceTable = new List<string>();
			foreach(DataRow row in dt.Rows)
			{
				ebxReferenceTable.Add(row[tableName + "_code"].ToString());
			}
		}
	}
}
