﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 7/14/2021
 * Time: 3:21 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;

namespace ShawIndustries.Model
{
    /// <summary>
    /// Description of BusinessSegmentTable.
    /// </summary>
  public class ClaimsAreaTable : IEquatable<ClaimsAreaTable>
	{
		public string Code { get; set; }
		public string Name { get; set; }
		public string ActiveIndicator { get; set; }
		
		
        public ClaimsAreaTable()
        {
        }

	
		public ClaimsAreaTable(string code, string name, string activeIndicator)
		{
			Code = code;
			Name = name;
			ActiveIndicator = activeIndicator;
		}
		
		public bool Equals(ClaimsAreaTable other)
		{
			return this.Code == other.Code && 
				this.Name == other.Name && 
				this.ActiveIndicator == other.ActiveIndicator;
		}
        
        
    }
}