﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 7/14/2021
 * Time: 12:41 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using Ranorex;

namespace ShawIndustries.Model
{
	/// <summary>
	/// Description of DropDownItemList.
	/// </summary>
	public class DropDownItemList
	{
		public List<string> dropDownList; 
		public DropDownItemList()
		{
		}
		public List<string> GetDropDownListItems(string dropDownName)
		{
			switch (dropDownName.ToLower())
			{
				case "pricingcountry":
					dropDownList = new List<string>() {"US - United States of America", "CA - Canada", "GB - United Kingdom"};
					break;
				
				default:
					Report.Info("Unable to find dropDown List");
					break;
			}
			return dropDownList;
		}
	}
}
