﻿/*
 * Created by Ranorex
 * User: fercraw
 * Date: 5/18/2021
 * Time: 11:38 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Data;
 
namespace ShawIndustries.Model
{
	/// <summary>
	/// Description of RemitToAddressRefTable.
	/// </summary>
	public class VariableReferenceTable
	{
		public List<string> variableRefTable {get; set;}
		
		public VariableReferenceTable(DataTable dt, string tableName, string column)
		{
			variableRefTable = new List<string>();
			
			foreach(DataRow row in dt.Rows)
			{				
				if (column == "code")
				{
					variableRefTable.Add(row[tableName + "_Code"].ToString());
				}
				else if (column == "address")
				{
					variableRefTable.Add(row[tableName + "_BoxNumber"].ToString());
					variableRefTable.Add(row[tableName + "_CityStreet"].ToString());
				}
				else if (column == "name")
				{
					variableRefTable.Add(row[tableName + "_name"].ToString());
				}
				else if (column == "id")
				{
					variableRefTable.Add(row[tableName + "_id"].ToString());
				}
				else if (column == "description")
				{
					variableRefTable.Add(row[tableName + "_description"].ToString());
				}
				else if (column == "other")
				{
					variableRefTable.Add(row[tableName + "_name"].ToString());
					variableRefTable.Add(row[tableName + "_activeIndicator"].ToString());
				}
				if (column == "home")
				{
					variableRefTable.Add(row[tableName + "_Code"].ToString() + " " + 
					                   row[tableName + "_name"].ToString() + " " + 
					                  row[tableName + "_activeIndicator"].ToString());
				}				
				
			}
		}
	}
}
