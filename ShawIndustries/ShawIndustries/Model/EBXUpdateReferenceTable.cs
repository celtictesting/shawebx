﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 7/14/2021
 * Time: 3:24 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using Ranorex;

namespace ShawIndustries.Model
{
	/// <summary>
	/// Description of CustomerTypeTable.
	/// </summary>
	///Select CustomerType_code, CustomerType_LegacyType, CustomerType_Description, CustomerType_activeIndicator from EBX_Replicated_Ref_CustomerType order by CustomerType_code

	public class EBXUpdateReferenceTable : IEquatable<EBXUpdateReferenceTable>
	{
		public string Code { get; set;}
		public string Name { get; set;}
		public string ActiveIndicator { get; set;}
		public string EBXType { get; set;}	
		
		public EBXUpdateReferenceTable()
		{
		}
		public EBXUpdateReferenceTable(string code, string name, string activeIndicator, string ebxType)
		{
			Code = code;
			Name = name;
			ActiveIndicator = activeIndicator;
			EBXType = ebxType;
		}
		
		public bool Equals(EBXUpdateReferenceTable other)
		{
			return this.Code == other.Code &&
				this.Name == other.Name &&
				this.ActiveIndicator == other.ActiveIndicator &&
				this.EBXType == other.EBXType;
		}
	}
}
