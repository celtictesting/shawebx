﻿/*
 * Created by Ranorex
 * User: fercraw
 * Date: 5/26/2021
 * Time: 3:23 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;

namespace ShawIndustries.Model
{
	/// <summary>
	/// Description of HomeCenterTable.
	/// </summary>
	public class HomeCenterTable : IEquatable<HomeCenterTable>
	{
		public string Code { get; set; }
		public string Name { get; set; }
		public string ActiveIndicator { get; set; }
		
		public HomeCenterTable()
		{
		}
		
		public HomeCenterTable(string code, string name, string activeIndicator)
		{
			Code = code;
			Name = name;
			ActiveIndicator = activeIndicator;
		}
		
		public bool Equals(HomeCenterTable other)
		{
			return this.Code == other.Code && 
				this.Name == other.Name && 
				this.ActiveIndicator == other.ActiveIndicator;
		}
	}
}
