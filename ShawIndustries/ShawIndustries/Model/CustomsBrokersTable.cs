﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 5/19/2021
 * Time: 12:48 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;

namespace ShawIndustries.Model
{
	/// <summary>
	/// Description of CustomsBrokersTable.
	/// </summary>
	public class CustomsBrokersTable : IEquatable<CustomsBrokersTable>
	{
		public string Code { get; set;}
		public string Type { get; set;}
		public string Name { get; set;}
		public string ActiveIndicator { get; set;}
		public CustomsBrokersTable()
		{
		}
		public CustomsBrokersTable(string code, string type, string name, string activeIndicator)
		{
			Code = code;
			Type = type;
			Name = name;
			ActiveIndicator = activeIndicator;
		}
		public bool Equals(CustomsBrokersTable other)
		{
			return this.Code == other.Code &&
				this.Type == other.Type &&
				this.Name == other.Name &&
				this.ActiveIndicator == other.ActiveIndicator;
		}
	}
}
