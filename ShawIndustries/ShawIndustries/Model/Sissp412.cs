﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 4/28/2021
 * Time: 9:17 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Data;
using IBM.Data.DB2;

namespace ShawIndustries.Model
{
	/// <summary>
	/// Description of Sissp412.
	/// </summary>
	public class Sissp412
	{
		public string procCall { get; set; }
		public List<DB2Parameter> parameters { get; set;}
		public Sissp412(string customerNumber, string contactGroupCD)
		{
			procCall = "CALL SHXP.SISSP412(@IN_CUST_NBR, @IN_TYPE_SELECT, @IN_CNTCT_GRP_CD, @IN_CNTCT_TYP_CD, @IN_DATETIME, @OUT_RETURNCODE, @OUT_RETURNERAMC, @OUT_SQLCODE, @OUT_SQLERRMC)";
			parameters = new List<DB2Parameter>();
			DB2Parameter param1 = new DB2Parameter("IN_CUST_NBR", DB2Type.Char, 7);
			param1.Direction = ParameterDirection.Input;
			param1.Value = customerNumber;
			parameters.Add(param1);
			
			DB2Parameter param2 = new DB2Parameter("IN_TYPE_SELECT", DB2Type.Char, 1);
			param2.Direction = ParameterDirection.Input;
			param2.Value = "C";
			parameters.Add(param2);
			
			DB2Parameter param3 = new DB2Parameter("IN_CNTCT_GRP_CD", DB2Type.Char, 10);
			param3.Direction = ParameterDirection.Input;
			param3.Value = contactGroupCD;
			parameters.Add(param3);
			
			DB2Parameter param4 = new DB2Parameter("IN_CNTCT_TYP_CD", DB2Type.Char, 10);
			param4.Direction = ParameterDirection.Input;
			param4.Value = "";
			parameters.Add(param4);
			
			DB2Parameter param5 = new DB2Parameter("IN_DATETIME", DB2Type.Char, 26);
			param5.Direction = ParameterDirection.Input;
			param5.Value = "";
			parameters.Add(param5);
			
			DB2Parameter param6 = new DB2Parameter("OUT_RETURNCODE", DB2Type.Integer);
			param6.Direction = ParameterDirection.Output;
			parameters.Add(param6);
			
			
			DB2Parameter param7 = new DB2Parameter("OUT_RETURNERAMC", DB2Type.VarChar, 560);
			param7.Direction = ParameterDirection.Output;
			parameters.Add(param7);
			
			DB2Parameter param8= new DB2Parameter("OUT_SQLCODE", DB2Type.Integer);
			param8.Direction = ParameterDirection.Output;
			parameters.Add(param8);
			
			DB2Parameter param9 = new DB2Parameter("OUT_SQLERRMC", DB2Type.VarChar, 560);
			param9.Direction = ParameterDirection.Output;
			parameters.Add(param9);
		}
	}
}
