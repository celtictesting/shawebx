﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/19/2022
 * Time: 4:28 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Data;

namespace ShawIndustries.Model
{
    /// <summary>
    /// Description of SellCoReferenceTable.
    /// </summary>

    public class SellCoReferenceTable : IEquatable<SellCoReferenceTable>
    {
    	public string Selco {get; set;}
    	
        public SellCoReferenceTable()
        {
        }
 
        public SellCoReferenceTable(string selco)
        {
        	Selco = selco;
        }
        
        public bool Equals(SellCoReferenceTable other)
        {
			return this.Selco == other.Selco;
			
        }
        	
   		public List<string> InventoryTable {get; set;}
   		public SellCoReferenceTable(DataTable dt)
   		{
   		InventoryTable = new List<string>();
			foreach(DataRow row in dt.Rows)
			{
				InventoryTable.Add(row[0].ToString());
			}
        }
    }
}