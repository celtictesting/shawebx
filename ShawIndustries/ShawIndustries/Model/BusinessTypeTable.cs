﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 7/14/2021
 * Time: 3:20 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;

namespace ShawIndustries.Model
{
    /// <summary>
    /// Description of BusinessSegmentTable.
    /// </summary>
  public class BusinessTypeTable : IEquatable<BusinessTypeTable>
	{
		public string Code { get; set; }
		public string Name { get; set; }
		
		
        public BusinessTypeTable()
        {
        }

	
		public BusinessTypeTable(string code, string name)
		{
			Code = code;
			Name = name;
		}
		
		public bool Equals(BusinessTypeTable other)
		{
			return this.Code == other.Code && 
				this.Name==other.Name;
		}
        
        
    }
}