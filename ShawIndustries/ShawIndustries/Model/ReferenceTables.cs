﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 5/10/2021
 * Time: 10:29 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Data;

namespace ShawIndustries.Model
{
	/// <summary>
	/// Description of ReferenceTables.
	/// </summary>
	public class ReferenceTables
	{
		public List<string> ebxReferenceTable { get; set;}
		public ReferenceTables(DataTable dt)
		{
			ebxReferenceTable = new List<string>();
			foreach(DataRow row in dt.Rows)
			{
				ebxReferenceTable.Add(row[0].ToString());
			}
		}
		public ReferenceTables(DataTable dt, string tableName)
		{
			ebxReferenceTable = new List<string>();
			foreach(DataRow row in dt.Rows)
			{
				ebxReferenceTable.Add(row[tableName + "_code"].ToString() + " - " + row[tableName + "_name"].ToString());
			}
		}
	}
}
