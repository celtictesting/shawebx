﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 10/27/2021
 * Time: 2:47 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Data;

using Ranorex;

namespace ShawIndustries.Model
{
    /// <summary>
    /// Description of TerritoryData.
    /// </summary>
          /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public class TerritoryData
        {
   		public List<string> InventoryTable { get; set;}
			public TerritoryData(DataTable dt)
	
        {
			InventoryTable = new List<string>();
			foreach(DataRow row in dt.Rows)
			{
				InventoryTable.Add(row[0].ToString());
				InventoryTable.Add(row[1].ToString());
			}
        }
    }
}
