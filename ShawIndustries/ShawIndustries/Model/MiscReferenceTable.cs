﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 7/14/2021
 * Time: 3:27 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Data;

namespace ShawIndustries.Model
{
    /// <summary>
    /// Description of MiscReferenceTable.
    /// </summary>

    public class MiscReferenceTable 
	{
		public List<string> ebxReferenceTable { get; set;}
		public MiscReferenceTable(DataTable dt, string tableName)
		{
			ebxReferenceTable = new List<string>();
			foreach(DataRow row in dt.Rows)
			{
				ebxReferenceTable.Add(row[tableName + "_code"].ToString() + "-" + row[tableName + "_name"].ToString());
			}
		}
	}
}