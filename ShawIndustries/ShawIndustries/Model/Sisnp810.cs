﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 4/27/2021
 * Time: 9:30 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Data;
using IBM.Data.DB2;

namespace ShawIndustries.Model
{
	/// <summary>
	/// Description of Sisnp810.
	/// </summary>
	public class Sisnp810
	{
		public string procCall { get; set; }
		public List<DB2Parameter> parameters { get; set;}
		public Sisnp810(string customerNumber)
		{
				procCall = "CALL SHXP.SISNP810(@IN_CUST_NBR, @OUT_RETURNCODE, @OUT_RETURNERAMC, @OUT_SQLCODE, @OUT_SQLERRMC)";
				parameters = new List<DB2Parameter>();
    			DB2Parameter param1 = new DB2Parameter("IN_CUST_NBR", DB2Type.Char, 7);
    			param1.Direction = ParameterDirection.Input;
    			param1.Value = customerNumber;
    			parameters.Add(param1);
    			
    			DB2Parameter param2 = new DB2Parameter("OUT_RETURNCODE", DB2Type.Integer);
    			param2.Direction = ParameterDirection.Output;
    			parameters.Add(param2);
    			
    			
    			DB2Parameter param3 = new DB2Parameter("OUT_RETURNERAMC", DB2Type.VarChar, 560);
    			param3.Direction = ParameterDirection.Output;
    			parameters.Add(param3);
    			
    			DB2Parameter param4 = new DB2Parameter("OUT_SQLCODE", DB2Type.Integer);
    			param4.Direction = ParameterDirection.Output;
    			parameters.Add(param4);
    			
    			DB2Parameter param5 = new DB2Parameter("OUT_SQLERRMC", DB2Type.VarChar, 560);
    			param5.Direction = ParameterDirection.Output;
    			parameters.Add(param5);			
		}
	}
}
