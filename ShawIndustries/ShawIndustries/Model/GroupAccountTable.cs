﻿/*
 * Created by Ranorex
 * User: fercraw
 * Date: 5/27/2021
 * Time: 11:11 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;

namespace ShawIndustries.Model
{
	/// <summary>
	/// Description of GroupAccountTable.
	/// </summary>
	public class GroupAccountTable : IEquatable<GroupAccountTable>
	{
		public string Name { get; set; }
		public string Id { get; set; }
		public string Description { get; set; }
		
		public GroupAccountTable()
		{			
		}
		
		public GroupAccountTable(string name, string id, string description)
		{
			Name = name;
			Id = id;
			Description = description;
		}
		
		public bool Equals(GroupAccountTable other)
		{
			return this.Name == other.Name && 
				this.Id == other.Id && 
				this.Description == other.Description;
		}
	}
}
