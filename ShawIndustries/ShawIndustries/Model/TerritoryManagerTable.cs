﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 7/21/2021
 * Time: 2:05 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;

namespace ShawIndustries.Model
{
	/// <summary>
	/// Description of TerritoryManagerTable.
	/// </summary>
	public class TerritoryManagerTable : IEquatable<BusinessTypeTable>
	{
		public string Code { get; set; }
		public string Name { get; set; }
		
		public TerritoryManagerTable()
		{
		}
		public TerritoryManagerTable(string code, string name)
		{
			Code = code;
			Name = name;
		}
		public bool Equals(BusinessTypeTable other)
		{
			return this.Code == other.Code && 
				this.Name==other.Name;
		}
	}
}
