﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 2/8/2022
 * Time: 10:39 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */

using System;
using System.Collections.Generic;
using System.Data;
using Ranorex;

namespace ShawIndustries.Model
{
    /// <summary>
    /// Description of SellingCompany.
    /// </summary>
          /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public class SellingCompany
        {
    		public List<string> InventoryTable { get; set;}
			public SellingCompany(DataTable dt)
	
        {
			InventoryTable = new List<string>();
			foreach(DataRow row in dt.Rows)
			{
				InventoryTable.Add(row[0].ToString());
			}
        }
    }
}
