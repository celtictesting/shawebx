﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 5/18/2021
 * Time: 1:41 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using Ranorex;

namespace ShawIndustries.Model
{
	/// <summary>
	/// Description of CustomerTypeTable.
	/// </summary>
	///Select CustomerType_code, CustomerType_LegacyType, CustomerType_Description, CustomerType_activeIndicator from EBX_Replicated_Ref_CustomerType order by CustomerType_code

	public class CustomerTypeTable : IEquatable<CustomerTypeTable>
	{
		public string Code { get; set;}
		public string LegacyType { get; set;}
		public string Description { get; set;}
		public string ActiveIndicator { get; set;}
		public CustomerTypeTable()
		{
		}
		public CustomerTypeTable(string code, string legacyType, string description, string activeIndicator)
		{
			Code = code;
			LegacyType = legacyType;
			Description = description;
			ActiveIndicator = activeIndicator;
		}
		
		public bool Equals(CustomerTypeTable other)
		{
			return this.Code == other.Code &&
				this.LegacyType == other.LegacyType &&
				this.Description == other.Description &&
				this.ActiveIndicator == other.ActiveIndicator;
		}
	}
}
