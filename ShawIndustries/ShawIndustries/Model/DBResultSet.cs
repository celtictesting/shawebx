﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 4/28/2021
 * Time: 10:36 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Data;
using IBM.Data.DB2;

namespace ShawIndustries.Model
{
	/// <summary>
	/// Description of Class1.
	/// </summary>
	public class DBResultSet
	{
		
		public List<DB2Parameter> Parameters { get; set;}
		public DataTable DBDataTable { get; set; }
		public DBResultSet()
		{
		}
		public DBResultSet(List<DB2Parameter> parameters, DataTable dbDataTable)
		{
			Parameters = parameters;
			DBDataTable = dbDataTable;
		}
	}
}
