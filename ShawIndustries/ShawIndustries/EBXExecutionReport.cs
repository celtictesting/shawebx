﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 8/31/2021
 * Time: 5:15 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using PdfSharp;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using PdfSharp.Charting;
using System.IO;
using System.Xml;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace ShawIndustries
{
    /// <summary>
    /// Description of EBXExecutionReport.
    /// </summary>
    [TestModule("DC7C7899-A70B-47F0-A47E-0B5CDD4A922F", ModuleType.UserCode, 1)]
    public class EBXExecutionReport : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public EBXExecutionReport()
        {
            // Do not delete - a parameterless constructor is required!
        }

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
            
         	Ranorex.Report.Setup(ReportLevel.Info,@"{0}\{1}.rxzlog",false, true);
            XmlLogger.AppendExisting = true;
            
            var testSuiteCompletedTime = System.DateTime.Now;
            var currentTestSuiteName = TestSuite.Current.Name;  
            var currentTestSuiteFolder = TestSuite.Current.CurrentTestContainer.Name;

            string CurrDir = System.IO.Directory.GetCurrentDirectory();        
    		string ProjPath = System.IO.Directory.GetParent(CurrDir).Parent.FullName;            
            System.IO.File.WriteAllText(@ProjPath + "_EBXExecutionReport.txt",currentTestSuiteName + Environment.NewLine + currentTestSuiteFolder + Environment.NewLine + testSuiteCompletedTime + Environment.NewLine + "------------------------" + Environment.NewLine);
            
			var list = new List<string>{"CreateRegularCustomer","CreateInternalCustomer","NegativeTestingCreateCustomer"};
            
             int index = list.Count;
    
             for (int i = 0; i<index; i++)
                     {

              	var MyTestName = TestSuite.Current.GetTestContainer(list[i]).Name;
             	var MyTestStatus = TestSuite.Current.GetTestContainer(list[i]).Status.ToString();


            	Report.Log(ReportLevel.Info, "Execution Info", MyTestName + " - " + MyTestStatus);
            	System.IO.File.AppendAllText(@ProjPath + "_EBXExecutionReport.txt", MyTestName +  "  -  " + MyTestStatus + Environment.NewLine);
             }
             
         string line;
         System.IO.TextReader readfile = new StreamReader (ProjPath + "_EBXExecutionReport.txt");
         int yPoint = 0;
         int lineCount = 1;
         
         PdfDocument Mypdf = new PdfDocument();
         PdfPage pdfpage = Mypdf.AddPage();
         XGraphics graph = XGraphics.FromPdfPage(pdfpage);
         XFont font = new XFont("Arial", 11, XFontStyle.Regular);

     
         while ((line = readfile.ReadLine()) !=null)

          		{
               		graph.DrawString(line, font, XBrushes.Black, new XRect(40, yPoint, pdfpage.Width.Point, pdfpage.Height.Point), XStringFormats.TopLeft);
                    yPoint = yPoint + 15;
                    lineCount++;
                }

         string pdfFilename = CurrDir + "\\Reports\\EBXExecutionReports_" + System.DateTime.UtcNow.ToString("yyyyMMddHHmmss") + ".pdf";
                   
                 
                    Mypdf.Save(pdfFilename);
                    readfile.Close();
                    readfile = null; 
            
                    Report.LogHtml(    ReportLevel.Success,
                    "ExecutivePDFReport",
                    string.Format(
                        "Successfully created PDF Report: <a href= '{0}' >Open EBX Execution PDF</a>",
                        pdfFilename));

             
         Ranorex.Report.End();
         Ranorex.Report.Start();
            
        }
    }
}
