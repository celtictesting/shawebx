﻿/*
 * Created by Ranorex
 * User: joecatr
 * Date: 4/1/2021
 * Time: 2:23 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace ShawIndustries.UserCodeCollection
{
    /// <summary>
    /// Description of RandomDates.
    /// </summary>
    [TestModule("38698823-5169-458C-BDB1-082E1420B55E", ModuleType.UserCode, 1)]
    public class RandomDates : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public RandomDates()
        {
            // Do not delete - a parameterless constructor is required!
        }

string _RandomDate = "";
[TestVariable("be9713b0-af4f-44af-b0c9-1b0cac7027ad")]
public string RandomDate
{
	get { return _RandomDate; }
	set { _RandomDate = value; }
}

string _RandomPre1700Date = "";
[TestVariable("e872b2d2-98de-4b55-af63-4c30f51b84c4")]
public string RandomPre1700Date
{
	get { return _RandomPre1700Date; }
	set { _RandomPre1700Date = value; }
}

string _RandomFutureDate = "";
[TestVariable("edffa84e-e631-4b0e-a299-9aa17562a4c3")]
public string RandomFutureDate
{
	get { return _RandomFutureDate; }
	set { _RandomFutureDate = value; }
}

string _Month = "";
[TestVariable("f7278566-7aae-4a06-894b-03d0e0ec3017")]
public string Month
{
	get { return _Month; }
	set { _Month = value; }
}

string _Day = "";
[TestVariable("0b26a9d8-296d-4dbf-b885-fa5f0e3df06f")]
public string Day
{
	get { return _Day; }
	set { _Day = value; }
}

string _RandomYear = "";
[TestVariable("b379e9c2-865b-4b37-9e5f-2294286d8b6d")]
public string RandomYear
{
	get { return _RandomYear; }
	set { _RandomYear = value; }
}

string _RandomPastDate = "";
[TestVariable("19afb48b-b0fa-4f9f-8406-9499e1165360")]
public string RandomPastDate
{
	get { return _RandomPastDate; }
	set { _RandomPastDate = value; }
}

string _PastYear = "";
[TestVariable("dbead352-691b-4eb1-b2e9-d65010895639")]
public string PastYear
{
	get { return _PastYear; }
	set { _PastYear = value; }
}

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
        	
        	string RandMonth = new Random().Next(01,12).ToString();
        	string RandDay = new Random().Next(01,28).ToString();
        	string RandYear = new Random().Next(1,9999).ToString();
        	string Pre1700 = new Random().Next(1,1699).ToString();
            string DateTime = System.DateTime.Today.ToString("yyyy");
            string BaseFutureDate = new Random().Next(1,100).ToString();
            string FutureDate = System.DateTime.Today.AddYears(5).ToString("yyyy");
            string PastDate = new Random().Next(2000,2020).ToString();
            string PreYear = System.DateTime.Today.AddYears(-3).ToString("yyyy");
            
            
            RandomDate = (RandMonth+"/"+RandDay+"/"+RandYear);
            RandomPre1700Date = Pre1700;
            RandomFutureDate = FutureDate;
            RandomPastDate = PastDate;
            Month = RandMonth;
            Day = RandDay;
            PastYear = PreYear;  
             
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
        }
        
//        public void RandomDates
//        {
//        	
//        }
    }
}
