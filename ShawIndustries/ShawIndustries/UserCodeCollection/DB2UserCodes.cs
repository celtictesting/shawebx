﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 4/27/2021
 * Time: 9:38 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;
using IBM.Data.DB2;
using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using ShawIndustries.Model;
using ShawIndustries.Service;

namespace ShawIndustries.UserCodeCollection
{
    /// <summary>
    /// Creates a Ranorex user code collection. A collection is used to publish user code methods to the user code library.
    /// </summary>
    [UserCodeCollection]
    public class DB2UserCodes
    {
    	
    	/// <summary>
    	/// Store procedure Sisnp810
    	/// </summary>
    	[UserCodeMethod]
    	public static string Sisnp810StoreProc(string customerNumber)
    	{
    		Sisnp810 sisnp810 = new Sisnp810(customerNumber);
    		DBConnector dbConnector = new DBConnector();
    		string dbConnectStatus = "";
    		DBResultSet dbResultSet = dbConnector.ConnectToDataBase(sisnp810.parameters, sisnp810.procCall);
    		foreach(DataRow row in dbResultSet.DBDataTable.Rows)
    		{
    			foreach(DataColumn dc in dbResultSet.DBDataTable.Columns)
    			{
    				Report.Info(dc.ToString() + " = " +row[dc].ToString());
    			}
    		}
    		foreach(DB2Parameter dbParameter in dbResultSet.Parameters)
    		{
    			if (dbParameter.ToString() == "OUT_SQLERRMC")
    			{
    				dbConnectStatus = dbParameter.Value.ToString(); 
    			}
    		}
    		return dbConnectStatus;
    	}
    	/// <summary>
    	/// Store procedure Sisnp895
    	/// </summary>
    	[UserCodeMethod]
    	public static string Sisnp895StoreProc(string customerNumber)
    	{
    		Sisnp895 sisnp895 = new Sisnp895(customerNumber);
    		DBConnector dbConnector = new DBConnector();
    		string dbConnectStatus = "";
    		DBResultSet dbResultSet = dbConnector.ConnectToDataBase(sisnp895.parameters, sisnp895.procCall);
    		foreach(DataRow row in dbResultSet.DBDataTable.Rows)
    		{
    			foreach(DataColumn dc in dbResultSet.DBDataTable.Columns)
    			{
    				Report.Info(dc.ToString() + " = " +row[dc].ToString());
    			}
    		}
    		foreach(DB2Parameter dbParameter in dbResultSet.Parameters)
    		{
    			if (dbParameter.ToString() == "OUT_SQLERRMC")
    			{
    				dbConnectStatus = dbParameter.Value.ToString(); 
    			}
    		}
    		return dbConnectStatus;
    	}
    	
    	/// <summary>
    	/// Store procedure Sissp401
    	/// </summary>
    	[UserCodeMethod]
    	public static string Sissp401StoreProc(string customerNumber)
    	{
    		Sissp401 sissp401 = new Sissp401(customerNumber);
    		DBConnector dbConnector = new DBConnector();
    		DBResultSet dbResultSet = dbConnector.ConnectToDataBase(sissp401.parameters, sissp401.procCall);
    		string dbConnectStatus = "";
    		foreach(DataRow row in dbResultSet.DBDataTable.Rows)
    		{
    			foreach(DataColumn dc in dbResultSet.DBDataTable.Columns)
    			{
    				Report.Info(dc.ToString() + " = " +row[dc].ToString());
    			}
    		}
    		foreach(DB2Parameter dbParameter in dbResultSet.Parameters)
    		{
    			Report.Info(dbParameter.ToString() + " = " + dbParameter.Value.ToString());
    			if (dbParameter.ToString() == "IN_CUST_NBR")
    			{
    				dbConnectStatus = dbParameter.Value.ToString(); 
    			}
    		}
    		return dbConnectStatus;
    	}
    	
    	/// <summary>
    	/// Store procedure Sissp492
    	/// </summary>
    	[UserCodeMethod]
    	public static string Sissp492StoreProc(string customerNumber)
    	{
    		Sissp492 sissp492 = new Sissp492(customerNumber);
    		DBConnector dbConnector = new DBConnector();
    		DBResultSet dbResultSet = dbConnector.ConnectToDataBase(sissp492.parameters, sissp492.procCall);
    		string dbConnectStatus = "";
    		foreach(DataRow row in dbResultSet.DBDataTable.Rows)
    		{
    			foreach(DataColumn dc in dbResultSet.DBDataTable.Columns)
    			{
    				Report.Info(dc.ToString() + " = " +row[dc].ToString());
    			}
    		}
    		foreach(DB2Parameter dbParameter in dbResultSet.Parameters)
    		{
    			Report.Info(dbParameter.ToString() + " = " + dbParameter.Value.ToString());
    			if (dbParameter.ToString() == "OUT_RETURNERAMC")
    			{
    				dbConnectStatus = dbParameter.Value.ToString(); 
    			}
    		}
    		return dbConnectStatus;
    	}
    	
    	/// <summary>
    	/// Sissp412 Stored Procedure
    	/// </summary>
    	[UserCodeMethod]
    	public static string Sissp412StoreProc(string customerNumber, string contactGroupCD)
    	{
    		Sissp412 sissp412 = new Sissp412(customerNumber, contactGroupCD);
    		DBConnector dbConnector = new DBConnector();
    		DBResultSet dbResultSet = dbConnector.ConnectToDataBase(sissp412.parameters, sissp412.procCall);
    		string dbConnectStatus = "";
    		foreach(DataRow row in dbResultSet.DBDataTable.Rows)
    		{
    			foreach(DataColumn dc in dbResultSet.DBDataTable.Columns)
    			{
    				Report.Info(dc.ToString() + " = " +row[dc].ToString());
    			}
    		}
    		foreach(DB2Parameter dbParameter in dbResultSet.Parameters)
    		{
    			Report.Info(dbParameter.ToString() + " = " + dbParameter.Value.ToString());
    			if (dbParameter.ToString() == "IN_CUST_NBR")
    			{
    				dbConnectStatus = dbParameter.Value.ToString(); 
    			}
    		}
    		return dbConnectStatus;
    	}
    }
}
