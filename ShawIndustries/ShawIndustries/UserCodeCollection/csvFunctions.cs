﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 11/30/2021
 * Time: 10:34 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;
using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Reporting;
using Ranorex.Core.Testing;
using ShawIndustries.Model;

namespace ShawIndustries.UserCodeCollection
{
    /// <summary>
    /// Creates a Ranorex user code collection. A collection is used to publish user code methods to the user code library.
    /// </summary>
    [UserCodeCollection]
    public class csvFunctions
    {
        // You can use the "Insert New User Code Method" functionality from the context menu,
        // to add a new method with the attribute [UserCodeMethod].
        /// <summary>
    	/// add Item to csv dataTable
    	/// </summary>
    	[UserCodeMethod]
    	public static void AddItemtoList(string style, string colorNumber)
    	{
    		CsvDataTable.csvDataTable.Rows.Add(style, colorNumber);
    	}
    	
    	/// <summary>
    	/// Convert Datatable to csv
    	/// </summary>
    	[UserCodeMethod]
    	public static void ConvertDTtoCvs()
    	{
    		StringBuilder sb = new StringBuilder();
    		string[] columnNames = CsvDataTable.csvDataTable.Columns.Cast<DataColumn>().
        		Select(column => column.ColumnName).
        		ToArray();
        	sb.AppendLine(string.Join(",", columnNames));

        	foreach (DataRow row in CsvDataTable.csvDataTable.Rows)
        	{
        		string[] fields = row.ItemArray.Select(field => field.ToString()).
        			ToArray();
        		sb.AppendLine(string.Join(",", fields));
        	}
			Report.Link("Test Results CSV Sheet", TestReport.ReportEnvironment.ReportFileDirectory + "\\" + TestReport.ReportEnvironment.ReportName + ".csv"); 
        	File.WriteAllText(TestReport.ReportEnvironment.ReportFileDirectory + "\\" + TestReport.ReportEnvironment.ReportName + ".csv", sb.ToString());
    	}
    }
}
