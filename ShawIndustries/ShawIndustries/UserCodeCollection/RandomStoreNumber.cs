﻿/*
 * Created by Ranorex
 * User: joecatr
 * Date: 4/6/2021
 * Time: 2:52 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace ShawIndustries.UserCodeCollection
{
	/// <summary>
	/// Description of RandomStoreNumber.
	/// </summary>
	[TestModule("36D12E7C-87FF-491B-AD9B-CC0AF9C1F5E2", ModuleType.UserCode, 1)]
	public class RandomStoreNumber : ITestModule
	{
		/// <summary>
		/// Constructs a new instance.
		/// </summary>
		public RandomStoreNumber()
		{
			// Do not delete - a parameterless constructor is required!
		}

		string _StoreNum = "";
		[TestVariable("ea620368-4800-4b27-aace-b6ac83ec4b20")]
		public string StoreNum
		{
			get { return _StoreNum; }
			set { _StoreNum = value; }
		}

		/// <summary>
		/// Performs the playback of actions in this module.
		/// </summary>
		/// <remarks>You should not call this method directly, instead pass the module
		/// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
		/// that will in turn invoke this method.</remarks>
		void ITestModule.Run()
		{
			Mouse.DefaultMoveTime = 300;
			Keyboard.DefaultKeyPressTime = 100;
			Delay.SpeedFactor = 1.0;
			
			string RandStoreNum = new Random().Next(100,99999).ToString();
			
			StoreNum = RandStoreNum;
			
			
			
		}
	}
}
