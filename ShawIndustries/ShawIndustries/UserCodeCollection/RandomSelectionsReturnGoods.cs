﻿/*
 * Created by Ranorex
 * User: joecatr
 * Date: 4/15/2021
 * Time: 2:01 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;


using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace ShawIndustries.UserCodeCollection
{
    /// <summary>
    /// Description of RandomSelectionsReturnGoods.
    /// </summary>
    [TestModule("1B57B773-B32F-4F83-8FF6-869BD8CDE17F", ModuleType.UserCode, 1)]
    public class RandomSelectionsReturnGoods : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public RandomSelectionsReturnGoods()
        {
            // Do not delete - a parameterless constructor is required!
        }

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
           
        }
    }
}
