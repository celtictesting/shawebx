﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 5/17/2021
 * Time: 2:47 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;
using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;
using ShawIndustries.Model;
using ShawIndustries.Service;

namespace ShawIndustries.UserCodeCollection
{
    /// <summary>
    /// ReferenceTable Codes
    /// </summary>
    [UserCodeCollection]
    public class SQLReferecneTableCodes
    {
    	// You can use the "Insert New User Code Method" functionality from the context menu,
    	// to add a new method with the attribute [UserCodeMethod].
    	/// <summary>
    	/// Get Customer Type table to table and compare it to dropdown menu
    	/// </summary>
   	[UserCodeMethod]
    	public static void getCustomerTypeTableDB(RepoItemInfo divtagInfo, string tableName)
    	{
    		DBConnector dbConnector = new DBConnector();
    		string query = "Select EBX_Replicated_Ref_CustomerType.CustomerType_code, EBX_Replicated_Ref_CustomerType.CustomerType_name from EBX_Replicated_Ref_CustomerType join EBX_Replicated_Ref_CustomerType_CustomerType_ebxCustomerType on EBX_Replicated_Ref_CustomerType.CustomerType_code = EBX_Replicated_Ref_CustomerType_CustomerType_ebxCustomerType.CustomerType_code where EBX_Replicated_Ref_CustomerType_CustomerType_ebxCustomerType.CustomerType_ebxCustomerType_ = 'REGULAR'";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		ContactTypeReferenceTable contactTypeReferenceTables = new ContactTypeReferenceTable(dt, tableName);
    		IList<DivTag> dropDownList = divtagInfo.CreateAdapters<DivTag>();
    		string optionText = "";
    		foreach (DivTag option in dropDownList)
    		{
    			//divtagInfo.FindAdapter<DivTag>().Element.GetAttributeValueText("InnerText");
    			optionText = option.Element.GetAttributeValueText("InnerText");
    			optionText =  Regex.Replace(optionText, ((char)160).ToString(), " ");
    			if(contactTypeReferenceTables.ebxReferenceTable.Contains(optionText))
    			{
    				Report.Log(ReportLevel.Info, "Validation", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo'.");
    				contactTypeReferenceTables.ebxReferenceTable.Remove(optionText);
    			}else
    			{
    				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo', not matched.");
    			}
    		}
    		if(contactTypeReferenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in contactTypeReferenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}
    	/// <summary>
    	/// Get All Customer Type table to table and compare it to Table
    	/// </summary>
    	[UserCodeMethod]
    	public static void getAllCustomerTypeTableDB(RepoItemInfo codeCell, RepoItemInfo nameCell, RepoItemInfo activeCell, RepoItemInfo ebxCell)
    	{
    		DBConnector dbConnector = new DBConnector();
    		string tableName = "CustomerType";
    		
    		string query = "Select EBX_Replicated_Ref_CustomerType.CustomerType_code, EBX_Replicated_Ref_CustomerType.CustomerType_name, EBX_Replicated_Ref_CustomerType.CustomerType_activeIndicator, EBX_Replicated_Ref_CustomerType_CustomerType_ebxCustomerType.CustomerType_ebxCustomerType_  from EBX_Replicated_Ref_CustomerType join EBX_Replicated_Ref_CustomerType_CustomerType_ebxCustomerType on EBX_Replicated_Ref_CustomerType.CustomerType_code = EBX_Replicated_Ref_CustomerType_CustomerType_ebxCustomerType.CustomerType_code";
    		Report.Info("SQL Query:  " + query);
    		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		List<EBXUpdateReferenceTable> dBContactTypeList = new List<EBXUpdateReferenceTable>();
    		
    		foreach(DataRow row in dt.Rows)
			{
    			EBXUpdateReferenceTable customerTypeTable = new EBXUpdateReferenceTable(row[tableName + "_Code"].ToString(),
    			                                                                        row[tableName + "_Name"].ToString(),
    			                                                                        row[tableName + "_ActiveIndicator"].ToString(), 
    			                                                                        row[tableName + "_EBXCustomerType_"].ToString());
				dBContactTypeList.Add(customerTypeTable);
			}
    		IList<TdTag> codeColumn = codeCell.CreateAdapters<TdTag>();
    		IList<DivTag> nameColumn = nameCell.CreateAdapters<DivTag>();
    		IList<TdTag> activeColumn = activeCell.CreateAdapters<TdTag>();
    		IList<TdTag> ebxColumn = ebxCell.CreateAdapters<TdTag>();
    		
    		int i = 0;
    		string codeText = "";
    		string nameText = "";
    		string activeText = "";
    		string ebxText = "";
    		
    		foreach (TdTag option in codeColumn)
    		{
    			codeText = option.Element.GetAttributeValueText("InnerText");
    			codeText =  Regex.Replace(codeText, ((char)160).ToString(), " ");
    			Report.Info("Row " + i + ": codeText: " + codeText);
    			
    			nameText = nameColumn[i].Element.GetAttributeValueText("InnerText");
    			nameText =  Regex.Replace(nameText, ((char)160).ToString(), " ");
    			Report.Info("Row " + i + ": nameText: " + nameText);    			
    			
    			ebxText = ebxColumn[i].Element.GetAttributeValueText("InnerText");
    			ebxText =  Regex.Replace(ebxText, ((char)160).ToString(), " ");
    			Report.Info("Row " + i + ": ebxText: " + ebxText);    			
    			
    			activeText = activeColumn[i].Element.GetAttributeValueText("InnerText");
    			activeText =  Regex.Replace(activeText, ((char)160).ToString(), " ");
    			if(activeText =="Yes")
    			{
    				activeText="Y";
    			}else
    			{
    				activeText="N";
    			}
    			
    			EBXUpdateReferenceTable customerType = new EBXUpdateReferenceTable(codeText, nameText, activeText, ebxText);
    			if(dBContactTypeList.Contains(customerType))
    			{
    				Report.Log(ReportLevel.Info, "Validation", "Validating Drop Down Item Match Reference Table (Text="+customerType.Code+" " + customerType.Name + ") on item 'divtagInfo'.");
    				dBContactTypeList.Remove(customerType);
    			}else
    			{
    				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+customerType.Code+" " + customerType.Name +") on item 'divtagInfo', not matched.");
    			}
    			i++;
    		}
    		if(dBContactTypeList.Count > 0)
    		{
    			foreach(EBXUpdateReferenceTable extra in dBContactTypeList)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra.Code+" " + extra.Name+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}
    	/// <summary>
    	/// Get All Customs Broker table to table and compare it to Table
    	/// </summary>
    	[UserCodeMethod]
    	public static void getAllCustomsBrokerTableDB(RepoItemInfo tdtagCodeInfo, RepoItemInfo tdtagVaribleColumInfo, string tableName)
    	{
    		var repo = ShawIndustriesRepository.Instance;

    		DBConnector dbConnector = new DBConnector();
    		string query = "Select " + tableName + "_code, " + tableName + "_type, " + tableName + "_name, " + tableName + "_activeIndicator from EBX_Replicated_Ref_" + tableName + " order by " + tableName + "_code";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		List<CustomsBrokersTable> dBCustomsBrokerTypeList = new List<CustomsBrokersTable>();
    		foreach(DataRow row in dt.Rows)
			{
    			CustomsBrokersTable customsBrokersTable = new CustomsBrokersTable(row[tableName + "_code"].ToString(), row[tableName + "_type"].ToString(), row[tableName + "_name"].ToString(), row[tableName + "_activeIndicator"].ToString());
				dBCustomsBrokerTypeList.Add(customsBrokersTable);
			}
    		IList<TdTag> codeColumn = tdtagCodeInfo.CreateAdapters<TdTag>();
    		repo.TableColumnNumber = "1";
    		IList<TdTag> typeColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    		repo.TableColumnNumber = "2";
    		IList<TdTag> nameColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    		repo.TableColumnNumber = "3";
    		IList<TdTag> activeColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    		int i = 0;
    		string codeText = "";
    		string typeText = "";
    		string nameText = "";
    		string activeIndicator = "";
    		foreach (TdTag option in codeColumn)
    		{
    			codeText = option.Element.GetAttributeValueText("InnerText");
    			codeText =  Regex.Replace(codeText, ((char)160).ToString(), " ");
    			typeText = typeColumn[i].Element.GetAttributeValueText("InnerText");
    			typeText =  Regex.Replace(typeText, ((char)160).ToString(), " ");
    			nameText = nameColumn[i].Element.GetAttributeValueText("InnerText");
    			nameText =  Regex.Replace(nameText, ((char)160).ToString(), " ");
    			activeIndicator = activeColumn[i].Element.GetAttributeValueText("InnerText");
    			activeIndicator =  Regex.Replace(activeIndicator, ((char)160).ToString(), " ");
    			if(activeIndicator =="Yes")
    			{
    				activeIndicator="1";
    			}else
    			{
    				activeIndicator="0";
    			}
    			
    			CustomsBrokersTable customsBrokersTable = new CustomsBrokersTable(codeText, typeText, nameText, activeIndicator);
    			if(dBCustomsBrokerTypeList.Contains(customsBrokersTable))
    			{
    				Report.Log(ReportLevel.Info, "Validation", "Validating Drop Down Item Match Reference Table (Text="+customsBrokersTable.Code+") on item 'divtagInfo'.");
    				dBCustomsBrokerTypeList.Remove(customsBrokersTable);
    			}else
    			{
    				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+customsBrokersTable.Code+customsBrokersTable.Name+customsBrokersTable.Type+customsBrokersTable.ActiveIndicator+") on item 'divtagInfo', not matched.");
    			}
    			i++;
    		}
    		if(dBCustomsBrokerTypeList.Count > 0)
    		{
    			foreach(CustomsBrokersTable extra in dBCustomsBrokerTypeList)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra.Code+extra.Name+extra.Type+extra.ActiveIndicator+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}
    	/// <summary>
    	/// Get All Legal Hold type table to table and compare it to Table
    	/// </summary>
    	[UserCodeMethod]
    	public static void getAllLegalHoldTypeTableDB(RepoItemInfo tdtagCodeInfo, RepoItemInfo tdtagVaribleColumInfo, string tableName)
    	{
    		var repo = ShawIndustriesRepository.Instance;

    		DBConnector dbConnector = new DBConnector();
    		string query = "Select " + tableName + "_code, " + tableName + "_name, " + tableName + "_activeIndicator from EBX_Replicated_Ref_" + tableName + " order by " + tableName + "_code";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		List<LegalHoldTypeTable> dBLegalHoldTypeList = new List<LegalHoldTypeTable>();
    		foreach(DataRow row in dt.Rows)
			{
    			LegalHoldTypeTable legalHoldTypeTable = new LegalHoldTypeTable(row[tableName + "_code"].ToString(), row[tableName + "_name"].ToString(), row[tableName + "_activeIndicator"].ToString());
				dBLegalHoldTypeList.Add(legalHoldTypeTable);
			}
    		IList<TdTag> codeColumn = tdtagCodeInfo.CreateAdapters<TdTag>();
    		repo.TableColumnNumber = "1";
    		IList<TdTag> nameColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    		repo.TableColumnNumber = "2";
    		IList<TdTag> activeColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    		int i = 0;
    		string codeText = "";
    		string nameText = "";
    		string activeIndicator = "";
    		foreach (TdTag option in codeColumn)
    		{
    			codeText = option.Element.GetAttributeValueText("InnerText");
    			codeText =  Regex.Replace(codeText, ((char)160).ToString(), " ");
    			nameText = nameColumn[i].Element.GetAttributeValueText("InnerText");
    			nameText =  Regex.Replace(nameText, ((char)160).ToString(), " ");
    			activeIndicator = activeColumn[i].Element.GetAttributeValueText("InnerText");
    			activeIndicator =  Regex.Replace(activeIndicator, ((char)160).ToString(), " ");
    			if(activeIndicator =="Yes")
    			{
    				activeIndicator="1";
    			}else
    			{
    				activeIndicator="0";
    			}
    			
    			LegalHoldTypeTable legalHoldType = new LegalHoldTypeTable(codeText, nameText, activeIndicator);
    			if(dBLegalHoldTypeList.Contains(legalHoldType))
    			{
    				Report.Success("Validation", "Validating Column Item Match Reference Table (Text="+legalHoldType.Code+ " " + legalHoldType.Name + " " + legalHoldType.ActiveIndicator + ") on item 'divtagInfo'.");
    				dBLegalHoldTypeList.Remove(legalHoldType);
    			}else
    			{
    				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+legalHoldType.Code+legalHoldType.Name+legalHoldType.ActiveIndicator+") on item 'divtagInfo', not matched.");
    			}
    			i++;
    		}
    		if(dBLegalHoldTypeList.Count > 0)
    		{
    			foreach(LegalHoldTypeTable extra in dBLegalHoldTypeList)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra.Code+extra.Name+extra.ActiveIndicator+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}

  	[UserCodeMethod]
    	public static void getAllShipCodeTableDB(RepoItemInfo tdtagCodeInfo, RepoItemInfo tdtagVaribleColumInfo, string tableName)
    	{
    		var repo = ShawIndustriesRepository.Instance;

    		DBConnector dbConnector = new DBConnector();
    		string query = "Select ShipCode_code, ShipCode_name, ShipCode_activeIndicator from EBX_Replicated_Ref_ShipCode where ShipCode_activeIndicator = '1'";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		List<LegalHoldTypeTable> dBLegalHoldTypeList = new List<LegalHoldTypeTable>();
    		foreach(DataRow row in dt.Rows)
			{
    			LegalHoldTypeTable legalHoldTypeTable = new LegalHoldTypeTable(row[tableName + "_code"].ToString(), row[tableName + "_name"].ToString(), row[tableName + "_activeIndicator"].ToString());
				dBLegalHoldTypeList.Add(legalHoldTypeTable);
			}
    		IList<TdTag> codeColumn = tdtagCodeInfo.CreateAdapters<TdTag>();
    		repo.TableColumnNumber = "1";
    		IList<TdTag> nameColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    		repo.TableColumnNumber = "2";
    		IList<TdTag> activeColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    		int i = 0;
    		string codeText = "";
    		string nameText = "";
    		string activeIndicator = "";
    		foreach (TdTag option in codeColumn)
    		{
    			codeText = option.Element.GetAttributeValueText("InnerText");
    			codeText =  Regex.Replace(codeText, ((char)160).ToString(), " ");
    			nameText = nameColumn[i].Element.GetAttributeValueText("InnerText");
    			nameText =  Regex.Replace(nameText, ((char)160).ToString(), " ");
    			activeIndicator = activeColumn[i].Element.GetAttributeValueText("InnerText");
    			activeIndicator =  Regex.Replace(activeIndicator, ((char)160).ToString(), " ");
    			if(activeIndicator =="Yes")
    			{
    				activeIndicator="1";
    			}else
    			{
    				activeIndicator="0";
    			}
    			
    			LegalHoldTypeTable legalHoldType = new LegalHoldTypeTable(codeText, nameText, activeIndicator);
    			if(dBLegalHoldTypeList.Contains(legalHoldType))
    			{
    				Report.Success("Validation", "Validating Column Item Match Reference Table (Text= "+legalHoldType.Code+ " " + legalHoldType.Name + ").  Item matched.");
    				dBLegalHoldTypeList.Remove(legalHoldType);
    			}else
    			{
    				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+legalHoldType.Code+legalHoldType.Name+legalHoldType.ActiveIndicator+") on item 'divtagInfo', not matched.");
    			}
    			i++;
    		}
    		if(dBLegalHoldTypeList.Count > 0)
    		{
    			foreach(LegalHoldTypeTable extra in dBLegalHoldTypeList)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra.Code+extra.Name+extra.ActiveIndicator+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}
    	/// <summary>
    	/// Get All Store type table to table and compare it to Table
    	/// </summary>
    	[UserCodeMethod]
    	public static void getAllShopTypeTableDB(RepoItemInfo tdtagCodeInfo, RepoItemInfo tdtagVaribleColumInfo, RepoItemInfo buttonNextInfo, string tableName)
    	{
    		Delay.Duration(2000, false);
    		var repo = ShawIndustriesRepository.Instance;

    		DBConnector dbConnector = new DBConnector();
    		string query = "Select " + tableName + "_Code, " + tableName + "_storeTypeGroupCode, " + tableName + "_Description, " + tableName + "_ActiveIndicator from EBX_Replicated_Ref_" + tableName + " order by " + tableName + "_Code";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		List<StoreTypeTable> dBStoreTypeList = new List<StoreTypeTable>();
    		foreach(DataRow row in dt.Rows)
			{
    			StoreTypeTable storeTypeTable = new StoreTypeTable(row[tableName + "_Code"].ToString(), row[tableName + "_storeTypeGroupCode"].ToString(), row[tableName + "_Description"].ToString(), row[tableName + "_ActiveIndicator"].ToString());
				dBStoreTypeList.Add(storeTypeTable);
			}
    		
    		string codeText = "";
    		string storeTypeGroupCode = "";
    		string description = "";
    		string activeIndicator = "";
    		bool isNextPage = true;
    		while(isNextPage)
    		{
    			Report.Log(ReportLevel.Info, "Data", "Validate Table");
    			IList<TdTag> codeColumn = tdtagCodeInfo.CreateAdapters<TdTag>();
    			repo.TableColumnNumber = "1";
    			IList<TdTag> storeTypeGroupCodeColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    			repo.TableColumnNumber = "2";
    			IList<TdTag> descriptionColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    			repo.TableColumnNumber = "3";
    			IList<TdTag> activeColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    			int i = 0;
    			i=0;
    			foreach (TdTag option in codeColumn)
    			{
    				//divtagInfo.FindAdapter<DivTag>().Element.GetAttributeValueText("InnerText");
    				codeText = option.Element.GetAttributeValueText("InnerText");
    				codeText =  Regex.Replace(codeText, ((char)160).ToString(), " ");
    				storeTypeGroupCode = storeTypeGroupCodeColumn[i].Element.GetAttributeValueText("InnerText");
    				storeTypeGroupCode =  Regex.Replace(storeTypeGroupCode, ((char)160).ToString(), " ");
    				description = descriptionColumn[i].Element.GetAttributeValueText("InnerText");
    				description =  Regex.Replace(description, ((char)160).ToString(), " ");
    				activeIndicator = activeColumn[i].Element.GetAttributeValueText("InnerText");
    				activeIndicator =  Regex.Replace(activeIndicator, ((char)160).ToString(), " ");
    				if(activeIndicator =="Yes")
    				{
    					activeIndicator="Y";
    				}else
    				{
    					activeIndicator="N";
    				}
    				
    				StoreTypeTable storeTypeTable = new StoreTypeTable(codeText, storeTypeGroupCode, description, activeIndicator);
    				if(dBStoreTypeList.Contains(storeTypeTable))
    				{
    					Report.Success("Validation", "Validating Column Item Match Reference Table (Text="+storeTypeTable.Code+ " " + storeTypeTable.GroupCode + " " + storeTypeTable.Description + " " + storeTypeTable.ActiveIndicator + ") on item 'divtagInfo'.");
    					dBStoreTypeList.Remove(storeTypeTable);
    				}else
    				{
    					Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+storeTypeTable.Code+storeTypeTable.GroupCode+storeTypeTable.Description+storeTypeTable.ActiveIndicator+") on item 'divtagInfo', not matched.");
    				}
    				i++;
    			}
    			isNextPage = bool.Parse(buttonNextInfo.FindAdapter<Unknown>().Element.GetAttributeValueText("Enabled"));
    			if(isNextPage)
    			{
    				buttonNextInfo.FindAdapter<Unknown>().Click();
    				Report.Log(ReportLevel.Info, "Delay", "Waiting for 5s.");
    				Delay.Duration(5000, false);
    			}
    		}
    		if(dBStoreTypeList.Count > 0)
    		{
    			foreach(StoreTypeTable extra in dBStoreTypeList)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra.Code+extra.GroupCode+extra.Description+extra.ActiveIndicator+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}
    	/// <summary>
    	/// Get All Store type table to table and compare it to Table
    	/// </summary>
    	[UserCodeMethod]
    	public static void getAllSubCreditAreaTableDB(RepoItemInfo tdtagCodeInfo, RepoItemInfo tdtagVaribleColumInfo, RepoItemInfo buttonNextInfo, string tableName)
    	{
    		Delay.Duration(2000, false);
    		var repo = ShawIndustriesRepository.Instance;

    		DBConnector dbConnector = new DBConnector();
    		string query = "Select SubCreditArea_AreaNumber, SubCreditArea_CreditAreaNumber, SubCreditArea_SalesType, SubCreditArea_Description, SubCreditArea_RemitToCode, SubCreditArea_UserCode, SubCreditArea_ActiveIndicator from EBX_Replicated_Ref_SubCreditArea ";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		List<SubCreditAreaTable> dBSubCreditAreaList = new List<SubCreditAreaTable>();
    		foreach(DataRow row in dt.Rows)
			{
    			SubCreditAreaTable subCreditAreaTable = new SubCreditAreaTable(row[tableName + "_AreaNumber"].ToString(), row[tableName + "_CreditAreaNumber"].ToString(), row[tableName + "_SalesType"].ToString(),
    			                                                               row[tableName + "_Description"].ToString(), row[tableName + "_RemitToCode"].ToString(), row[tableName + "_UserCode"].ToString(), row[tableName + "_ActiveIndicator"].ToString());
				dBSubCreditAreaList.Add(subCreditAreaTable);
			}
    		
    		string areaNumber = "";
    		string creditAreaNumber = "";
    		string salesType = "";
    		string description = "";
    		string remitToCode = "";
    		string userCode = "";
    		string activeIndicator = "";
    		bool isNextPage = true;
    		while(isNextPage)
    		{
    			
    			Report.Log(ReportLevel.Info, "Data", "Validate Table");
    			IList<TdTag> areaNumberColumn = tdtagCodeInfo.CreateAdapters<TdTag>();
    			repo.TableColumnNumber = "1";
    			IList<TdTag> creditAreaNumberColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    			repo.TableColumnNumber = "2";
    			IList<TdTag> salesTypeColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    			repo.TableColumnNumber = "3";
    			IList<TdTag> descriptionColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    			repo.TableColumnNumber = "4";
    			IList<TdTag> remitToCodeColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    			repo.TableColumnNumber = "5";
    			IList<TdTag> userCodeColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    			repo.TableColumnNumber = "6";
    			IList<TdTag> activeColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    			int i = 0;
    			i=0;
    			foreach (TdTag option in areaNumberColumn)
    			{
    				areaNumber = option.Element.GetAttributeValueText("InnerText");
    				areaNumber =  Regex.Replace(areaNumber, ((char)160).ToString(), " ");
    				creditAreaNumber = creditAreaNumberColumn[i].Element.GetAttributeValueText("InnerText");
    				creditAreaNumber =  Regex.Replace(creditAreaNumber, ((char)160).ToString(), " ");
    				salesType = salesTypeColumn[i].Element.GetAttributeValueText("InnerText");
    				salesType =  Regex.Replace(salesType, ((char)160).ToString(), " ");
    				description = descriptionColumn[i].Element.GetAttributeValueText("InnerText");
    				description =  Regex.Replace(description, ((char)160).ToString(), " ");
    				remitToCode = remitToCodeColumn[i].Element.GetAttributeValueText("InnerText");
    				remitToCode =  Regex.Replace(remitToCode, ((char)160).ToString(), " ");
    				userCode = userCodeColumn[i].Element.GetAttributeValueText("InnerText");
    				userCode =  Regex.Replace(userCode, ((char)160).ToString(), " ");
    				activeIndicator = activeColumn[i].Element.GetAttributeValueText("InnerText");
    				activeIndicator =  Regex.Replace(activeIndicator, ((char)160).ToString(), " ");
    				if(activeIndicator =="Yes")
    				{
    					activeIndicator="Y";
    				}else
    				{
    					activeIndicator="N";
    				}
    				
    				SubCreditAreaTable subCreditAreaTable = new SubCreditAreaTable(areaNumber, creditAreaNumber, salesType, description, remitToCode, userCode, activeIndicator);
    				if(dBSubCreditAreaList.Contains(subCreditAreaTable))
    				{
    					Report.Success("Validation", "Validating Column Item Match Reference Table (Text="+subCreditAreaTable.AreaNumber+ " " + subCreditAreaTable.CreditAreaNumber + " " + subCreditAreaTable.SalesType + " " 
    					               + subCreditAreaTable.Description + " " + subCreditAreaTable.RemitToCode + " " + subCreditAreaTable.UserCode + " " + subCreditAreaTable.ActiveIndicator + ") on item 'divtagInfo'.");
    					dBSubCreditAreaList.Remove(subCreditAreaTable);
    				}else
    				{
    					Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+subCreditAreaTable.AreaNumber+subCreditAreaTable.CreditAreaNumber+subCreditAreaTable.SalesType+subCreditAreaTable.Description+subCreditAreaTable.RemitToCode+subCreditAreaTable.UserCode+subCreditAreaTable.ActiveIndicator+") on item 'divtagInfo', not matched.");
    				}
    				i++;
    			}
    			isNextPage = bool.Parse(buttonNextInfo.FindAdapter<Unknown>().Element.GetAttributeValueText("Enabled"));
    			if(isNextPage)
    			{
    				buttonNextInfo.FindAdapter<Unknown>().Click();
    				Report.Log(ReportLevel.Info, "Delay", "Waiting for 5s.");
    				Delay.Duration(5000, false);
    			}
    		}
    		if(dBSubCreditAreaList.Count > 0)
    		{
    			foreach(SubCreditAreaTable extra in dBSubCreditAreaList)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra.AreaNumber+extra.CreditAreaNumber+extra.SalesType+extra.Description+extra.RemitToCode+extra.UserCode+extra.ActiveIndicator+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}
    	/// <summary>
    	/// Get All Ref Title table to table and compare it to Table
    	/// </summary>
    	[UserCodeMethod]
    	public static void getAllRefTitleTableDB(RepoItemInfo tdtagCodeInfo, RepoItemInfo tdtagVaribleColumInfo, RepoItemInfo buttonNextInfo, string tableName)
    	{
    		var repo = ShawIndustriesRepository.Instance;

    		DBConnector dbConnector = new DBConnector();
    		string query = "Select " + tableName + "_type, " + tableName + "_code, " + tableName + "_name, " + tableName + "_activeIndicator from EBX_Replicated_Ref_" + tableName + " where " + tableName + "_type ='CONTACT'";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
     		List<CustomsBrokersTable> dBCustomsBrokerTypeList = new List<CustomsBrokersTable>();
    		foreach(DataRow row in dt.Rows)
    		{
    			CustomsBrokersTable customsBrokersTable = new CustomsBrokersTable(row[tableName + "_code"].ToString(), row[tableName + "_type"].ToString(), row[tableName + "_name"].ToString(), row[tableName + "_activeIndicator"].ToString());
    			dBCustomsBrokerTypeList.Add(customsBrokersTable);
    		}

    		string typeText = "";
    		string codeText = "";
    		string nameText = "";
    		string activeIndicator = "";
    		bool isNextPage = true;
    		while(isNextPage)
    		{
    			Report.Log(ReportLevel.Info, "Data", "Validate Table");
    			repo.TableColumnNumber = "2";  
    			IList<TdTag> typeColumn = tdtagCodeInfo.CreateAdapters<TdTag>();
    			repo.TableColumnNumber = "3";    			
    			IList<TdTag> codeColumn = tdtagCodeInfo.CreateAdapters<TdTag>();
    			repo.TableColumnNumber = "1";
    			IList<TdTag> nameColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    			repo.TableColumnNumber = "2";
    			IList<TdTag> activeColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    			int i = 0;
    			i=0;
    			foreach (TdTag option in typeColumn)
    			{
    				typeText = option.Element.GetAttributeValueText("InnerText");
    				typeText =  Regex.Replace(typeText, ((char)160).ToString(), " ");
    				codeText = codeColumn[i].Element.GetAttributeValueText("InnerText");
    				codeText =  Regex.Replace(codeText, ((char)160).ToString(), " ");
    				nameText = nameColumn[i].Element.GetAttributeValueText("InnerText");
    				nameText =  Regex.Replace(nameText, ((char)160).ToString(), " ");
    				activeIndicator = activeColumn[i].Element.GetAttributeValueText("InnerText");
    				activeIndicator =  Regex.Replace(activeIndicator, ((char)160).ToString(), " ");
    				if(activeIndicator =="Yes")
    				{
    					activeIndicator="1";
    				}else
    				{
    					activeIndicator="0";
    				}
    				
    				CustomsBrokersTable customsBrokersTable = new CustomsBrokersTable(codeText, typeText, nameText, activeIndicator);
    				if(dBCustomsBrokerTypeList.Contains(customsBrokersTable))
    				{
    					Report.Success("Validation", "Database information matches reference table (Text= "+customsBrokersTable.Type+ " - "+customsBrokersTable.Code+ " - "+customsBrokersTable.Name+").");
    					Report.Info("Info", "DB Count: " + dBCustomsBrokerTypeList.Count);
    					dBCustomsBrokerTypeList.Remove(customsBrokersTable);
    				}else
    				{
    					Report.Failure("Data", "Database information DOES NOT match reference table (Text= "+customsBrokersTable.Type+ " - "+customsBrokersTable.Code+ " - "+ customsBrokersTable.Name +").");
    				}
    				i++;
    			}
    			isNextPage = bool.Parse(buttonNextInfo.FindAdapter<Unknown>().Element.GetAttributeValueText("Enabled"));
    			if(isNextPage)
    			{
    				buttonNextInfo.FindAdapter<Unknown>().Click();
    				Report.Log(ReportLevel.Info, "Delay", "Waiting for 5s.");
    				Delay.Duration(5000, false);
    			}
    		}
    		if(dBCustomsBrokerTypeList.Count > 0)
    		{
    			foreach(CustomsBrokersTable extra in dBCustomsBrokerTypeList)
    			{
    				Report.Info("Data", "Items in DB but not in the Reference Table (Text="+extra.Type+" - "+extra.Code+" - "+extra.Name+").");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}  
  
   	[UserCodeMethod]
    	public static void getPrincipalRefTitleTableDB(RepoItemInfo tdtagCodeInfo, RepoItemInfo tdtagVaribleColumInfo, RepoItemInfo buttonNextInfo, string tableName)
    	{
    		var repo = ShawIndustriesRepository.Instance;

    		DBConnector dbConnector = new DBConnector();
    		string query = "Select " + tableName + "_type, " + tableName + "_code, " + tableName + "_name, " + tableName + "_activeIndicator from EBX_Replicated_Ref_" + tableName + " where " + tableName + "_type ='PRINCIPAL'";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
     		List<CustomsBrokersTable> dBCustomsBrokerTypeList = new List<CustomsBrokersTable>();
    		foreach(DataRow row in dt.Rows)
    		{
    			CustomsBrokersTable customsBrokersTable = new CustomsBrokersTable(row[tableName + "_code"].ToString(), row[tableName + "_type"].ToString(), row[tableName + "_name"].ToString(), row[tableName + "_activeIndicator"].ToString());
    			dBCustomsBrokerTypeList.Add(customsBrokersTable);
    		}

    		string typeText = "";
    		string codeText = "";
    		string nameText = "";
    		string activeIndicator = "";
    		bool isNextPage = true;
    		while(isNextPage)
    		{
    			Report.Log(ReportLevel.Info, "Data", "Validate Table");
    			repo.TableColumnNumber = "2";  
    			IList<TdTag> typeColumn = tdtagCodeInfo.CreateAdapters<TdTag>();
    			repo.TableColumnNumber = "3";    			
    			IList<TdTag> codeColumn = tdtagCodeInfo.CreateAdapters<TdTag>();
    			repo.TableColumnNumber = "1";
    			IList<TdTag> nameColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    			repo.TableColumnNumber = "2";
    			IList<TdTag> activeColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    			int i = 0;
    			i=0;
    			foreach (TdTag option in typeColumn)
    			{
    				typeText = option.Element.GetAttributeValueText("InnerText");
    				typeText =  Regex.Replace(typeText, ((char)160).ToString(), " ");
    				codeText = codeColumn[i].Element.GetAttributeValueText("InnerText");
    				codeText =  Regex.Replace(codeText, ((char)160).ToString(), " ");
    				nameText = nameColumn[i].Element.GetAttributeValueText("InnerText");
    				nameText =  Regex.Replace(nameText, ((char)160).ToString(), " ");
    				activeIndicator = activeColumn[i].Element.GetAttributeValueText("InnerText");
    				activeIndicator =  Regex.Replace(activeIndicator, ((char)160).ToString(), " ");
    				if(activeIndicator =="Yes")
    				{
    					activeIndicator="1";
    				}else
    				{
    					activeIndicator="0";
    				}
    				
    				CustomsBrokersTable customsBrokersTable = new CustomsBrokersTable(codeText, typeText, nameText, activeIndicator);
    				if(dBCustomsBrokerTypeList.Contains(customsBrokersTable))
    				{
    					Report.Success("Validation", "Database information matches reference table (Text= "+customsBrokersTable.Type+ " - "+customsBrokersTable.Code+ " - "+customsBrokersTable.Name+").");
    					Report.Info("Info", "DB Count: " + dBCustomsBrokerTypeList.Count);
    					dBCustomsBrokerTypeList.Remove(customsBrokersTable);
    				}else
    				{
    					Report.Failure("Data", "Database information DOES NOT match reference table (Text= "+customsBrokersTable.Type+ " - "+customsBrokersTable.Code+ " - "+ customsBrokersTable.Name +").");
    				}
    				i++;
    			}
    			isNextPage = bool.Parse(buttonNextInfo.FindAdapter<Unknown>().Element.GetAttributeValueText("Enabled"));
    			if(isNextPage)
    			{
    				buttonNextInfo.FindAdapter<Unknown>().Click();
    				Report.Log(ReportLevel.Info, "Delay", "Waiting for 5s.");
    				Delay.Duration(5000, false);
    			}
    		}
    		if(dBCustomsBrokerTypeList.Count > 0)
    		{
    			foreach(CustomsBrokersTable extra in dBCustomsBrokerTypeList)
    			{
    				Report.Info("Data", "Items in DB but not in the Reference Table (Text="+extra.Type+" - "+extra.Code+" - "+extra.Name+").");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}  
    	
    [UserCodeMethod]
   	public static void getAllMailingsTypeTableDB(RepoItemInfo tdtagCodeInfo, RepoItemInfo tdtagVaribleColumInfo, string tableName)
    	{
    		var repo = ShawIndustriesRepository.Instance;
    		
    		DBConnector dbConnector = new DBConnector();
    		string query = "Select MailingsType_code, MailingsType_name, MailingsType_activeIndicator from EBX_Replicated_Ref_MailingsType where MailingsType_activeIndicator = '1'";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		List<LegalHoldTypeTable> dBMailingsTypeList = new List<LegalHoldTypeTable>();
    		foreach(DataRow row in dt.Rows)
			{
    			LegalHoldTypeTable mailingTypeTable = 
    				new LegalHoldTypeTable(row[tableName + "_code"].ToString(), 
    				                       row[tableName + "_name"].ToString(), 
    				                       row[tableName + "_activeIndicator"].ToString());
				dBMailingsTypeList.Add(mailingTypeTable);
			}
    		
    		IList<TdTag> codeColumn = tdtagCodeInfo.CreateAdapters<TdTag>();
    		repo.TableColumnNumber = "1";
    		IList<TdTag> nameColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    		repo.TableColumnNumber = "2";
    		IList<TdTag> activeColumn = tdtagVaribleColumInfo.CreateAdapters<TdTag>();
    		
    		int i = 0;
    		string codeText = "";
    		string nameText = "";
    		string activeIndicator = "";
    		
    		foreach (TdTag option in codeColumn)
    		{
    			codeText = option.Element.GetAttributeValueText("InnerText");
    			codeText =  Regex.Replace(codeText, ((char)160).ToString(), " ");
    			
    			nameText = nameColumn[i].Element.GetAttributeValueText("InnerText");
    			nameText =  Regex.Replace(nameText, ((char)160).ToString(), " ");
    			
    			activeIndicator = activeColumn[i].Element.GetAttributeValueText("InnerText");
    			activeIndicator =  Regex.Replace(activeIndicator, ((char)160).ToString(), " ");
    			if(activeIndicator =="Yes")
    			{
    				activeIndicator="1";
    			}else
    			{
    				activeIndicator="0";
    			}
    			
    			LegalHoldTypeTable mailingType = new LegalHoldTypeTable(codeText, nameText, activeIndicator);
    			if(dBMailingsTypeList.Contains(mailingType))
    			{
    				Report.Success("Validation", "Validating Column Item Match Reference Table (Text= "+mailingType.Code+ " " + mailingType.Name + ") Matches.");
    				dBMailingsTypeList.Remove(mailingType);
    			}else
    			{
    				Report.Failure("Data", "Validating Column Item Match Reference Table (Text= "+mailingType.Code+ " " + mailingType.Name + ") DOES NOT Match.");
    			}
    			i++;
    		}
    		if(dBMailingsTypeList.Count > 0)
    		{
    			foreach(LegalHoldTypeTable extra in dBMailingsTypeList)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text= "+extra.Code+extra.Name+extra.ActiveIndicator+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}    	
    	
    	/// <summary>
    	/// Get All Remit to Address table to table and compare it to Table
    	/// </summary>
    	[UserCodeMethod]
    	public static void getAllRemitToAddressTableDB(RepoItemInfo codeCell, 
    	     RepoItemInfo boxNumberCell, RepoItemInfo cityStreetCell)
    	{
    		string tableName = "RemittoAddress";
    		
    		var repo = ShawIndustriesRepository.Instance;

    		DBConnector dbConnector = new DBConnector();
    		
    		string query = "SELECT " + tableName + "_Code, " + tableName + "_BoxNumber, "  + tableName + "_CityStreet " 
    			+ "FROM EBX_Replicated_Ref_" + tableName + " ORDER BY " + tableName + "_Code";
    		Report.Info("SQL Query: " + query);
    		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		List<RemitToAddressTable> dBRemitToAddressList = new List<RemitToAddressTable>();
    		
    		foreach(DataRow row in dt.Rows)
			{
    			RemitToAddressTable remitToAddressTable = 
    				new RemitToAddressTable(row[tableName + "_Code"].ToString(), 
    				                       row[tableName + "_BoxNumber"].ToString(), 
    				                       row[tableName + "_CityStreet"].ToString());
				dBRemitToAddressList.Add(remitToAddressTable);
			}
    		
    		IList<TdTag> codeColumn = codeCell.CreateAdapters<TdTag>();
    		IList<TdTag> boxNumberColumn = boxNumberCell.CreateAdapters<TdTag>();
    		IList<TdTag> cityStreetColumn = cityStreetCell.CreateAdapters<TdTag>();
    		
    		
    		int i = 0;
    		string codeText = "";
    		string boxNumberText = "";
    		string cityStreetText = "";
    		
    		
    		foreach (TdTag option in codeColumn)
    		{
    			codeText = option.Element.GetAttributeValueText("InnerText");
    			codeText =  Regex.Replace(codeText, ((char)160).ToString(), " ");
    			
    			boxNumberText = boxNumberColumn[i].Element.GetAttributeValueText("InnerText");
    			boxNumberText =  Regex.Replace(boxNumberText, ((char)160).ToString(), " ");
    			
    			cityStreetText = cityStreetColumn[i].Element.GetAttributeValueText("InnerText");
    			cityStreetText =  Regex.Replace(cityStreetText, ((char)160).ToString(), " ");
    			
    			Report.Info("Values on screen: " + codeText + " - " + boxNumberText + " - " + cityStreetText);
    			
    			
    			RemitToAddressTable remitToAddress = new RemitToAddressTable(codeText, boxNumberText, cityStreetText);
    			if(dBRemitToAddressList.Contains(remitToAddress))
    			{
    				Report.Success("Validation", "Validating Column Item Match Reference Table (Text="+remitToAddress.Code+ " " + remitToAddress.BoxNumber + " " + remitToAddress.CityStreet + ") on item 'tdTag'.");
    				dBRemitToAddressList.Remove(remitToAddress);
    			}else
    			{
    				Report.Failure("Data", "Validating Table on App  Match Reference Table (Text="+remitToAddress.Code+remitToAddress.BoxNumber+remitToAddress.CityStreet+") on item 'tdTag', not matched.");
    			}
    			i++;
    		}
    		
    		if(dBRemitToAddressList.Count > 0)
    		{
    			foreach(RemitToAddressTable extra in dBRemitToAddressList)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra.Code+extra.BoxNumber+extra.CityStreet+") on item 'tdTag', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'tdTag', all items in Database not matched.");
    		}
    		
    	}
    	
    	
    	/// <summary>
    	/// Get All Mail List Options table to table and compare it to Table
    	/// </summary>
    	[UserCodeMethod]
    	public static void getAllMailListOptionsTableDB(RepoItemInfo codeCell, RepoItemInfo nameCell, RepoItemInfo activeIndicatorCell)
    	{
    		string tableName = "MailListOptions";
    		
    		var repo = ShawIndustriesRepository.Instance;
    		
    		DBConnector dbConnector = new DBConnector();
    		
    		string query = "Select MailListOptions_code, MailListOptions_name, MailListOptions_activeIndicator from EBX_Replicated_Ref_MailListOptions order by MailListOptions_code";    		
    		Report.Info("SQL Query: " + query);    		
    		   		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		List<LegalHoldTypeTable> dBMailListOptionsList = new List<LegalHoldTypeTable>();
    		
    		foreach(DataRow row in dt.Rows)
			{
    			LegalHoldTypeTable mailListOptionsTable = 
    				new LegalHoldTypeTable(row[tableName + "_Code"].ToString(), 
    				                       row[tableName + "_Name"].ToString(), 
    				                       row[tableName + "_ActiveIndicator"].ToString());
				dBMailListOptionsList.Add(mailListOptionsTable);
			}
    		
    		IList<TdTag> codeColumn = codeCell.CreateAdapters<TdTag>();
    		IList<TdTag> nameColumn = nameCell.CreateAdapters<TdTag>();
    		IList<TdTag> activeIndicatorColumn = activeIndicatorCell.CreateAdapters<TdTag>();
    		
    		
    		int i = 0;
    		string codeText = "";
    		string nameText = "";
    		string activeIndicatorText = "";
    		
    		
    		foreach (TdTag option in codeColumn)
    		{
    			codeText = option.Element.GetAttributeValueText("InnerText");
    			codeText =  Regex.Replace(codeText, ((char)160).ToString(), " ");
    			Report.Info("Row " + i + ": codeText: " + codeText);
    			
    			nameText = nameColumn[i].Element.GetAttributeValueText("InnerText");
    			nameText =  Regex.Replace(nameText, ((char)160).ToString(), " ");
    			Report.Info("Row " + i + ": nameText: " + nameText);
    			
    			activeIndicatorText = activeIndicatorColumn[i].Element.GetAttributeValueText("InnerText");
    			activeIndicatorText =  Regex.Replace(activeIndicatorText, ((char)160).ToString(), " ");    			
    			if (activeIndicatorText == "Yes")
    				activeIndicatorText = "1";
    			else if (activeIndicatorText == "No")
    				activeIndicatorText = "0";
    			Report.Info("Row " + i + ": activeIndicatorText: " + activeIndicatorText);
    			
    			LegalHoldTypeTable mailToOptions = new LegalHoldTypeTable(codeText, nameText, activeIndicatorText);
    			if(dBMailListOptionsList.Contains(mailToOptions))
    			{
    				Report.Success("Validation", "Validating Column Item Match Reference Table (Text="+mailToOptions.Code+ " " + mailToOptions.Name + " " + mailToOptions.ActiveIndicator + ") on item 'tdTag'.");
    				dBMailListOptionsList.Remove(mailToOptions);
    			}else
    			{
    				Report.Failure("Data", "Validating Table on App  Match Reference Table (Text="+mailToOptions.Code+mailToOptions.Name+mailToOptions.ActiveIndicator+") on item 'tdTag', not matched.");
    			}
    			i++;
    		}
    		
    		if(dBMailListOptionsList.Count > 0)
    		{
    			foreach(LegalHoldTypeTable extra in dBMailListOptionsList)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra.Code+extra.Name+extra.ActiveIndicator+") on item 'tdTag', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'tdTag', all items in Database not matched.");
    		}    		
    	}
    	
    	
    	/// <summary>
    	/// Get All Home Center table to table and compare it to Table
    	/// </summary>
    	[UserCodeMethod]
    	public static void getAllHomeCenterTableDB(RepoItemInfo codeCell, 
    	     RepoItemInfo nameCell, RepoItemInfo activeIndicatorCell)
    	{
    		string tableName = "HomeCenter";
    		
    		var repo = ShawIndustriesRepository.Instance;
    		
    		DBConnector dbConnector = new DBConnector();
    		
    		string query = "SELECT " + tableName + "_code, " + tableName + "_name, " + tableName + "_activeIndicator "
    			+ "FROM EBX_Replicated_Ref_" + tableName + " WHERE " + tableName + "_activeIndicator = '1'";  		
    		Report.Info("SQL Query: " + query);    		
    		   		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		List<HomeCenterTable> dBHomeCenterList = new List<HomeCenterTable>();
    		
    		foreach(DataRow row in dt.Rows)
			{
    			HomeCenterTable homeCenterTable = 
    				new HomeCenterTable(row[tableName + "_Code"].ToString(), 
    				                       row[tableName + "_Name"].ToString(), 
    				                       row[tableName + "_ActiveIndicator"].ToString());
				dBHomeCenterList.Add(homeCenterTable);
			}
    		
    		IList<TdTag> codeColumn = codeCell.CreateAdapters<TdTag>();
    		IList<TdTag> nameColumn = nameCell.CreateAdapters<TdTag>();
    		IList<TdTag> activeIndicatorColumn = activeIndicatorCell.CreateAdapters<TdTag>();
    		
    		
    		int i = 0;
    		string codeText = "";
    		string nameText = "";
    		string activeIndicatorText = "";
    		
    		
    		foreach (TdTag option in codeColumn)
    		{
    			codeText = option.Element.GetAttributeValueText("InnerText");
    			codeText =  Regex.Replace(codeText, ((char)160).ToString(), " ");
    			Report.Info("Row " + i + ": codeText: " + codeText);
    			
    			nameText = nameColumn[i].Element.GetAttributeValueText("InnerText");
    			nameText =  Regex.Replace(nameText, ((char)160).ToString(), " ");
    			Report.Info("Row " + i + ": nameText: " + nameText);
    			
    			activeIndicatorText = activeIndicatorColumn[i].Element.GetAttributeValueText("InnerText");
    			activeIndicatorText =  Regex.Replace(activeIndicatorText, ((char)160).ToString(), " ");
    			if (activeIndicatorText == "Yes")
    				activeIndicatorText = "1";
    			else if (activeIndicatorText == "No")
    				activeIndicatorText = "0";
    			Report.Info("Row " + i + ": activeIndicatorText: " + activeIndicatorText);
    			
    			HomeCenterTable homeCenterOptions = new HomeCenterTable(codeText, nameText, activeIndicatorText);
    			if(dBHomeCenterList.Contains(homeCenterOptions))
    			{
    				Report.Success("Validation", "Validating Column Item Match Reference Table (Text="+homeCenterOptions.Code+ " " 
    				               + homeCenterOptions.Name + " " + homeCenterOptions.ActiveIndicator + ") on item 'tdTag'.");
    				dBHomeCenterList.Remove(homeCenterOptions);
    			}else
    			{
    				Report.Failure("Data", "Validating Table on App  Match Reference Table (Text="
    				               +homeCenterOptions.Code+homeCenterOptions.Name+homeCenterOptions.ActiveIndicator+") on item 'tdTag', not matched.");
    			}
    			i++;
    		}
    		
    		if(dBHomeCenterList.Count > 0)
    		{
    			foreach(HomeCenterTable extra in dBHomeCenterList)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="
    				            +extra.Code+extra.Name+extra.ActiveIndicator+") on item 'tdTag', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'tdTag', all items in Database not matched.");
    		}    		
    	}
  
    	[UserCodeMethod]
    	public static void getAllBusinessSegmentDB(RepoItemInfo codeCell, 
    	     RepoItemInfo nameCell, RepoItemInfo activeIndicatorCell)
    	{
    		string tableName = "BusinessSegment";
    		
    		var repo = ShawIndustriesRepository.Instance;
    		
    		DBConnector dbConnector = new DBConnector();
    		
    		string query = "SELECT " + tableName + "_code, " + tableName + "_name, " + tableName + "_activeIndicator "
    			+ "FROM EBX_Replicated_Ref_" + tableName + " WHERE " + tableName + "_activeIndicator = '1'";  		
    		Report.Info("SQL Query: " + query);    		
    		   		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		List<BusinessSegmentTable> dBBusinessSegmentList = new List<BusinessSegmentTable>();
    		
    		foreach(DataRow row in dt.Rows)
			{
    			BusinessSegmentTable BusinessSegmentTable = 
    				new BusinessSegmentTable(row[tableName + "_Code"].ToString(), 
    				                       row[tableName + "_Name"].ToString(), 
    				                       row[tableName + "_ActiveIndicator"].ToString());
				dBBusinessSegmentList.Add(BusinessSegmentTable);
			}
    		
    		IList<TdTag> codeColumn = codeCell.CreateAdapters<TdTag>();
    		IList<TdTag> nameColumn = nameCell.CreateAdapters<TdTag>();
    		IList<TdTag> activeIndicatorColumn = activeIndicatorCell.CreateAdapters<TdTag>();
    		
    		
    		int i = 0;
    		string codeText = "";
    		string nameText = "";
    		string activeIndicatorText = "";
    		
    		
    		foreach (TdTag option in codeColumn)
    		{
    			codeText = option.Element.GetAttributeValueText("InnerText");
    			codeText =  Regex.Replace(codeText, ((char)160).ToString(), " ");
    			Report.Info("Row " + i + ": codeText: " + codeText);
    			
    			nameText = nameColumn[i].Element.GetAttributeValueText("InnerText");
    			nameText =  Regex.Replace(nameText, ((char)160).ToString(), " ");
    			Report.Info("Row " + i + ": nameText: " + nameText);
    			
    			activeIndicatorText = activeIndicatorColumn[i].Element.GetAttributeValueText("InnerText");
    			activeIndicatorText =  Regex.Replace(activeIndicatorText, ((char)160).ToString(), " ");
    			if (activeIndicatorText == "Yes")
    				activeIndicatorText = "1";
    			else if (activeIndicatorText == "No")
    				activeIndicatorText = "0";
    			Report.Info("Row " + i + ": activeIndicatorText: " + activeIndicatorText);
    			
    			BusinessSegmentTable BusinessSegmentOptions = new BusinessSegmentTable(codeText, nameText, activeIndicatorText);
    			if(dBBusinessSegmentList.Contains(BusinessSegmentOptions))
    			{
    				Report.Success("Validation", "Validating Column Item Match Reference Table (Text="+BusinessSegmentOptions.Code+ " " 
    				               + BusinessSegmentOptions.Name + " " + BusinessSegmentOptions.ActiveIndicator + ") on item 'tdTag'.");
    				dBBusinessSegmentList.Remove(BusinessSegmentOptions);
    			}else
    			{
    				Report.Failure("Data", "Validating Table on App  Match Reference Table (Text="
    				               +BusinessSegmentOptions.Code+BusinessSegmentOptions.Name+BusinessSegmentOptions.ActiveIndicator+") on item 'tdTag', not matched.");
    			}
    			i++;
    		}
    		
    		if(dBBusinessSegmentList.Count > 0)
    		{
    			foreach(BusinessSegmentTable extra in dBBusinessSegmentList)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="
    				            +extra.Code+extra.Name+extra.ActiveIndicator+") on item 'tdTag', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'tdTag', all items in Database not matched.");
    		}    		
    	}
       	[UserCodeMethod]
    	public static void getBusinessTypeDropdown(RepoItemInfo codeCell, RepoItemInfo nameCell)
    	{
    		
    		string tableName = "BusinessType";
    		
    		var repo = ShawIndustriesRepository.Instance;
    		
    		DBConnector dbConnector = new DBConnector();
    		
    		string query = "Select EBX_Replicated_Ref_BusinessType.BusinessType_code, EBX_Replicated_Ref_BusinessType.BusinessType_name from EBX_Replicated_Ref_BusinessType join EBX_Replicated_Ref_BusinessType_BusinessType_ebxCustomerType on EBX_Replicated_Ref_BusinessType.BusinessType_code = EBX_Replicated_Ref_BusinessType_BusinessType_ebxCustomerType.BusinessType_code where EBX_Replicated_Ref_BusinessType_BusinessType_ebxCustomerType.BusinessType_ebxCustomerType_ = 'REGULAR'"; 		
    		Report.Info("SQL Query: " + query);    		
    		   		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		List<BusinessTypeTable> dBBusinessTypeList = new List<BusinessTypeTable>();
    		
    		foreach(DataRow row in dt.Rows)
			{
    			BusinessTypeTable BusinessTypeTable = 
    				new BusinessTypeTable(row[tableName + "_Code"].ToString(), 
    				                      row[tableName + "_Name"].ToString());
				dBBusinessTypeList.Add(BusinessTypeTable);
			}
    		
    		IList<TdTag> codeColumn = codeCell.CreateAdapters<TdTag>();
    		IList<TdTag> nameColumn = nameCell.CreateAdapters<TdTag>();
    		
    		
    		int i = 0;
    		string codeText = "";
    		string nameText = "";
    		
    		
    		foreach (TdTag option in codeColumn)
    		{
    			codeText = option.Element.GetAttributeValueText("InnerText");
    			codeText =  Regex.Replace(codeText, ((char)160).ToString(), " ");
    			Report.Info("Row " + i + ": codeText: " + codeText);
    			
    			nameText = nameColumn[i].Element.GetAttributeValueText("InnerText");
    			nameText =  Regex.Replace(nameText, ((char)160).ToString(), " ");
    			Report.Info("Row " + i + ": nameText: " + nameText);
    			
    			
    			BusinessTypeTable BusinessTypeOptions = new BusinessTypeTable(codeText, nameText);
    			if(dBBusinessTypeList.Contains(BusinessTypeOptions))
    			{
    				Report.Success("Validation", "Validating Column Item Match Reference Table (Text="+BusinessTypeOptions.Code+ " " 
    				                + BusinessTypeOptions.Name + ") on item 'tdTag'.");
    				dBBusinessTypeList.Remove(BusinessTypeOptions);
    			}else
    			{
    				Report.Failure("Data", "Validating Table on App  Match Reference Table (Text="
    				               +BusinessTypeOptions.Code+BusinessTypeOptions.Name+ ") on item 'tdTag', not matched.");
    			}
    			i++;
    		}
    		
    		if(dBBusinessTypeList.Count > 0)
    		{
    			foreach(BusinessTypeTable extra in dBBusinessTypeList)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="
    				            +extra.Code+extra.Code+extra.Name+") on item 'tdTag', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'tdTag', all items in Database not matched.");
    		}    		
    	}
  	 	   		 	
     	[UserCodeMethod]
    	public static void getAllBusinessTypeDB(RepoItemInfo codeCell, 
    	     RepoItemInfo nameCell, RepoItemInfo activeCell, RepoItemInfo ebxCell)
    	{
  		
    		var repo = ShawIndustriesRepository.Instance;
    		
    		DBConnector dbConnector = new DBConnector();
    		string tableName = "BusinessType";
    		
    		string query = "Select EBX_Replicated_Ref_BusinessType.BusinessType_code, EBX_Replicated_Ref_BusinessType.BusinessType_name, EBX_Replicated_Ref_BusinessType.BusinessType_activeIndicator,  EBX_Replicated_Ref_BusinessType_BusinessType_ebxCustomerType.BusinessType_ebxCustomerType_  from EBX_Replicated_Ref_BusinessType join EBX_Replicated_Ref_BusinessType_BusinessType_ebxCustomerType on EBX_Replicated_Ref_BusinessType.BusinessType_code = EBX_Replicated_Ref_BusinessType_BusinessType_ebxCustomerType.BusinessType_code"; 		
    		Report.Info("SQL Query: " + query);    		
    		   		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    	//	EBXUpdateReferenceTable referenceTables = new EBXUpdateReferenceTable();
    		
    		List<EBXUpdateReferenceTable> dBBusinessTypeList = new List<EBXUpdateReferenceTable>();
    		
    		foreach(DataRow row in dt.Rows)
			{
    			EBXUpdateReferenceTable ebxUpdateReferenceTable = 
    				new EBXUpdateReferenceTable(row[tableName + "_Code"].ToString(), 
    				                      row[tableName + "_Name"].ToString(),
    				                     row[tableName + "_ActiveIndicator"].ToString(),
    				                    row[tableName + "_EBXCustomerType_"].ToString());
				dBBusinessTypeList.Add(ebxUpdateReferenceTable);
			}
    		
    		IList<TdTag> codeColumn = codeCell.CreateAdapters<TdTag>();
    		IList<TdTag> nameColumn = nameCell.CreateAdapters<TdTag>();
     		IList<TdTag> activeColumn = activeCell.CreateAdapters<TdTag>();
    		IList<TdTag> ebxColumn = ebxCell.CreateAdapters<TdTag>();     		
    		
    		int i = 0;
    		string codeText = "";
    		string nameText = "";
      		string activeText = "";
    		string ebxText = "";      		
    		
    		foreach (TdTag option in codeColumn)
    		{
    			codeText = option.Element.GetAttributeValueText("InnerText");
    			codeText =  Regex.Replace(codeText, ((char)160).ToString(), " ");
    			Report.Info("Row " + i + ": codeText: " + codeText);
    			
    			nameText = nameColumn[i].Element.GetAttributeValueText("InnerText");
    			nameText =  Regex.Replace(nameText, ((char)160).ToString(), " ");
    			Report.Info("Row " + i + ": nameText: " + nameText);
    			
    			activeText = activeColumn[i].Element.GetAttributeValueText("InnerText");
    			activeText =  Regex.Replace(activeText, ((char)160).ToString(), " ");
    			if (activeText == "Yes")
    				activeText = "1";
    			else if (activeText == "No")
    				activeText = "0";
    			Report.Info("Row " + i + ": activeIndicatorText: " + activeText);
    			
    			ebxText = ebxColumn[i].Element.GetAttributeValueText("InnerText");
    			ebxText =  Regex.Replace(ebxText, ((char)160).ToString(), " ");
    			Report.Info("Row " + i + ": ebxText: " + ebxText);
    			
    			EBXUpdateReferenceTable BusinessTypeOptions = new EBXUpdateReferenceTable(codeText, nameText, activeText, ebxText);
    			if(dBBusinessTypeList.Contains(BusinessTypeOptions))
    			{
    				Report.Success("Validation", "Validating Column Item Match Reference Table (Text="+BusinessTypeOptions.Code+ " " 
    				                + BusinessTypeOptions.Name + ") on item 'tdTag'.");
    				dBBusinessTypeList.Remove(BusinessTypeOptions);
    			}else
    			{
    				Report.Failure("Data", "Validating Table on App  Match Reference Table (Text="
    				               +BusinessTypeOptions.Code+BusinessTypeOptions.Name+ ") on item 'tdTag', not matched.");
    			}
    			i++;
    		}
    		
    		if(dBBusinessTypeList.Count > 0)
    		{
    			foreach(EBXUpdateReferenceTable extra in dBBusinessTypeList)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="
    				            +extra.Code+extra.Code+extra.Name+") on item 'tdTag', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'tdTag', all items in Database not matched.");
    		}    		
    	}

    	[UserCodeMethod]
    	public static void getAllClaimsAreaDB(RepoItemInfo codeCell, 
    	     RepoItemInfo nameCell, RepoItemInfo activeIndicatorCell)
    	{
    		string tableName = "ClaimsArea";
    		
    		var repo = ShawIndustriesRepository.Instance;
    		
    		DBConnector dbConnector = new DBConnector();
    		
    		string query = "SELECT " + tableName + "_code, " + tableName + "_name, " + tableName + "_activeIndicator "
    			+ "FROM EBX_Replicated_Ref_" + tableName + " WHERE " + tableName + "_activeIndicator = '1'";  		
    		Report.Info("SQL Query: " + query);    		
    		   		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		List<BusinessSegmentTable> dBBusinessSegmentList = new List<BusinessSegmentTable>();
    		
    		foreach(DataRow row in dt.Rows)
			{
    			BusinessSegmentTable BusinessSegmentTable = 
    				new BusinessSegmentTable(row[tableName + "_Code"].ToString(), 
    				                       row[tableName + "_Name"].ToString(), 
    				                       row[tableName + "_ActiveIndicator"].ToString());
				dBBusinessSegmentList.Add(BusinessSegmentTable);
			}
    		
    		IList<TdTag> codeColumn = codeCell.CreateAdapters<TdTag>();
   			IList<TdTag> nameColumn = nameCell.CreateAdapters<TdTag>();
    		IList<TdTag> activeIndicatorColumn = activeIndicatorCell.CreateAdapters<TdTag>();
    		
    		
 	  		int i = 0;
    		string codeText = "";
    		string nameText = "";
    		string activeIndicatorText = "";
    		
    		
    		foreach (TdTag option in codeColumn)
    		{
    			codeText = option.Element.GetAttributeValueText("InnerText");
    			codeText =  Regex.Replace(codeText, ((char)160).ToString(), " ");
    			Report.Info("Row " + i + ": codeText: " + codeText);
    			
    			nameText = nameColumn[i].Element.GetAttributeValueText("InnerText");
    			nameText =  Regex.Replace(nameText, ((char)160).ToString(), " ");
    			Report.Info("Row " + i + ": nameText: " + nameText);
    			
    			activeIndicatorText = activeIndicatorColumn[i].Element.GetAttributeValueText("InnerText");
    			activeIndicatorText =  Regex.Replace(activeIndicatorText, ((char)160).ToString(), " ");
    			if (activeIndicatorText == "Yes")
    				activeIndicatorText = "1";
    			else if (activeIndicatorText == "No")
    				activeIndicatorText = "0";
    			Report.Info("Row " + i + ": activeIndicatorText: " + activeIndicatorText);
    			
   			
    			BusinessSegmentTable BusinessSegmentOptions = new BusinessSegmentTable(codeText, nameText, activeIndicatorText);
    			if(dBBusinessSegmentList.Contains(BusinessSegmentOptions))
    			{
    				Report.Success("Validation", "Validating Column Item Match Reference Table (Text="+BusinessSegmentOptions.Code+ " " 
    				               + BusinessSegmentOptions.Name + " " + BusinessSegmentOptions.ActiveIndicator + ") on item 'tdTag'.");
    				dBBusinessSegmentList.Remove(BusinessSegmentOptions);
    			}else
    			{
    				Report.Failure("Data", "Validating Table on App  Match Reference Table (Text="
    				               +BusinessSegmentOptions.Code+BusinessSegmentOptions.Name+BusinessSegmentOptions.ActiveIndicator+") on item 'tdTag', not matched.");
    			}
    			
    			

    			i++;
    		}

    		
    		if(dBBusinessSegmentList.Count > 0)
    		{
    			foreach(BusinessSegmentTable extra in dBBusinessSegmentList)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="
    				            +extra.Code+extra.Name+extra.ActiveIndicator+") on item 'tdTag', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'tdTag', all items in Database not matched.");
    		}    		
    	}     	
    	
    	/// <summary>
    	/// Get All Group Account table to table and compare it to Table
    	/// </summary>
    	/// 
    	
    	[UserCodeMethod]
    	public static void getAllGroupAccountTableDB(RepoItemInfo nameCell, 
    	     RepoItemInfo idCell, RepoItemInfo descriptionCell, RepoItemInfo buttonNextInfo)
    	{
    		Delay.Duration(2000, false);
    		
    		string tableName = "GroupAccount";
    		
    		var repo = ShawIndustriesRepository.Instance;
    		
    		DBConnector dbConnector = new DBConnector();
    		
    		string query = "SELECT " + tableName + "_name, " + tableName + "_id, " + tableName + "_description "
    			+ "FROM EBX_Replicated_Ref_" + tableName + " ORDER BY " + tableName + "_name";    		
    		Report.Info("SQL Query: " + query);    		
    		   		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		List<GroupAccountTable> dBGroupAccountList = new List<GroupAccountTable>();
    		
    		foreach(DataRow row in dt.Rows)
			{
    			GroupAccountTable groupAccountTable = 
    				new GroupAccountTable(row[tableName + "_Name"].ToString(), 
    				                       row[tableName + "_Id"].ToString(), 
    				                       row[tableName + "_Description"].ToString());
				dBGroupAccountList.Add(groupAccountTable);
			}
    		    		
    		
    		string nameText = "";
    		string idText = "";
    		string descriptionText = "";
    		bool isNextPage = true;
    		int page = 1;
    		
    		
    		while(isNextPage)
    		{
    			IList<TdTag> nameColumn = nameCell.CreateAdapters<TdTag>();
    			IList<DivTag> idColumn = idCell.CreateAdapters<DivTag>();
    			IList<TdTag> descriptionColumn = descriptionCell.CreateAdapters<TdTag>();
    		
    			int i = 0;
	    		foreach (TdTag option in nameColumn)
	    		{
	    			nameText = option.Element.GetAttributeValueText("InnerText");
	    			nameText =  Regex.Replace(nameText, ((char)160).ToString(), " ");
	    			Report.Info("Page: " + page + " - Row " + i + ": nameText: " + nameText);
	    			
	    			idText = idColumn[i].Element.GetAttributeValueText("InnerText");
	    			idText =  Regex.Replace(idText, ((char)160).ToString(), " ");
	    			Report.Info("Page: " + page + " - Row " + i + ": idText: " + idText);
	    			
	    			descriptionText = descriptionColumn[i].Element.GetAttributeValueText("InnerText");
	    			descriptionText =  Regex.Replace(descriptionText, ((char)160).ToString(), " ");    			
	    			Report.Info("Page: " + page + " - Row " + i + ": descriptionText: " + descriptionText);
	    			
	    			GroupAccountTable groupAccountOptions = new GroupAccountTable(nameText, idText, descriptionText);
	    			if(dBGroupAccountList.Contains(groupAccountOptions))
	    			{
	    				Report.Success("Validation", "Validating Column Item Match Reference Table (Text="+groupAccountOptions.Name+ " " 
	    				               + groupAccountOptions.Id + " " + groupAccountOptions.Description + ") on item 'tdTag'.");
	    				dBGroupAccountList.Remove(groupAccountOptions);
	    			}else
	    			{
	    				Report.Failure("Data", "Validating Table on App  Match Reference Table (Text="
	    				               +groupAccountOptions.Name+groupAccountOptions.Id+groupAccountOptions.Description+") on item 'tdTag', not matched.");
	    			}
	    			i++;
	    		}
	    		isNextPage = bool.Parse(buttonNextInfo.FindAdapter<Unknown>().Element.GetAttributeValueText("Enabled"));
    			if(isNextPage)
    			{
    				buttonNextInfo.FindAdapter<Unknown>().Click();
    				Report.Log(ReportLevel.Info, "Delay", "Waiting for 5s.");
    				Delay.Duration(5000, false);
    				page++;
    			}
	    		
    		}
    		
    		if(dBGroupAccountList.Count > 0)
    		{
    			foreach(GroupAccountTable extra in dBGroupAccountList)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="
    				            +extra.Name+extra.Id+extra.Description+") on item 'tdTag', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'tdTag', all items in Database not matched.");
    		}  
   
    	}
    
    }
}
