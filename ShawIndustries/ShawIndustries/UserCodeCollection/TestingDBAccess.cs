﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 4/7/2021
 * Time: 4:56 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;
using System.Data.Common;
using System.Data.Odbc;
using IBM.Data.DB2;
using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

//using IBM.Data.DB2Types;		





namespace ShawIndustries.UserCodeCollection
{
    /// <summary>
    /// Description of TestingDBAccess.
    /// </summary>
    [TestModule("11FFB40D-8DA7-4293-A268-7E703188C254", ModuleType.UserCode, 1)]
    public class TestingDBAccess : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public TestingDBAccess()
        {
            // Do not delete - a parameterless constructor is required!
        }

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
			DataTable dt = new DataTable();
    		using(SqlConnection conn = new SqlConnection())
    		{
    			conn.ConnectionString = "Data Source=SWWDEBXSQLDEV;Initial Catalog=EBXDatabaseQA;User Id=sqlcelicqa;Password=M^bb34fcPkn9QkItqYViB#4s;";
    			conn.Open();
    			string query = "Select Title_type, Title_code, Title_name, Title_activeIndicator from EBX_Replicated_Ref_Title  order by Title_type, Title_code";
    			SqlCommand comm = new SqlCommand(query, conn);
    			using(SqlDataReader reader = comm.ExecuteReader())
    			{
    				dt.Load(reader);
    			}
    			foreach(DataRow row in dt.Rows)
    			{
    				foreach(DataColumn dc in dt.Columns)
    				{
    					Report.Info(dc.ToString() + " = " +row[dc].ToString());
    				}
    			}
    			
//    			DataRow row = dt.Rows[0];
    			dt.Dispose();
    			conn.Close();
    		}     
//			DB2Connection conn = new DB2Connection();
//			DataTable dt = new DataTable();
//    		
//				conn.ConnectionString = "database=DBQ1; server=MVSA:5027;" + "UserID=vendcelt;Password=24thgvcx;";
//
//    			conn.Open();
//    			DB2Transaction trans = conn.BeginTransaction();
//    			DB2Command cmd = conn.CreateCommand();
//    			
//    			string procCall = "CALL SHXP.SISNP810(@param1, @param2, @param3, @param4, @param5)";
//    			cmd.Transaction = trans;
//    			cmd.CommandType = CommandType.Text;
//    			cmd.CommandText = procCall;
//    			
//    			DB2Parameter param1 = new DB2Parameter("@param1", DB2Type.Char, 7);
//    			param1.Direction = ParameterDirection.Input;
//    			param1.Value = "3500639";
//    			cmd.Parameters.Add(param1);
//    			
//    			DB2Parameter param2 = new DB2Parameter("@param2", DB2Type.Integer);
//    			param2.Direction = ParameterDirection.Output;
//    			cmd.Parameters.Add(param2);
//    			
//    			DB2Parameter param3 = new DB2Parameter("@param3", DB2Type.VarChar, 560);
//    			param3.Direction = ParameterDirection.Output;
//    			cmd.Parameters.Add(param3);
//    			
//    			DB2Parameter param4 = new DB2Parameter("@param4", DB2Type.Integer);
//    			param4.Direction = ParameterDirection.Output;
//    			cmd.Parameters.Add(param4);
//    			
//    			DB2Parameter param5 = new DB2Parameter("@param5", DB2Type.VarChar, 560);
//    			param5.Direction = ParameterDirection.Output;
//    			cmd.Parameters.Add(param5);
//    			                   
//    			using(DB2DataReader reader = cmd.ExecuteReader())
//    			{
//    				dt.Load(reader);
//    			}
//    			DataRow row = dt.Rows[0];
//    			dt.Dispose();
//    			conn.Close();
//    			
//    			Report.Info(row[2].ToString());
        }
    }
}
