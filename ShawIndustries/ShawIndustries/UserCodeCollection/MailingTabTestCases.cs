﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 4/15/2021
 * Time: 2:15 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.UserCodeCollection
{
    /// <summary>
    /// user codes for mail tab
    /// </summary>
    [UserCodeCollection]
    public class MailingTabTestCases
    {
    	
    	/// <summary>
    	/// Select the opposite of what's already selected.
    	/// </summary>
    	[UserCodeMethod]
    	public static void SelectOppositeYesNo(RepoItemInfo inputtagInfoYes, RepoItemInfo inputtagInfoNo)
    	{
    		Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'inputtagInfo' at Center.", inputtagInfoYes);
            string Checked;
            Checked = inputtagInfoYes.FindAdapter<InputTag>().Element.GetAttributeValueText("Checked");
            if (Checked == "True")
            {
            	inputtagInfoNo.FindAdapter<InputTag>().Click();
            }else
            {
            	inputtagInfoYes.FindAdapter<InputTag>().Click();
            }
    	}
    	
    	/// <summary>
    	/// Converts Boolean to Yes NO and Validate Data
    	/// </summary>
    	[UserCodeMethod]
    	public static void ConvertTrueFalseToYesNo(RepoItemInfo tdtagInfo, string defaultOptionSelected)
    	{
    		if (defaultOptionSelected == "True")
        	{
        		defaultOptionSelected = "Yes";
        	}else
        	{
        		defaultOptionSelected = "No";
        	}
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText=$CombinedPDF) on item 'tdtagInfo'.", tdtagInfo);
            Validate.AttributeEqual(tdtagInfo, "InnerText", defaultOptionSelected);
    	}
    	
    	/// <summary>
    	/// returns random email
    	/// </summary>
    	[UserCodeMethod]
    	public static string RandonEmailAddress()
    	{
    		string emailAddress = "Automation" + DateTimeOffset.UtcNow.ToUnixTimeSeconds() + "@yahoo.com";
            Report.Info("Unique emailAddress: " + emailAddress);
            return emailAddress;
    	}
    	
    	/// <summary>
    	/// Combines two strings together
    	/// </summary>
    	[UserCodeMethod]
    	public static string CombineStringsSeperateComma(string email1, string email2)
    	{
    		string finalEmail = email1 + ", " + email2;
    		Report.Info("Final String: " + finalEmail);
    		return finalEmail;
    	}
    	
    	
    	/// <summary>
    	/// Combines all items into a string
    	/// </summary>
    	[UserCodeMethod]
    	public static string GetAllTextPutIntoString(RepoItemInfo itemOptionInfo, string attributeType)
    	{
    		IList<Unknown> itemsOption = itemOptionInfo.CreateAdapters<Unknown>();
    		string combinedItems = "";
    		foreach(Unknown items in itemsOption)
    		{
    			if (combinedItems == "")
    			{
    				combinedItems = items.Element.GetAttributeValueText(attributeType);
    			}else
    			{
    				combinedItems = combinedItems + ";" + items.Element.GetAttributeValueText(attributeType);
    			}
    		}
    		return combinedItems;
    	}
    }
}
