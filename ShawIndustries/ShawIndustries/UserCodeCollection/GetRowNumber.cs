﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 4/14/2021
 * Time: 6:03 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.UserCodeCollection
{
    /// <summary>
    /// Creates a Ranorex user code collection. A collection is used to publish user code methods to the user code library.
    /// </summary>
    [UserCodeCollection]
    public class GetRowNumber
    {
        // You can use the "Insert New User Code Method" functionality from the context menu,
        // to add a new method with the attribute [UserCodeMethod].
        
        /// <summary>
        /// Find the Row Number for a table
        /// </summary>
        [UserCodeMethod]
        public static string GetRowNumberTable(RepoItemInfo tdtagInfo, string textName)
        {
        	Delay.Duration(2000, false);
        	IList<TdTag> tableRows = tdtagInfo.CreateAdapters<TdTag>();
        	int rowNumber = 0;
        	foreach(TdTag tdtag in tableRows)
        	{
        		rowNumber++;
        		string websiteText = Regex.Replace(tdtag.Element.GetAttributeValueText("InnerText"), "[^A-Za-z0-9]", " ");
        		textName = Regex.Replace(textName, "[^A-Za-z0-9]", " ");
        		if (websiteText == textName)
        		{
        			Report.Info("row number match = " + rowNumber);
        			break;
        		}
        	}
        	return rowNumber.ToString();
        }
    }
}
