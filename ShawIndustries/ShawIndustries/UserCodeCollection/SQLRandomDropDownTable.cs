﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 7/21/2021
 * Time: 1:57 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;
using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using ShawIndustries.Model;
using ShawIndustries.Service;

namespace ShawIndustries.UserCodeCollection
{
    /// <summary>
    /// Creates a Ranorex user code collection. A collection is used to publish user code methods to the user code library.
    /// </summary>
    [UserCodeCollection]
    public class SQLRandomDropDownTable
    {
    	
    	/// <summary>
    	/// This is a placeholder text. Please describe the purpose of the
    	/// user code method here. The method is published to the user code library
    	/// within a user code collection.
    	/// </summary>
    	[UserCodeMethod]
    	public static string GetARandomTerritoryManagerNumber()
    	{
    		var randomNumber = new Random();
    		DBConnector dbConnector = new DBConnector();
    		string query = "Select TerritoryManager_code, TerritoryManager_name from EBX_Replicated_Ref_TerritoryManage where nullif (TerritoryManager_name,' ') is not null";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		bool isFound = false;
    		int i = 0;
    		while (!isFound)
    		{
    			i = randomNumber.Next(dt.Rows.Count);
    			query = "Select A.sellingCompanyType_code, A.sellingCompanyType_name from EBX_Replicated_Ref_SellingCompanyType A inner join EBX_Replicated_Ref_SellingCompany B on A.sellingCompanyType_code = B.SellingCompany_sellingCompanyCode_ where B.SellingCompany_territory_ = " + dt.Rows[i]["TerritoryManager_code"].ToString();
    	   		DataTable dt2 = dbConnector.ConnectToSQLDataBase(query);
    	   		if(dt2.Rows.Count > 0)
    	   		{
    	   			isFound = true;
    	   		}
    		}
    		Report.Info(dt.Rows[i]["TerritoryManager_code"].ToString() + " - " + dt.Rows[i]["TerritoryManager_name"].ToString());
    		return dt.Rows[i]["TerritoryManager_code"].ToString() + " - " + dt.Rows[i]["TerritoryManager_name"].ToString();
    	}
    }
}
