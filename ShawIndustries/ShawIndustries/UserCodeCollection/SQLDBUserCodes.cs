﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 5/10/2021
 * Time: 11:17 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;
using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;
using ShawIndustries.Model;
using ShawIndustries.Service;

namespace ShawIndustries.UserCodeCollection
{
    /// <summary>
    /// Creates a Ranorex user code collection. A collection is used to publish user code methods to the user code library.
    /// </summary>
    [UserCodeCollection]
    public class SQLDBUserCodes
    {
    	
    	/// <summary>
    	/// Get Reference table to table and compare it to dropdown menu
    	/// </summary>
    	  	[UserCodeMethod]
    	   	public static void getBusinessSegmentTableDB(RepoItemInfo divtagInfo, string tableName)
    	{
    		DBConnector dbConnector = new DBConnector();
    		string query = "Select BusinessSegment_code, BusinessSegment_name, BusinessSegment_activeIndicator from EBX_Replicated_Ref_BusinessSegment where BusinessSegment_activeIndicator = '1'";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		ReferenceTables referenceTables = new ReferenceTables(dt, tableName);
    		IList<DivTag> dropDownList = divtagInfo.CreateAdapters<DivTag>();
    		string optionText = "";
    		foreach (DivTag option in dropDownList)
    		{
    			//divtagInfo.FindAdapter<DivTag>().Element.GetAttributeValueText("InnerText");
    			optionText = option.Element.GetAttributeValueText("InnerText");
    			optionText =  Regex.Replace(optionText, ((char)160).ToString(), " ");
    			if(referenceTables.ebxReferenceTable.Contains(optionText))
    			{
    				Report.Log(ReportLevel.Success, "Validation", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo'.");
    				referenceTables.ebxReferenceTable.Remove(optionText);
    			}else
    			{
    				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo', not matched.");
    			}
    		}
    		if(referenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in referenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}
    	  	[UserCodeMethod]
    	   	public static void getBusinessTypeTableDB(RepoItemInfo divtagInfo, string tableName)
    	{
    		DBConnector dbConnector = new DBConnector();
    		string query = "Select EBX_Replicated_Ref_BusinessType.BusinessType_code, EBX_Replicated_Ref_BusinessType.BusinessType_name from EBX_Replicated_Ref_BusinessType join EBX_Replicated_Ref_BusinessType_BusinessType_ebxCustomerType on EBX_Replicated_Ref_BusinessType.BusinessType_code = EBX_Replicated_Ref_BusinessType_BusinessType_ebxCustomerType.BusinessType_code where EBX_Replicated_Ref_BusinessType_BusinessType_ebxCustomerType.BusinessType_ebxCustomerType_ = 'REGULAR'";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		ReferenceTables referenceTables = new ReferenceTables(dt, tableName);
    		IList<DivTag> dropDownList = divtagInfo.CreateAdapters<DivTag>();
    		string optionText = "";
    		foreach (DivTag option in dropDownList)
    		{
    			//divtagInfo.FindAdapter<DivTag>().Element.GetAttributeValueText("InnerText");
    			optionText = option.Element.GetAttributeValueText("InnerText");
    			optionText =  Regex.Replace(optionText, ((char)160).ToString(), " ");
    			if(referenceTables.ebxReferenceTable.Contains(optionText))
    			{
    				Report.Log(ReportLevel.Info, "Validation", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo'.");
    				referenceTables.ebxReferenceTable.Remove(optionText);
    			}else
    			{
    				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo', not matched.");
    			}
    		}
    		if(referenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in referenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}
    	   	
    	[UserCodeMethod]
    	public static void getAllBusinessTypeTableDB(RepoItemInfo divtagInfo, string tableName)
    	{
    		DBConnector dbConnector = new DBConnector();
    		string query = "Select " + tableName + "_code, " + tableName + "_name, "+ tableName + "_activeIndicator from EBX_Replicated_Ref_" + tableName + "  order by " + tableName + "_code";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		ContactTypeReferenceTable contactTypeReferenceTables = new ContactTypeReferenceTable(dt, tableName);
    		IList<TdTag> dropDownList = divtagInfo.CreateAdapters<TdTag>();
    		string optionText = "";
    		foreach (TdTag option in dropDownList)
    		{
    			//divtagInfo.FindAdapter<DivTag>().Element.GetAttributeValueText("InnerText");
    			optionText = option.Element.GetAttributeValueText("InnerText");
    			optionText =  Regex.Replace(optionText, ((char)160).ToString(), " ");
    			if(contactTypeReferenceTables.ebxReferenceTable.Contains(optionText))
    			{
    				Report.Log(ReportLevel.Info, "Validation", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo'.");
    				contactTypeReferenceTables.ebxReferenceTable.Remove(optionText);
    			}else
    			{
    				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo', not matched.");
    			}
    		}
    		if(contactTypeReferenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in contactTypeReferenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}


    	[UserCodeMethod]
    	public static void getClaimsAreaTableDB(RepoItemInfo divtagInfo, string tableName)
    	{
    		DBConnector dbConnector = new DBConnector();
    		string query = "Select ClaimsArea_code, ClaimsArea_name, ClaimsArea_activeIndicator from EBX_Replicated_Ref_ClaimsArea where ClaimsArea_activeIndicator = '1'";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		ReferenceTables referenceTables = new ReferenceTables(dt, tableName);
    		IList<DivTag> dropDownList = divtagInfo.CreateAdapters<DivTag>();
    		string optionText = "";
    		foreach (DivTag option in dropDownList)
    		{
    			//divtagInfo.FindAdapter<DivTag>().Element.GetAttributeValueText("InnerText");
    			optionText = option.Element.GetAttributeValueText("InnerText");
    			optionText =  Regex.Replace(optionText, ((char)160).ToString(), " ");
    			if(referenceTables.ebxReferenceTable.Contains(optionText))
    			{
    				Report.Log(ReportLevel.Info, "Validation", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo'.");
    				referenceTables.ebxReferenceTable.Remove(optionText);
    			}else
    			{
    				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo', not matched.");
    			}
    		}
    		if(referenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in referenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}

    	   	[UserCodeMethod]
    	   	public static void getSellingTableDB(RepoItemInfo divtagInfo, string tableName, string territoryNumber)
    	   	{
    	   		DBConnector dbConnector = new DBConnector();
    	   		string query = "Select A.sellingCompanyType_code, A.sellingCompanyType_name from EBX_Replicated_Ref_SellingCompanyType A inner join EBX_Replicated_Ref_SellingCompany B on A.sellingCompanyType_code = B.SellingCompany_sellingCompanyCode_ where B.SellingCompany_territory_ = " + territoryNumber;
    	   		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    	   		
    	   		ReferenceTables referenceTables = new ReferenceTables(dt, tableName);
    	   		IList<DivTag> dropDownList = divtagInfo.CreateAdapters<DivTag>();
    	   		string optionText = "";
    	   		foreach (DivTag option in dropDownList)
    	   		{
    	   			//divtagInfo.FindAdapter<DivTag>().Element.GetAttributeValueText("InnerText");
    	   			optionText = option.Element.GetAttributeValueText("InnerText");
    	   			optionText =  Regex.Replace(optionText, ((char)160).ToString(), " ");
    	   			if(referenceTables.ebxReferenceTable.Contains(optionText))
    	   			{
    	   				Report.Log(ReportLevel.Info, "Validation", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo'.");
    	   				referenceTables.ebxReferenceTable.Remove(optionText);
    	   			}else
    	   			{
    	   				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo', not matched.");
    	   			}
    	   		}
    	   		if(referenceTables.ebxReferenceTable.Count > 0)
    	   		{
    	   			foreach(string extra in referenceTables.ebxReferenceTable)
    	   			{
    	   				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra+") on item 'divtagInfo', extra items in Database not matched.");
    	   			}
    	   			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    	   		}
    	   	}
    	   	
    	   	[UserCodeMethod]
    	   	public static void getReferenceTableDB(RepoItemInfo divtagInfo, string tableName)
    	   	{
    	   		DBConnector dbConnector = new DBConnector();
    	   		string query = "Select " + tableName + "_code, " + tableName + "_name, "+ tableName + "_activeIndicator from EBX_Replicated_Ref_" + tableName + " where " + tableName + "_activeIndicator = '1'";
    	   		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    	   		ReferenceTables referenceTables = new ReferenceTables(dt, tableName);
    		IList<DivTag> dropDownList = divtagInfo.CreateAdapters<DivTag>();
    		string optionText = "";
    		foreach (DivTag option in dropDownList)
    		{
    			//divtagInfo.FindAdapter<DivTag>().Element.GetAttributeValueText("InnerText");
    			optionText = option.Element.GetAttributeValueText("InnerText");
    			optionText =  Regex.Replace(optionText, ((char)160).ToString(), " ");
    			if(referenceTables.ebxReferenceTable.Contains(optionText))
    			{
    				Report.Log(ReportLevel.Info, "Validation", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo'.");
    				referenceTables.ebxReferenceTable.Remove(optionText);
    			}else
    			{
    				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo', not matched.");
    			}
    		}
    		if(referenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in referenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}
    	/// <summary>
    	/// Get Mailing Type table to table and compare it to dropdown menu
    	/// </summary>
    	[UserCodeMethod]
    	public static void getMailingTypeTableDB(RepoItemInfo divtagInfo, string tableName, string gridDocumentTypeItems)
    	{
    		DBConnector dbConnector = new DBConnector();
    		string query = "Select MailingsType_name from EBX_Replicated_Ref_MailingsType where MailingsType_activeIndicator = '1'";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		MailingsTypeTable referenceTables = new MailingsTypeTable(dt, tableName);
    		IList<DivTag> dropDownList = divtagInfo.CreateAdapters<DivTag>();
    		string optionText = "";
    		foreach (DivTag option in dropDownList)
    		{
    			//divtagInfo.FindAdapter<DivTag>().Element.GetAttributeValueText("InnerText");
    			optionText = option.Element.GetAttributeValueText("InnerText");
    			optionText =  Regex.Replace(optionText, ((char)160).ToString(), " ");
    			if(referenceTables.ebxReferenceTable.Contains(optionText))
    			{
    				Report.Log(ReportLevel.Success, "Validation", "Validating Drop Down Item (Text="+optionText+") Matches the Reference Table Info.");
    				referenceTables.ebxReferenceTable.Remove(optionText);
    			}else
    			{
    				Report.Failure("Data", "Validating Drop Down Item (Text="+optionText+") DOES NOT Match the Reference Table Info.");
    			}
    		}
    		if(gridDocumentTypeItems != "")
    		{
    			string[]docItemsArray = gridDocumentTypeItems.Split(';');
    			foreach (string option in docItemsArray)
    			{
    				optionText =  Regex.Replace(option, ((char)160).ToString(), " ");
    				if(referenceTables.ebxReferenceTable.Contains(optionText))
    				{
    					Report.Log(ReportLevel.Success, "Validation", "Validating Drop Down Item (Text="+optionText+") Matches the Reference Table Info.");
    					referenceTables.ebxReferenceTable.Remove(optionText);
    				}else
    				{
    					Report.Failure("Data", "Validating Drop Down Item (Text="+optionText+") DOES NOT Match the Reference Table Info.");
    				}
    			}
    		}
    		if(referenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in referenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}
    	/// <summary>
    	/// Get Currency Code Reference table to table and compare it to dropdown menu
    	/// </summary>
    	[UserCodeMethod]
    	public static void getCurrencyCodeTableDB(RepoItemInfo divtagInfo, string countryCode)
    	{
    		string tableName = "CurrencyCode";
    		
    		DBConnector dbConnector = new DBConnector();
    		
    		string query = "Select " + tableName + "_code, " + tableName + "_name from EBX_Replicated_Ref_" + tableName + 
    			" where " + tableName + "_Country = '" + countryCode + "' and " + tableName + "_activeIndicator = 'Y'";    		
    		Report.Info("SQL Query: " + query);
    		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		ReferenceTables referenceTables = new ReferenceTables(dt, tableName);
    		IList<DivTag> dropDownList = divtagInfo.CreateAdapters<DivTag>();
    		string optionText = "";
    		
    		foreach (DivTag option in dropDownList)
    		{
    			optionText = option.Element.GetAttributeValueText("InnerText");
    			optionText =  Regex.Replace(optionText, ((char)160).ToString(), " ");
    			Report.Info("optionText: " + optionText);
    			if(referenceTables.ebxReferenceTable.Contains(optionText))
    			{
    				Report.Log(ReportLevel.Info, "Validation", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo'.");
    				referenceTables.ebxReferenceTable.Remove(optionText);
    			}else
    			{
    				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo', not matched.");
    			}
    		}
    		if(referenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in referenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}
    	/// <summary>
    	/// Get Contact Type table to table and compare it to dropdown menu
    	/// </summary>
    	[UserCodeMethod]
    	public static void getContactTypeTableDB(RepoItemInfo divtagInfo, string tableName)
    	{
    		DBConnector dbConnector = new DBConnector();
    		string query = "Select " + tableName + "_code, " + tableName + "_name, "+ tableName + "_activeIndicator from EBX_Replicated_Ref_" + tableName + " where " + tableName + "_activeIndicator = '1'";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		ContactTypeReferenceTable contactTypeReferenceTables = new ContactTypeReferenceTable(dt, tableName);
    		IList<DivTag> dropDownList = divtagInfo.CreateAdapters<DivTag>();
    		    		
    		string optionText = "";
    		foreach (DivTag option in dropDownList)
    		{
    			//divtagInfo.FindAdapter<DivTag>().Element.GetAttributeValueText("InnerText");
    			optionText = option.Element.GetAttributeValueText("InnerText");
    			optionText =  Regex.Replace(optionText, ((char)160).ToString(), " ");
    			if(contactTypeReferenceTables.ebxReferenceTable.Contains(optionText))
    			{
    				Report.Log(ReportLevel.Info, "Validation", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo'.");
    				contactTypeReferenceTables.ebxReferenceTable.Remove(optionText);
    			}else
    			{
    				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo', not matched.");
    			}
    		}
    		if(contactTypeReferenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in contactTypeReferenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}
    	
    	
    	/// <summary>
    	/// Original Remit to Address Code with validation on a cell by cell basis
    	/// Get Remit to Address Reference table to table and compare it to Remit to Address Code Values data on App
    	/// </summary>
    	[UserCodeMethod]
    	public static void getRemitToAddressTableCodeDB(RepoItemInfo tdTagInfo, string column)
    	{   
    		string tableName = "RemittoAddress";

    		DBConnector dbConnector = new DBConnector();
    		
    		string query = "SELECT " + tableName + "_Code, " + tableName + "_BoxNumber, " + tableName + "_CityStreet "
    			+ "FROM EBX_Replicated_Ref_" + tableName + " ORDER BY " + tableName + "_Code";    		
    		Report.Info("SQL Query: " + query);
    		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		VariableReferenceTable referenceTables = new VariableReferenceTable(dt, tableName, column);
    		
    		IList<TdTag> tableCellValueList = tdTagInfo.CreateAdapters<TdTag>();
    		string cellText = "";
    		
    		foreach (TdTag cellValue in tableCellValueList)
    		{
    			cellText = cellValue.Element.GetAttributeValueText("InnerText");
    			cellText =  Regex.Replace(cellText, ((char)160).ToString(), " ");
    			
    			if(referenceTables.variableRefTable.Contains(cellText))
    			{
    				Report.Log(ReportLevel.Info, "Validation", "Table Cell Value on App Does Match Reference Table (Text = " +
    				           "" + cellText + ") on item 'tdTagInfo'.");
    				referenceTables.variableRefTable.Remove(cellText);
    			}else
    			{
    				Report.Failure("Data", "Validating Table Cell Value on App " +
    				               "(Text = " + cellText + ") on item 'tdTagInfo', NOT matched with Reference Table.");
    			}
    		}
    		if(referenceTables.variableRefTable.Count > 0)
    		{
    			foreach(string extra in referenceTables.variableRefTable)
    			{
    				Report.Info("Data", "Validating Table Cell Value Item Match Reference Table (Text = " +
    				            "" + extra + ") on item 'tdTagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Table Cell Value Item Match Reference Table on item 'tdTagInfo', " +
    			               "all items in Database not matched.");
    		}
    	}

	[UserCodeMethod]
    	public static void getShipCodeTableDB(RepoItemInfo divtagInfo, string tableName)
    	{
    		DBConnector dbConnector = new DBConnector();
    		string query = "Select ShipCode_code, ShipCode_name from EBX_Replicated_Ref_ShipCode where ShipCode_activeIndicator = '1'";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		ReferenceTables referenceTables = new ReferenceTables(dt, tableName);
    		IList<DivTag> dropDownList = divtagInfo.CreateAdapters<DivTag>();
    		string optionText = "";
    		foreach (DivTag option in dropDownList)
    		{
    			//divtagInfo.FindAdapter<DivTag>().Element.GetAttributeValueText("InnerText");
    			optionText = option.Element.GetAttributeValueText("InnerText");
    			optionText =  Regex.Replace(optionText, ((char)160).ToString(), " ");
    			if(referenceTables.ebxReferenceTable.Contains(optionText))
    			{
    				Report.Log(ReportLevel.Success, "Validation", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo'.");
    				referenceTables.ebxReferenceTable.Remove(optionText);
    			}else
    			{
    				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo', not matched.");
    			}
    		}
    		if(referenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in referenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text= "+extra+ ") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}    	
    	
[UserCodeMethod]
    	public static void getStoreTypeTableDB(RepoItemInfo divTagInfo, string tableName)
    	{
    		//string tableName = "StoreType";
    		
    		DBConnector dbConnector = new DBConnector();
    		
    		string query = "Select StoreType_Code, StoreType_Description from EBX_Replicated_Ref_StoreType where StoreType_ActiveIndicator='Y' order by StoreType_Code";
    		Report.Info("SQL Query: " + query);
    		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		DescReferenceTable referenceTables = new DescReferenceTable(dt, tableName);
    		IList<DivTag> dropDownList = divTagInfo.CreateAdapters<DivTag>();
    		string optionText = "";
    		foreach (DivTag option in dropDownList)
    		{
    			//divtagInfo.FindAdapter<DivTag>().Element.GetAttributeValueText("InnerText");
    			optionText = option.Element.GetAttributeValueText("InnerText");
    			optionText =  Regex.Replace(optionText, ((char)160).ToString(), " ");
    			if(referenceTables.ebxReferenceTable.Contains(optionText))
    			{
    				Report.Log(ReportLevel.Success, "Validation", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo'.");
    				referenceTables.ebxReferenceTable.Remove(optionText);
    			}else
    			{
    				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo', not matched.");
    			}
    		}
    		if(referenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in referenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}

[UserCodeMethod]
    	public static void getSubCreditAreaTableDB(RepoItemInfo divTagInfo, string tableName)
    	{
    		//string tableName = "StoreType";
    		
    		DBConnector dbConnector = new DBConnector();
    		
    		string query = "Select SubCreditArea_Description, SubCreditArea_CreditAreaNumber from EBX_Replicated_Ref_SubCreditArea order by SubCreditArea_Description";
    		Report.Info("SQL Query: " + query);
    		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		SubCreditReferenceTable referenceTables = new SubCreditReferenceTable(dt, tableName);
    		IList<DivTag> dropDownList = divTagInfo.CreateAdapters<DivTag>();
    		string optionText = "";
    		foreach (DivTag option in dropDownList)
    		{
    			//divtagInfo.FindAdapter<DivTag>().Element.GetAttributeValueText("InnerText");
    			optionText = option.Element.GetAttributeValueText("InnerText");
    			optionText =  Regex.Replace(optionText, ((char)160).ToString(), " ");
    			if(referenceTables.ebxReferenceTable.Contains(optionText))
    			{
    				Report.Log(ReportLevel.Success, "Validation", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo'.");
    				referenceTables.ebxReferenceTable.Remove(optionText);
    			}else
    			{
    				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo', not matched.");
    			}
    		}
    		if(referenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in referenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}    	 

    	
    	/// <summary>
    	/// Get Contact Type table to table and compare it to dropdown menu
    	/// </summary>
    	[UserCodeMethod]
    	public static void getAllContactTypeTableDB(RepoItemInfo divtagInfo, string tableName)
    	{
    		DBConnector dbConnector = new DBConnector();
    		string query = "Select " + tableName + "_code, " + tableName + "_name, "+ tableName + "_activeIndicator from EBX_Replicated_Ref_" + tableName + "  order by " + tableName + "_code";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		ContactTypeReferenceTable contactTypeReferenceTables = new ContactTypeReferenceTable(dt, tableName);
    		IList<TdTag> dropDownList = divtagInfo.CreateAdapters<TdTag>();
    		string optionText = "";
    		foreach (TdTag option in dropDownList)
    		{
    			//divtagInfo.FindAdapter<DivTag>().Element.GetAttributeValueText("InnerText");
    			optionText = option.Element.GetAttributeValueText("InnerText");
    			optionText =  Regex.Replace(optionText, ((char)160).ToString(), " ");
    			if(contactTypeReferenceTables.ebxReferenceTable.Contains(optionText))
    			{
    				Report.Log(ReportLevel.Info, "Validation", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo'.");
    				contactTypeReferenceTables.ebxReferenceTable.Remove(optionText);
    			}else
    			{
    				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo', not matched.");
    			}
    		}
    		if(contactTypeReferenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in contactTypeReferenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}
 
    	
    	[UserCodeMethod]
    	   	public static void getContactTitleTableDB(RepoItemInfo divtagInfo, string tableName)
    	{
    		DBConnector dbConnector = new DBConnector();
    		string query = "Select Title_code, Title_name from EBX_Replicated_Ref_Title where Title_type = 'CONTACT' order by Title_code";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		ReferenceTables referenceTables = new ReferenceTables(dt, tableName);
    		IList<DivTag> dropDownList = divtagInfo.CreateAdapters<DivTag>();
    		
   		
    		string optionText = "";
    		
    		foreach (DivTag option in dropDownList)
    		{
    			//divtagInfo.FindAdapter<DivTag>().Element.GetAttributeValueText("InnerText");
    			optionText = option.Element.GetAttributeValueText("InnerText");
     			optionText =  Regex.Replace(optionText, ((char)160).ToString(), " ");
     			
   			
     			if(referenceTables.ebxReferenceTable.Contains(optionText))
    			{
    				Report.Log(ReportLevel.Success, "Validation", "Validating Drop Down Item Match Reference Table (Text= "+optionText+") on item 'divtagInfo'.");
    				referenceTables.ebxReferenceTable.Remove(optionText);
    			}
    			else
    			{
    				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo', not matched.");
    			}
    		}
    		
    		if(referenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in referenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Extra Items in Database Table - (Text= "+extra+").");
    			}
    			Report.Failure("Data", "There is a discrepancy.  All items in the database are not matched.");
    		}
    	}    	
    	
    	   	[UserCodeMethod]
    	   	public static void getPrincipalTitleTableDB(RepoItemInfo divtagInfo, string tableName)
    	{
    		DBConnector dbConnector = new DBConnector();
    		string query = "Select Title_code, Title_name from EBX_Replicated_Ref_Title where Title_type = 'PRINCIPAL' order by Title_code";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		ReferenceTables referenceTables = new ReferenceTables(dt, tableName);
    		IList<DivTag> dropDownList = divtagInfo.CreateAdapters<DivTag>();
    		
   		
    		string optionText = "";
    		
    		foreach (DivTag option in dropDownList)
    		{
    			//divtagInfo.FindAdapter<DivTag>().Element.GetAttributeValueText("InnerText");
    			optionText = option.Element.GetAttributeValueText("InnerText");
     			optionText =  Regex.Replace(optionText, ((char)160).ToString(), " ");
     			
   			
     			if(referenceTables.ebxReferenceTable.Contains(optionText))
    			{
    				Report.Log(ReportLevel.Success, "Validation", "Drop Down Item Matches Reference Table (Text= "+optionText+").");
    				referenceTables.ebxReferenceTable.Remove(optionText);
    			}
    			else
    			{
    				Report.Failure("Data", "Drop Down Item DOES NOT match Reference Table (Text="+optionText+").");
    			}
    		}
    		
    		if(referenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in referenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Extra Items in Database Table - (Text= "+extra+").");
    			}
    			
    			Report.Failure("Data", "There is a discrepancy.  All items in the database are not matched.");
    		}
    	}    	
    	   	
  	[UserCodeMethod]
    	   	public static void getLegalHoldTypeTableDB(RepoItemInfo divtagInfo, string tableName)
    	{
    		DBConnector dbConnector = new DBConnector();
    		string query = "Select LegalHoldType_code, LegalHoldType_name, LegalHoldType_activeIndicator from EBX_Replicated_Ref_LegalHoldType order by LegalHoldType_code";
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		ReferenceTables referenceTables = new ReferenceTables(dt, tableName);
    		IList<DivTag> dropDownList = divtagInfo.CreateAdapters<DivTag>();
    		string optionText = "";
    		foreach (DivTag option in dropDownList)
    		{
    			//divtagInfo.FindAdapter<DivTag>().Element.GetAttributeValueText("InnerText");
    			optionText = option.Element.GetAttributeValueText("InnerText");
    			optionText =  Regex.Replace(optionText, ((char)160).ToString(), " ");
    		
    			if(referenceTables.ebxReferenceTable.Contains(optionText))
    			{
    				Report.Log(ReportLevel.Info, "Validation", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo'.");
    				referenceTables.ebxReferenceTable.Remove(optionText);
    			}else
    			{
    				Report.Failure("Data", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo', not matched.");
    			}
    		}
    		if(referenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in referenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra+") on item 'divtagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
    		}
    	}    	
    	
    	/// <summary>
    	/// Get Remit to Mail List Options table to table and compare it to Mail List Options Values data on App
    	/// </summary>
    	[UserCodeMethod]
    	public static void getMailListOptionsTableDB(RepoItemInfo divtagInfo, string tableName)
    	{    		
    		
    		DBConnector dbConnector = new DBConnector();
    		
    		string query = "Select MailListOptions_code, MailListOptions_name, MailListOptions_activeIndicator from EBX_Replicated_Ref_MailListOptions order by MailListOptions_code";    		
    		Report.Info("SQL Query: " + query);
    		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		ReferenceTables referenceTables = new ReferenceTables(dt, tableName);
    		
    		IList<DivTag> tableCellValueList = divtagInfo.CreateAdapters<DivTag>();
    		string cellText = "";
    		
    		foreach (DivTag cellValue in tableCellValueList)
    		{
    			cellText = cellValue.Element.GetAttributeValueText("InnerText");
    			cellText =  Regex.Replace(cellText, ((char)160).ToString(), " ");
    			
    			if (cellText == "Yes")
    				cellText = "1";
    			else if (cellText == "No")
    				cellText = "0";
    			
    			if(referenceTables.ebxReferenceTable.Contains(cellText))
    			{
    				Report.Log(ReportLevel.Success, "Validation", "Dropdown Value on App Matches Reference Table (Text = " +
    				           "" + cellText + ").");
    				referenceTables.ebxReferenceTable.Remove(cellText);
    			}else
    			{
    				Report.Failure("Data", "Table Cell Value on App " +
    				               "(Text = " + cellText + ") DOES NOT match.");
    			}
    		}
    		if(referenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in referenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Validating Table Cell Value Item Match Reference Table (Text = " +
    				            "" + extra + ") extra items in Database not matched.");
    			}
    				Report.Failure("Data", "Validating Table Cell Value Item Match Reference Table on item 'tdTagInfo', " +
    			               "all items in Database not matched.");
    		}
    	}
    	
    	
    	
    	/// <summary>
    	/// Get Home Center table to table and compare it to Values data on App
    	/// </summary>
    	[UserCodeMethod]
    	public static void getHomeCenterTableDB(RepoItemInfo divtagInfo, string tableName)
    	{    		
    	//	string tableName = "HomeCenter";
    		
    		DBConnector dbConnector = new DBConnector();
    		
    		string query = "Select HomeCenter_code, HomeCenter_name, HomeCenter_activeIndicator from EBX_Replicated_Ref_HomeCenter where HomeCenter_activeIndicator = '1'";    		
    		Report.Info("SQL Query: " + query);
    		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		MiscReferenceTable referenceTables = new MiscReferenceTable(dt, tableName);
    		
    		IList<DivTag> tableCellValueList = divtagInfo.CreateAdapters<DivTag>();
    		string cellText = "";
    		
    		foreach (DivTag cellValue in tableCellValueList)
    		{
    			cellText = cellValue.Element.GetAttributeValueText("InnerText");
    			cellText =  Regex.Replace(cellText, ((char)160).ToString(), " ");
    			
    			
    			if(referenceTables.ebxReferenceTable.Contains(cellText))
    			{
    				Report.Log(ReportLevel.Info, "Validation", "Table Cell Value on App Matches Reference Table (Text = " +
    				           "" + cellText + ") .");
    				referenceTables.ebxReferenceTable.Remove(cellText);
    			}else
    			{
    				Report.Failure("Data", "Validating Table Cell Value on App " +
    				               "(Text = " + cellText + ") DOES NOT match the Reference Table.");
    			}
    		}
    		if(referenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in referenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Validating Table Cell Value Item Match Reference Table (Text = " +
    				            "" + extra + ") on item 'tdTagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Table Cell Value Item Match Reference Table on item 'tdTagInfo', " +
    			               "all items in Database not matched.");
    		}
    	}

    	
    	/// <summary>
    	/// Get Group Account table to table and compare it to Values data on App
    	/// </summary>
    	[UserCodeMethod]
    	public static void getGroupAccountTableDB(RepoItemInfo divtagInfo, string tableName)
    	{    		
    		
    		DBConnector dbConnector = new DBConnector();
    		
    		string query = "Select GroupAccount_name from EBX_Replicated_Ref_GroupAccount  order by GroupAccount_name";    		
    		Report.Info("SQL Query: " + query);
    		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		MailingsTypeTable referenceTables = new MailingsTypeTable(dt, tableName);
    		
    		IList<DivTag> tableCellValueList = divtagInfo.CreateAdapters<DivTag>();
    		string nameText = "";
    		
    		foreach (DivTag nameValue in tableCellValueList)
    		{
    			nameText = nameValue.Element.GetAttributeValueText("InnerText");
    			nameText =  Regex.Replace(nameText, ((char)160).ToString(), " ");
    			
   			
    			if(referenceTables.ebxReferenceTable.Contains(nameText))
    			{
    				Report.Log(ReportLevel.Info, "Validation", "Table Cell Value on App Does Match Reference Table (Text = " +
    				           "" + nameText + ") on item 'tdTagInfo'.");
    				referenceTables.ebxReferenceTable.Remove(nameText);
    			}else
    			{
    				Report.Failure("Data", "Validating Table Cell Value on App " +
    				               "(Text = " + nameText + ") on item 'tdTagInfo', NOT matched with Reference Table.");
    			}
    		}
    		if(referenceTables.ebxReferenceTable.Count > 0)
    		{
    			foreach(string extra in referenceTables.ebxReferenceTable)
    			{
    				Report.Info("Data", "Validating Table Cell Value Item Match Reference Table (Text = " +
    				            "" + extra + ") on item 'tdTagInfo', extra items in Database not matched.");
    			}
    			Report.Failure("Data", "Validating Table Cell Value Item Match Reference Table on item 'tdTagInfo', " +
    			               "all items in Database not matched.");
    		}
    	}
    	
    }
}
