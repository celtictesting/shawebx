﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 4/28/2021
 * Time: 12:09 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;
using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;

namespace ShawIndustries.UserCodeCollection
{
    /// <summary>
    /// Creates a Ranorex user code collection. A collection is used to publish user code methods to the user code library.
    /// </summary>
    [UserCodeCollection]
    public class DataValidationCodes
    {
        // You can use the "Insert New User Code Method" functionality from the context menu,
        // to add a new method with the attribute [UserCodeMethod].
        
        /// <summary>
        /// Compares two strings match
        /// </summary>
        [UserCodeMethod]
        public static void ValidateTwoStringsMatch(string data1, string data2)
        {
        	Report.Log(ReportLevel.Info, "Validation", "Validating Data (Text = " + data1 + ")");
        	if(!Validate.Equals(data1.Replace(" ", ""), data2.Replace(" ", "")))
        	{
        		Report.Failure("Validation", "Validating Data (Text = " + data1 + " is Not Equal to " + data2 + ")");
        	}
        }
        /// <summary>
        /// get text remove characters then compares strings
        /// </summary>
        [UserCodeMethod]
        public static void GetTextRemoveRandomCharacters(RepoItemInfo tdtagInfo, string PhysicalAddress)
        {
        	string PhysicalAddress1 = tdtagInfo.FindAdapter<Unknown>().Element.GetAttributeValue("InnerText").ToString();
            PhysicalAddress1 = Regex.Replace(PhysicalAddress1, "[^A-Za-z0-9]", "");
            
            int maxLength = 25;
            
            if (PhysicalAddress1.Length > maxLength)
            {
            	PhysicalAddress1 = PhysicalAddress1.Substring(0, maxLength);            
            }   
            Report.Info("PhysicalAddress1: " + PhysicalAddress1);
            
            PhysicalAddress = Regex.Replace(PhysicalAddress, "[^A-Za-z0-9]", "");
           
            if (PhysicalAddress.Length > maxLength)
            {
            	PhysicalAddress = PhysicalAddress.Substring(0, maxLength);
            }   
            Report.Info("PhysicalAddress: " + PhysicalAddress);
            
            if(PhysicalAddress1 == PhysicalAddress)
            {
            	Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (InnerText= " + PhysicalAddress + ") on item 'tdtagInfo'.", tdtagInfo);
            }else
            {
            	Report.Failure("Data", "(Optional Action)\r\nValidating AttributeEqual Not Equal (InnerText= " + PhysicalAddress + ") on item 'tdtagInfo'.");
            }
        }  
		/// <summary>
        /// get text remove characters then compares strings
        /// </summary>
        [UserCodeMethod]
        public static void GetTextRemoveCharactersEnterValue(RepoItemInfo tdtagInfo, string PhysicalAddress, string attributeValue)
        {
        	string PhysicalAddress1 = tdtagInfo.FindAdapter<Unknown>().Element.GetAttributeValue(attributeValue).ToString();
            PhysicalAddress1 = Regex.Replace(PhysicalAddress1, "[^A-Za-z0-9]", "");
            
            int maxLength = 25;
            
            if (PhysicalAddress1.Length > maxLength)
            {
            	PhysicalAddress1 = PhysicalAddress1.Substring(0, maxLength);            
            }   
            Report.Info("PhysicalAddress1: " + PhysicalAddress1);
            
            PhysicalAddress = Regex.Replace(PhysicalAddress, "[^A-Za-z0-9]", "");
           
            if (PhysicalAddress.Length > maxLength)
            {
            	PhysicalAddress = PhysicalAddress.Substring(0, maxLength);
            }   
            Report.Info("PhysicalAddress: " + PhysicalAddress);
            
            if(PhysicalAddress1 == PhysicalAddress)
            {
            	Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (InnerText= " + PhysicalAddress + ") on item 'tdtagInfo'.", tdtagInfo);
            }else
            {
            	Report.Failure("Data", "(Optional Action)\r\nValidating AttributeEqual Not Equal (InnerText= " + PhysicalAddress + ") on item 'tdtagInfo'.");
            }
        }
		 /// <summary>
        /// Get text from TagValue, remove characters and then compares strings
        /// </summary>
        [UserCodeMethod]
        public static void CompareTagWithVariable(RepoItemInfo tdtagInfo, string variableText)
        {
        	string tagValue = tdtagInfo.FindAdapter<Unknown>().Element.GetAttributeValue("tagvalue").ToString();
            tagValue = Regex.Replace(tagValue, "[^A-Za-z0-9]", "");
            
            int maxLength = 20;
            
            if (tagValue.Length > maxLength)
            {
            	tagValue = tagValue.Substring(0, maxLength);
            }            
            Report.Info("Text value from screen: " + tagValue);
            
            variableText = Regex.Replace(variableText, "[^A-Za-z0-9]", "");            
            if (variableText.Length > maxLength)
            {
            	variableText = variableText.Substring(0, maxLength);
            }            
            Report.Info("Text from variable value: " + variableText);
            
            if(tagValue == variableText)
            {
            	Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (InnerText= " + variableText + ") on item 'tdtagInfo'.", tdtagInfo);
            }else
            {
            	Report.Failure("Data", "(Optional Action)\r\nValidating AttributeEqual Not Equal (InnerText= " + variableText + ") on item 'tdtagInfo'.");
            }
        }
        
        /// <summary>
        /// VerifyDateEqual Current date (M/d/yyyy)
        /// </summary>
        [UserCodeMethod]
        public static void ValidateDateMatchCurrentDate(string date)
        {
        	Report.Info("Current Date: " + System.DateTime.Now.ToString("M/d/yyyy"));
        	if(date == System.DateTime.Now.ToString("M/d/yyyy"))
        	{
        		Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (InnerText= " + date + ") equal Current Date");
        	}else
        	{
        		Report.Failure("Data", "(Optional Action)\r\nValidating AttributeEqual Not Equal (InnerText= " + date + ") Current Date");
        	}
        }
        /// <summary>
        /// Validate Dropdown Contains new items
        /// </summary>
        [UserCodeMethod]
        public static void ValidateDropDownItems(RepoItemInfo divtagInfo, string item1, string item2, string attributeType)
        {
        	
        	IList<Unknown> dropDownList = divtagInfo.CreateAdapters<Unknown>();
        	List<string>testItemList = new List<string>(){item1, item2};
        	string optionText = "";
        	foreach (Unknown option in dropDownList)
        	{
        		//divtagInfo.FindAdapter<DivTag>().Element.GetAttributeValueText("InnerText");
        		optionText = option.Element.GetAttributeValueText(attributeType);
        		optionText =  Regex.Replace(optionText, ((char)160).ToString(), " ");
        		if(testItemList.Contains(optionText))
        		{
        			Report.Log(ReportLevel.Info, "Validation", "Validating Drop Down Item Match Reference Table (Text="+optionText+") on item 'divtagInfo'.");
        			testItemList.Remove(optionText);
        		}
        	}
        	if(testItemList.Count > 0)
        	{
        		foreach(string extra in testItemList)
        		{
        			Report.Info("Data", "Validating Drop Down Item Match Reference Table (Text="+extra+") on item 'divtagInfo', extra items in Database not matched.");
        		}
        		Report.Failure("Data", "Validating Drop Down Item Match Reference Table on item 'divtagInfo', all items in Database not matched.");
        	}
        }
    }
}
