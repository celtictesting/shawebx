﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 7/2/2021
 * Time: 10:01 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;
using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;

namespace ShawIndustries.UserCodeCollection
{
    /// <summary>
    /// Selection by Label
    /// </summary>
    [UserCodeCollection]
    public class SelectorByTextLabel
    {
    	
    	/// <summary>
    	/// Key into input by label
    	/// </summary>
    	[UserCodeMethod]
    	public static string SelectInputField(RepoItemInfo labelFieldInfo, string labelName)
    	{
    		var repo = ShawIndustriesRepository.Instance;
			repo.labelName = labelName;
			string rowId = labelFieldInfo.FindAdapter<Unknown>().Parent.Parent.Element.GetAttributeValueText("Id").ToString();
			Report.Info(rowId);
			return rowId;
    	}
    	
    	/// <summary>
    	/// Get Date format mm/dd/yyyy
    	/// </summary>
    	[UserCodeMethod]
    	public static string GetDateFromThreeFields(RepoItemInfo labelFieldInfo)
    	{
    		var repo = ShawIndustriesRepository.Instance;
    		repo.iterationNumber = "1";
    		string month = labelFieldInfo.FindAdapter<Unknown>().Element.GetAttributeValueText("Value").ToString();
    		repo.iterationNumber = "2";
    		string day = labelFieldInfo.FindAdapter<Unknown>().Element.GetAttributeValueText("Value").ToString();
    		repo.iterationNumber = "3";
    		string year = labelFieldInfo.FindAdapter<Unknown>().Element.GetAttributeValueText("Value").ToString();
    		string date = month + "/" + day + "/" + year;
    		Report.Info("Date: " + date);
    		return date;
    	}
    }
}
