﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 3/12/2021
 * Time: 2:58 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace ShawIndustries.UserCodeCollection
{
    /// <summary>
    /// Creates a Ranorex user code collection. A collection is used to publish user code methods to the user code library.
    /// </summary>
    [UserCodeCollection]
    public class RandomTextGenerator
    {
        // You can use the "Insert New User Code Method" functionality from the context menu,
        // to add a new method with the attribute [UserCodeMethod].
        
        /// <summary>
        /// This is a placeholder text. Please describe the purpose of the
        /// user code method here. The method is published to the user code library
        /// within a user code collection.
        /// </summary>
        [UserCodeMethod]
        public static string GetRandomEmailAddress()
        {
        	String CustomerEmailAddress = "Automation" + DateTimeOffset.UtcNow.ToUnixTimeSeconds() + "@test.com";
        	return CustomerEmailAddress;
        }
        [UserCodeMethod]
        public static string GetRandomPhoneNumber()
        {
        	var ddrandom = new Random();
        	int index = ddrandom.Next(1000000, 9999999);
        	String phoneNumber = index.ToString();
        	return phoneNumber;
        }
        /// <summary>
        /// Random Three digits
        /// </summary>
        [UserCodeMethod]
        public static string RandomThreeDigits()
        {
        	var ddrandom = new Random();
        	int index = ddrandom.Next(100, 999);
        	String threeDigitNumber = index.ToString();
        	return threeDigitNumber;
        }
        /// <summary>
        /// Random Four digits
        /// </summary>
        [UserCodeMethod]
        public static string RandomFourDigits()
        {
        	var ddrandom = new Random();
        	int index = ddrandom.Next(1000, 9999);
        	String fourDigitNumber = index.ToString();
        	return fourDigitNumber;
        }
        /// <summary>
        /// Random Month digit
        /// </summary>
        [UserCodeMethod]
        public static string RandomMonthNuumber()
        {
        	var ddrandom = new Random();
        	int index = ddrandom.Next(1, 12);
        	String monthNumber = index.ToString();
        	return monthNumber;
        }
        /// <summary>
        /// Random days digit
        /// </summary>
        [UserCodeMethod]
        public static string RandomDayNuumber()
        {
        	var ddrandom = new Random();
        	int index = ddrandom.Next(1, 28);
        	String number = index.ToString();
        	return number;
        }
        /// <summary>
        /// Random Month digit
        /// </summary>
        [UserCodeMethod]
        public static string RandomYearNuumber()
        {
        	var ddrandom = new Random();
        	int index = ddrandom.Next(1, 10);
        	index = int.Parse(System.DateTime.Now.ToString("yyyy")) - index;
        	String number = index.ToString();
        	return number;
        }
        /// <summary>
        /// Change value of a variable
        /// </summary>
        [UserCodeMethod]
        public static string SetVariableValue(string newValue)
        {
        	return newValue;
        }
        
        /// <summary>
        /// TrimSellingCompanyToNumber
        /// </summary>
        [UserCodeMethod]
        public static string TrimSellingCompany(string sellingCompany)
        {
        	string territoryNumber = "";
        	territoryNumber = Regex.Replace(sellingCompany, "[^0-9]", "");
        	Report.Info(territoryNumber);
        	return territoryNumber;
        }
        /// <summary>
        /// TrimSellingCompanyTo Customer Name
        /// </summary>
        [UserCodeMethod]
        public static string TrimToCustomerName(string sellingCompany)
        {
        	string customerName = "";
        	customerName = Regex.Replace(sellingCompany, "[^a-zA-Z ]", "");
        	customerName = customerName.Trim();
        	Report.Info(customerName);
        	return customerName;
        }
    }
}
