﻿/*
 * Created by Ranorex
 * User: joecatr
 * Date: 4/5/2021
 * Time: 3:16 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace ShawIndustries.UserCodeCollection
{
    /// <summary>
    /// Description of RandomName.
    /// </summary>
    [TestModule("26EAAD4E-50A4-4549-8095-DAD0D30C379E", ModuleType.UserCode, 1)]
    public class RandomName : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public RandomName()
        {
            // Do not delete - a parameterless constructor is required!
        }

string _First = "";
[TestVariable("3658169f-b455-4d36-a701-fae523739954")]
public string First
{
	get { return _First; }
	set { _First = value; }
}

string _Last = "";
[TestVariable("be3a6c05-6a0e-40f5-8fc7-729f344e56b3")]
public string Last
{
	get { return _Last; }
	set { _Last = value; }
}

string _Name = "";
[TestVariable("44fa7d08-9c65-4d98-8119-cc783a592097")]
public string Name
{
	get { return _Name; }
	set { _Name = value; }
}

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
              var Frandom = new Random();
			var list1 = new List<string>{"John", "Steve", "Larry", "David", "Joe", "Brandt", "Clarence", "Wilson", "Amamnda", "Carol", "Denise", "Vanessa", "Hannah", "Wendy", "Deborah", "Jake", "Jesse", "Sam", "Allison", "Jamie", "William", "Jonathan", "Madison", "Evelyn", "Ashlyn", "Jacob", "Donald", "Richard"};
			int index1 = Frandom.Next(list1.Count);
			First = list1[index1];
			
			var Lrandom = new Random();
			var list2 = new List<string>{"Addams", "Baker", "Brown", "Bryant", "Charlseton", "Chesterfield", "Davis", "Davidson", "Edwards", "Emmerson", "Frank", "Francis", "Grant", "Gleeson", "Harrison", "Haynes", "Ingram", "Innis", "Jaconbs", "Jefferson", "Keller", "Kennedy", "Lovecraft", "Livington", "Maddox", "Mitchell", "Nelson", "Nemon", "Oscar", "Ogelsby", "Patterson", "Perdue", "Quincy", "Reagan", "Rodgers", "Smith", "Stevenson", "Truman", "Tackleberry", "Underwood", "Uther", "Victoria", "Vance", "Williams", "Winchester", "Xi", "Yates", "Young", "Zander"};
			int index2 = Lrandom.Next(list2.Count);
			Last = list2[index2];
			
			Name = First + " " + Last;
        }
    }
}
