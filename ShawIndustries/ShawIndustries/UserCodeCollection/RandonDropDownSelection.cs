﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 3/12/2021
 * Time: 1:11 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;
using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;
using ShawIndustries.Model;

namespace ShawIndustries.UserCodeCollection
{
    /// <summary>
    /// Creates a Ranorex user code collection. A collection is used to publish user code methods to the user code library.
    /// </summary>
    [UserCodeCollection]
    public class RandonDropDownSelection
    {
    	
    	/// <summary>
    	/// Get random Number
    	/// </summary>
    	[UserCodeMethod]
    	public static string GetRandonNumber(string Id, int lowNumber, int highNumber)
    	{
        	var ddrandom = new Random();
        	int index = ddrandom.Next(lowNumber, highNumber);
			Id = Id + index;
			return Id;
    	}
        /// <summary>
    	/// Get random Number
    	/// </summary>
    	[UserCodeMethod]
    	public static string GetNewAddress(string Id, int lowNumber, int highNumber, string currentAddress1, RepoItemInfo divtagInfo)
    	{
    		var repo = ShawIndustriesRepository.Instance;
        	var ddrandom = new Random();
        	int index = 0;
        	bool isNewAddress = false;

        	while (!isNewAddress)
        	{
        		index = ddrandom.Next(lowNumber, highNumber);
        		repo.PhysicalAddress = Id + index;
        		string newAddress = divtagInfo.FindAdapter<Unknown>().Element.GetAttributeValueText("InnerText");
        		Report.Info(newAddress);
        		if(currentAddress1 != newAddress)
        		{
        			isNewAddress=true;
        		}
        	}
        	Id = Id + index;
        	Report.Info(Id);
			return Id;
    	}    	
    	
        /// <summary>
        /// Get Yes or No
        /// </summary>
        [UserCodeMethod]
        public static string GetRandonYesNo()
        {
        	string yesNo = "";
        	var ddrandom = new Random();
        	int index = ddrandom.Next(1, 3);
        	Report.Info(index.ToString());
        	if (index == 1)
        	{
        		yesNo= "Yes";
        	}else
        	{
        		yesNo = "No";
        	}
        	return yesNo;
        }
        
        /// <summary>
        /// Select dropdown next to text label
        /// </summary>
        [UserCodeMethod]
        public static string RetrieveTextLabelRow(RepoItemInfo labeltagInfo)
        {
        	
        	Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'labeltagInfo' at Center.", labeltagInfo);
            return labeltagInfo.FindAdapter<LabelTag>().Parent.Parent.Element.GetAttributeValue("Id").ToString();
        }
        /// <summary>
        /// Select dropdown next to text label
        /// </summary>
        [UserCodeMethod]
        public static string RetrieveTextLabelRowThreeLevel(RepoItemInfo labeltagInfo, string labelText, string attributeType)
        {
        	var repo = ShawIndustriesRepository.Instance;
        	repo.DropDownLabelName = labelText;
        	Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'labeltagInfo' at Center.", labeltagInfo);
            return labeltagInfo.FindAdapter<LabelTag>().Parent.Parent.Parent.Element.GetAttributeValue(attributeType).ToString();
        }
        /// <summary>
        /// Select dropdown next to text label
        /// </summary>
        [UserCodeMethod]
        public static string RetrieveTextLabelRowTwoLevel(RepoItemInfo labeltagInfo, string labelText, string attributeType)
        {
        	var repo = ShawIndustriesRepository.Instance;
        	repo.DropDownLabelName = labelText;
        	Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'labeltagInfo' at Center.", labeltagInfo);
            return labeltagInfo.FindAdapter<LabelTag>().Parent.Parent.Element.GetAttributeValue(attributeType).ToString();
        }
        /// <summary>
        /// Select internal customer dropdown next to text label
        /// </summary>
        [UserCodeMethod]
        public static string RetrieveInternalTextLabelRow(RepoItemInfo labeltagInfo)
        {
        	
        	Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'labeltagInfo' at Center.", labeltagInfo);
            return labeltagInfo.FindAdapter<LabelTag>().Parent.Parent.Parent.Element.GetAttributeValue("Id").ToString();
        }
        
        /// <summary>
        /// Get all items then clicks one
        /// </summary>
        [UserCodeMethod]
        public static void SelectRandomItemFromDropDown(RepoItemInfo divtagInfo)
        {
        	IList<DivTag> dropDownList = divtagInfo.CreateAdapters<DivTag>();
        	var ddrandom = new Random();
        	int index = ddrandom.Next(1, dropDownList.Count);
        	Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'spantagInfo' at Center.", divtagInfo);
        	dropDownList[index-1].EnsureVisible();
        	dropDownList[index-1].PerformClick();
        	//dropDownList[index-1].Click(Location.UpperLeft);
        }
        
        /// <summary>
        /// Get all items makes sure random doesn't equal previous selling company then clicks one
        /// </summary>
        [UserCodeMethod]
        public static void SelectRandomNewItemFromDropDown(RepoItemInfo divtagInfo, string previousSellingCompany, string attributeType)
        {
        	IList<Unknown> dropDownList = divtagInfo.CreateAdapters<Unknown>();
        	bool isFound = false;
        	var ddrandom = new Random();
        	int index = 0;
        	string sellingCompanyText = "";
        	while(!isFound)
        	{
        		index = ddrandom.Next(1, dropDownList.Count);
        		sellingCompanyText = dropDownList[index-1].Element.GetAttributeValue(attributeType).ToString();
        		sellingCompanyText = Regex.Replace(sellingCompanyText, ((char)160).ToString(), " ");
        		if(sellingCompanyText != Regex.Replace(previousSellingCompany, ((char)160).ToString(), " "))
        		{
        			isFound = true;
        		}
        	}
        	
        	Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'spantagInfo' at Center.", divtagInfo);
        	dropDownList[index-1].EnsureVisible();
        	dropDownList[index-1].Click(Location.UpperLeft);
        }
        
        /// <summary>
        /// Select Item from list that match string
        /// </summary>
        [UserCodeMethod]
        public static void SelectItemFromDropDown(RepoItemInfo divtagInfo, string optionItem, string attributeType)
        {
        	IList<Unknown> dropDownList = divtagInfo.CreateAdapters<Unknown>();
        	bool isFound = false;
        	string optionItemEdited = "";
        	foreach (Unknown items in dropDownList)
        	{
        		optionItemEdited = items.Element.GetAttributeValue(attributeType).ToString();
        		optionItemEdited = Regex.Replace(optionItemEdited, ((char)160).ToString(), " ");
        		if (optionItemEdited == Regex.Replace(optionItem, ((char)160).ToString(), " "))
        		{
        			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'spantagInfo' at Center.", divtagInfo);
        			items.Click(Location.UpperLeft);
        			isFound = true;
        			break;
        		}
        	}
        	if(!isFound)
        	{
        		Report.Failure("Data", "Unable to find Item");
        	}
        }
        
        /// <summary>
        /// Get string Name from List
        /// </summary>
        [UserCodeMethod]
        public static string GetRandomItemList(string dropDownName, string priceCountry)
        {
        	List<string> dropDownList = new DropDownItemList().GetDropDownListItems(dropDownName);
        	var ddrandom = new Random();
        	
        	int index = ddrandom.Next(0, dropDownList.Count - 1);
        	string newPriceCountry = dropDownList[index];
        	while(newPriceCountry == priceCountry)
        	{
        		index = ddrandom.Next(0, dropDownList.Count - 1);
        		newPriceCountry = dropDownList[index];
        	}
        	return newPriceCountry;
        }  
        /// <summary>
        /// mouse scroll down for seconds.
        /// </summary>
        [UserCodeMethod]
        public static void scrollPageDownSecs(int seconds)
        {
        	Stopwatch timer = new Stopwatch();
        	timer.Start();
        	Report.Log(ReportLevel.Info, "Mouse", "Mouse scroll Vertical");
        	while(timer.Elapsed.TotalSeconds < seconds)
        	{
        		Mouse.ScrollWheel(-1000);
        		Delay.Duration(2000, false);
        	}
        	timer.Stop();
        }
    }
}
