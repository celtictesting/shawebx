﻿/*
 * Created by Ranorex
 * User: danhoan
 * Date: 4/27/2021
 * Time: 9:13 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using IBM.Data.DB2;
using Ranorex;
using ShawIndustries.Model;

namespace ShawIndustries.Service
{
	/// <summary>
	/// Description of DBConnector.
	/// </summary>
	public class DBConnector
	{
		public DBResultSet ConnectToDataBase(List<DB2Parameter> parameters, string procCall)
		{
			DBResultSet dbResultSet = new DBResultSet();
			DataTable dt = new DataTable();
			using(DB2Connection conn = new DB2Connection())
			{
				conn.ConnectionString = "database=DBQ1; server=MVSA:5027;" + "UserID=$WSCMMSQ;Password=Lt64bvcx;";

				conn.Open();
				DB2Transaction trans = conn.BeginTransaction();
				DB2Command cmd = conn.CreateCommand();
				cmd.Transaction = trans;
				cmd.CommandType = CommandType.Text;
				cmd.CommandText = procCall;
				foreach (DB2Parameter dbParameter in parameters)
				{
					cmd.Parameters.Add(dbParameter);
				}
				
				using(DB2DataReader reader = cmd.ExecuteReader())
				{
					dt.Load(reader);
				}
				dt.Dispose();
				dbResultSet.DBDataTable = dt;
				dbResultSet.Parameters = parameters;
			}
			return dbResultSet;
		}
		public DataTable ConnectToSQLDataBase(string query)
		{
			DataTable dt = new DataTable();
    		using(SqlConnection conn = new SqlConnection())
    		{
    			conn.ConnectionString = "Data Source=SWWDEBXSQLDEV;Initial Catalog=EBXDatabaseQA;User Id=sqlcelicqa;Password=M^bb34fcPkn9QkItqYViB#4s;";
    			conn.Open();
    			SqlCommand comm = new SqlCommand(query, conn);
    			using(SqlDataReader reader = comm.ExecuteReader())
    			{
    				dt.Load(reader);
    			}
    			dt.Dispose();
    			conn.Close();
    		}   
			return dt;
		}
		
		public DataTable ConnectToDB2DataBase(string query)
		{
			DataTable dt = new DataTable();
			using(DB2Connection conn = new DB2Connection())
			{
				conn.ConnectionString = "database=DBQ1; server=MVSA:5027;" + "UserID=$WSCMMSQ;Password=Lt64bvcx;";
				conn.Open();

		 		DB2Command comm = new DB2Command(query, conn);
    			using(DB2DataReader reader = comm.ExecuteReader())
    			{
    				dt.Load(reader);
    			}
    			dt.Dispose();
    			conn.Close();
    		}   
			return dt;
		}			
	}
}
