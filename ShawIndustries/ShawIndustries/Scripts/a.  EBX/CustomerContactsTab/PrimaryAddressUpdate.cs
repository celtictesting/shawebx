﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.CustomerContactsTab
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The PrimaryAddressUpdate recording.
    /// </summary>
    [TestModule("09076f2a-bde9-4e03-bbd4-64a8a09892a5", ModuleType.Recording, 1)]
    public partial class PrimaryAddressUpdate : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static PrimaryAddressUpdate instance = new PrimaryAddressUpdate();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public PrimaryAddressUpdate()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static PrimaryAddressUpdate Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.AutomationContact' at Center.", repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.AutomationContactInfo, new RecordItemIndex(0));
            repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.AutomationContact.DoubleClick();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.BusinessAddress1'.", repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.BusinessAddress1Info, new RecordItemIndex(1));
            Validate.Exists(repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.BusinessAddress1Info);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.BusinessAddress2'.", repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.BusinessAddress2Info, new RecordItemIndex(2));
            Validate.Exists(repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.BusinessAddress2Info);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.CustomerAddressTab.EbxSubSessioniFrame.Close_Button' at Center.", repo.EBXApplication.CustomerAddressTab.EbxSubSessioniFrame.Close_ButtonInfo, new RecordItemIndex(3));
            repo.EBXApplication.CustomerAddressTab.EbxSubSessioniFrame.Close_Button.Click();
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
