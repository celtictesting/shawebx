﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.CustomerContactsTab
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The AddContactContact recording.
    /// </summary>
    [TestModule("263e2495-e223-49c5-94dc-50dd0cf2ebff", ModuleType.Recording, 1)]
    public partial class AddContactContact : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static AddContactContact instance = new AddContactContact();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public AddContactContact()
        {
            ContactContact = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static AddContactContact Instance
        {
            get { return instance; }
        }

#region Variables

        /// <summary>
        /// Gets or sets the value of variable ContactContact.
        /// </summary>
        [TestVariable("3f8a78ea-8387-4b2a-a12b-e39688decedd")]
        public string ContactContact
        {
            get { return repo.ContactContact; }
            set { repo.ContactContact = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.Address_Type_DropDown_Arrow' at Center.", repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.Address_Type_DropDown_ArrowInfo, new RecordItemIndex(0));
            repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.Address_Type_DropDown_Arrow.Click();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.Contact_Person_Field' at Center.", repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.Contact_Person_FieldInfo, new RecordItemIndex(1));
            repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.Contact_Person_Field.Click();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'Smith{Return}' with focus on 'EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.Contact_Person_Field'.", repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.Contact_Person_FieldInfo, new RecordItemIndex(2));
            repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.Contact_Person_Field.PressKeys("Smith{Return}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1s.", new RecordItemIndex(3));
            Delay.Duration(1000, false);
            
            UserCodeMethod(repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.Contact_Person_FieldInfo);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.ContactContactDropdown' at CenterLeft.", repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.ContactContactDropdownInfo, new RecordItemIndex(5));
            repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.ContactContactDropdown.Click(Location.CenterLeft);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Get Value", "Getting attribute 'TagValue' from item 'EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.ContactContactDropdown' and assigning its value to variable 'ContactContact'.", repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.ContactContactDropdownInfo, new RecordItemIndex(6));
            ContactContact = repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.ContactContactDropdown.Element.GetAttributeValueText("TagValue");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "User", ContactContact, new RecordItemIndex(7));
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
