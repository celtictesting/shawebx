﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 3/3/2022
 * Time: 2:20 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;


namespace ShawIndustries.Scripts.a.__EBX.GeneralTab
{
    /// <summary>
    /// Description of GetDefaultRemitToAddress.
    /// </summary>
    [TestModule("32FF5E27-D42C-4F67-8839-DC315BFF022A", ModuleType.UserCode, 1)]
    public class GetDefaultRemitToAddress : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public GetDefaultRemitToAddress()
        {
            // Do not delete - a parameterless constructor is required!
        }

  
  string _SubCreditArea = "";
  [TestVariable("e7f1f24e-965e-4321-9053-e181e07a99f8")]
  public string SubCreditArea
  {
  	get { return _SubCreditArea; }
  	set { _SubCreditArea = value; }
  }
  

string _RemitCode = "";
[TestVariable("3c9e064b-f6f2-4981-b467-2977d0a63585")]
public string RemitCode
{
	get { return _RemitCode; }
	set { _RemitCode = value; }
}


string _RemitBoxNumber = "";
[TestVariable("f902be7d-f366-43a6-9c45-229065ef04fc")]
public string RemitBoxNumber
{
	get { return _RemitBoxNumber; }
	set { _RemitBoxNumber = value; }
}


string _RemitStreetAddress = "";
[TestVariable("5518e928-2495-478d-bb30-cca1b2799bb9")]
public string RemitStreetAddress
{
	get { return _RemitStreetAddress; }
	set { _RemitStreetAddress = value; }
}


string _DefaultRemitToAddress = "";
[TestVariable("5ef57008-180c-4bc4-9ef4-addebc482b99")]
public string DefaultRemitToAddress
{
	get { return _DefaultRemitToAddress; }
	set { _DefaultRemitToAddress = value; }
}


        
        
        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
           	DBConnector dbConnector = new DBConnector();
           	
            SubCreditArea = SubCreditArea.Split(new string[] {"("}, StringSplitOptions.None)[1].Split(')')[0].Trim();
    		
    		string query = "SELECT a.RemittoAddress_Code, a.RemittoAddress_BoxNumber, a.RemittoAddress_CityStreet from EBX_Replicated_Ref_RemittoAddress a  join " 
    			+ "EBX_Replicated_Ref_SubCreditArea b on a.RemittoAddress_Code = b.SubCreditArea_RemitToCode where b.SubCreditArea_CreditAreaNumber = '" + SubCreditArea + "'";
    	
    		Report.Info("SQL Query: " + query);
    		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
    		
    		RemitToAddressTable referenceTables = new RemitToAddressTable(dt);

    		RemitCode = referenceTables.InventoryTable[0].ToString();
    		RemitBoxNumber = referenceTables.InventoryTable[1].ToString();
    		RemitStreetAddress = referenceTables.InventoryTable[2].ToString();
    		DefaultRemitToAddress = (RemitCode + " - " + RemitBoxNumber + " - " + RemitStreetAddress);
    		
			Report.Info("Info", "Default Remit to Address for Sub-Credit Area ( " + SubCreditArea + " ) is :   " + DefaultRemitToAddress);

    		
    		
        }
    }
}
