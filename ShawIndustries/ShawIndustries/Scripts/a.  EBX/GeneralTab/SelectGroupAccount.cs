﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.GeneralTab
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The SelectGroupAccount recording.
    /// </summary>
    [TestModule("ae30fca8-78d4-4d0b-adb2-d3512699bc33", ModuleType.Recording, 1)]
    public partial class SelectGroupAccount : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static SelectGroupAccount instance = new SelectGroupAccount();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public SelectGroupAccount()
        {
            GroupAccount = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static SelectGroupAccount Instance
        {
            get { return instance; }
        }

#region Variables

        /// <summary>
        /// Gets or sets the value of variable GroupAccount.
        /// </summary>
        [TestVariable("8b77e291-db05-4348-8f6c-85b448e3eaeb")]
        public string GroupAccount
        {
            get { return repo.GroupAccount; }
            set { repo.GroupAccount = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Invoke action", "Invoking WaitForDocumentLoaded() on item 'EBXApplication'.", repo.EBXApplication.SelfInfo, new RecordItemIndex(0));
            repo.EBXApplication.Self.WaitForDocumentLoaded();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.MainTab.EbxSubSessioniFrame.GroupAccountDropdownIcon' at Center.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.GroupAccountDropdownIconInfo, new RecordItemIndex(1));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.GroupAccountDropdownIcon.Click();
            Delay.Milliseconds(0);
            
            UserCodeMethod(repo.EBXApplication.GeneralTab.GroupAccountInfo);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.MainTab.EbxSubSessioniFrame.RandomGroupAccount' at CenterLeft.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.RandomGroupAccountInfo, new RecordItemIndex(3));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.RandomGroupAccount.Click(Location.CenterLeft);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Get Value", "Getting attribute 'TagValue' from item 'EBXApplication.MainTab.EbxSubSessioniFrame.GroupAccount' and assigning its value to variable 'GroupAccount'.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.GroupAccountInfo, new RecordItemIndex(4));
            GroupAccount = repo.EBXApplication.MainTab.EbxSubSessioniFrame.GroupAccount.Element.GetAttributeValueText("TagValue");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "User", GroupAccount, new RecordItemIndex(5));
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
