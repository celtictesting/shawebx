﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.DataBaseScripts
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Sissp492StoreProcedure recording.
    /// </summary>
    [TestModule("9250ba68-b690-4718-8dd6-383dd5827cf7", ModuleType.Recording, 1)]
    public partial class Sissp492StoreProcedure : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static Sissp492StoreProcedure instance = new Sissp492StoreProcedure();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Sissp492StoreProcedure()
        {
            outputResponse = "";
            customerNumber = "3500639";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Sissp492StoreProcedure Instance
        {
            get { return instance; }
        }

#region Variables

        string _outputResponse;

        /// <summary>
        /// Gets or sets the value of variable outputResponse.
        /// </summary>
        [TestVariable("48cdcac2-a6b2-489f-9770-776dc677b34c")]
        public string outputResponse
        {
            get { return _outputResponse; }
            set { _outputResponse = value; }
        }

        string _customerNumber;

        /// <summary>
        /// Gets or sets the value of variable customerNumber.
        /// </summary>
        [TestVariable("4484886c-2af3-43c6-b23c-e45cec71d355")]
        public string customerNumber
        {
            get { return _customerNumber; }
            set { _customerNumber = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            outputResponse = UserCodeCollection.DB2UserCodes.Sissp492StoreProc(customerNumber);
            Delay.Milliseconds(0);
            
            UserCodeCollection.DataValidationCodes.ValidateTwoStringsMatch(outputResponse, "DATA RETRIEVED SUCCESSFULLY");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
