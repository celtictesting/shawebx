﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 10/27/2021
 * Time: 2:36 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;

using ShawIndustries.Model;
using ShawIndustries.Service;

namespace ShawIndustries.Scripts.a.__EBX.DataBaseScripts
{
    /// <summary>
    /// Description of CaptureTerritoryInformation.
    /// </summary>
    [TestModule("DA952BA4-53B9-4896-AF77-8899B46EAB86", ModuleType.UserCode, 1)]
    public class CaptureTerritoryInformation : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public CaptureTerritoryInformation()
        {
            // Do not delete - a parameterless constructor is required!
        }

string _TerritoryCode = "";
[TestVariable("43d217c2-27ee-48fd-8377-b9844d164641")]
public string TerritoryCode
{
	get { return _TerritoryCode; }
	set { _TerritoryCode = value; }
}


string _TerritoryName = "";
[TestVariable("0f67ec59-1bcc-4004-9f9e-e742a735c814")]
public string TerritoryName
{
	get { return _TerritoryName; }
	set { _TerritoryName = value; }
}

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
    		DBConnector dbConnector = new DBConnector();
    		
    		string query = "Select Top 1 TerritoryManager_code, TerritoryManager_name from EBXDatabaseQA.dbo.EBX_Replicated_Ref_TerritoryManage where T_CREATION_DATE > Convert(datetime,'2021-01-01') Order By NEWID()";

    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
      		
    		TerritoryData referenceTables = new TerritoryData(dt);

    		TerritoryCode = referenceTables.InventoryTable[0].ToString();
    		TerritoryName = referenceTables.InventoryTable[1].ToString();
    		
			Report.Info("Info", "Random Territory Code:  " + TerritoryCode);
			Report.Info("Info", "Random Territory Name:  " + TerritoryName);

        }

    }
}
           