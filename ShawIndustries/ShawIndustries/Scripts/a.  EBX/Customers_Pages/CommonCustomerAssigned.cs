﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 4/7/2022
 * Time: 3:20 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Service;
using ShawIndustries.Model;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;

namespace ShawIndustries.Scripts.a.__EBX.Customers_Pages
{
    /// <summary>
    /// Description of CommonCustomerAssigned.
    /// </summary>
    [TestModule("20BAF4AD-93DD-4722-B051-6F18BFFBF35D", ModuleType.UserCode, 1)]
    public class CommonCustomerAssigned : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public CommonCustomerAssigned()
        {
            // Do not delete - a parameterless constructor is required!
        }

  
  string _CommonCustomerId = "";
  [TestVariable("ba1a4d94-34c1-44a3-bede-86eee9a03ce5")]
  public string CommonCustomerId
  {
  	get { return _CommonCustomerId; }
  	set { _CommonCustomerId = value; }
  }
        
        
        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
            DBConnector dbConnector = new DBConnector();
            string query = "Select TOP 1 Id from dbo.EBX_Replicated_Customer where Main_Name like 'Automation%' and Main_Parent_ is not null Order By NEWID()";
    		Report.Info("SQL Query Used: " + query);
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
      		
    		SellingCompany referenceTables = new SellingCompany(dt);

    		CommonCustomerId = referenceTables.InventoryTable[0].ToString();
			Report.Info("Info", "Random Customer that already has an assigned Common Customer: " + CommonCustomerId);
    		
    		
        }
    }
}
