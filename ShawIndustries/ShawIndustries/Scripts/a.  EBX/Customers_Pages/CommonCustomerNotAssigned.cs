﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/23/2022
 * Time: 2:54 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Service;
using ShawIndustries.Model;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;

namespace ShawIndustries.Scripts.a.__EBX.Customers_Pages
{
    /// <summary>
    /// Description of CommonCustomerNotAssigned.
    /// </summary>
    [TestModule("99999BEE-FA26-401E-8825-89660DF95E0D", ModuleType.UserCode, 1)]
    public class CommonCustomerNotAssigned : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public CommonCustomerNotAssigned()
        {
            // Do not delete - a parameterless constructor is required!
        }

string _CommonCustomerID = "";
[TestVariable("cac01b9e-54e1-4bb1-a856-6af2672080a8")]
public string CommonCustomerID
{
	get { return _CommonCustomerID; }
	set { _CommonCustomerID = value; }
}

string _customerNumber = "";
[TestVariable("2464b77e-f014-4bfe-a641-5714fc578421")]
public string customerNumber
{
	get { return _customerNumber; }
	set { _customerNumber = value; }
}

string _CurrencyCodeValue = "";
[TestVariable("da7f6bd4-29d3-46f1-88f2-e95098d248b6")]
public string CurrencyCodeValue
{
	get { return _CurrencyCodeValue; }
	set { _CurrencyCodeValue = value; }
}

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
            CurrencyCodeValue = CurrencyCodeValue.Split('-')[0].TrimEnd();
            DBConnector dbConnector = new DBConnector();
            string query = "Select TOP 1 Id from dbo.EBX_Replicated_Customer where Main_Name like 'Automation%' and Main_Parent_ is null and Main_CustomerNumber != '" + customerNumber + "' and CompanyInformation_PricingFormula_CurrencyCode_ != '" + CurrencyCodeValue + "' Order By NEWID()";
    		Report.Info("SQL Query Used: " + query);
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
      		
    		SellingCompany referenceTables = new SellingCompany(dt);

    		_CommonCustomerID = referenceTables.InventoryTable[0].ToString();
			Report.Info("Info", "Random Customer that does not have an assigned Common Customer: " + _CommonCustomerID);
    		
            
        }
    }
}
