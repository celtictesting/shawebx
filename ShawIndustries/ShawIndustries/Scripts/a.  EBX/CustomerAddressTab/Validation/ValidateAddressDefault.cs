﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.CustomerAddressTab.Validation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The ValidateAddressDefault recording.
    /// </summary>
    [TestModule("cf501cc2-5591-41d4-a96f-bb46bc2518dd", ModuleType.Recording, 1)]
    public partial class ValidateAddressDefault : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static ValidateAddressDefault instance = new ValidateAddressDefault();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public ValidateAddressDefault()
        {
            PhysicalAddress = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static ValidateAddressDefault Instance
        {
            get { return instance; }
        }

#region Variables

        /// <summary>
        /// Gets or sets the value of variable PhysicalAddress.
        /// </summary>
        [TestVariable("64a78d13-7d12-413e-8bb4-718c99442a8a")]
        public string PhysicalAddress
        {
            get { return repo.PhysicalAddress; }
            set { repo.PhysicalAddress = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'EBXApplication.CustomerAddressTab.EbxSubSessioniFrame.AddressFirstLine'.", repo.EBXApplication.CustomerAddressTab.EbxSubSessioniFrame.AddressFirstLineInfo, new RecordItemIndex(0));
            Validate.Exists(repo.EBXApplication.CustomerAddressTab.EbxSubSessioniFrame.AddressFirstLineInfo);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Get Value", "Getting attribute 'InnerText' from item 'EBXApplication.CustomerAddressTab.EbxSubSessioniFrame.SalesforcePhysicalAddress' and assigning its value to variable 'PhysicalAddress'.", repo.EBXApplication.CustomerAddressTab.EbxSubSessioniFrame.SalesforcePhysicalAddressInfo, new RecordItemIndex(1));
            PhysicalAddress = repo.EBXApplication.CustomerAddressTab.EbxSubSessioniFrame.SalesforcePhysicalAddress.Element.GetAttributeValueText("InnerText");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "User", "Salesforce Physical Address:", new RecordItemIndex(2));
            
            Report.Log(ReportLevel.Info, "User", PhysicalAddress, new RecordItemIndex(3));
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
