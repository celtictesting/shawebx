﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.CustomerAddressTab
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Get_Mailing_Address recording.
    /// </summary>
    [TestModule("32292009-25b4-46b8-8757-c85737e91e1e", ModuleType.Recording, 1)]
    public partial class Get_Mailing_Address : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static Get_Mailing_Address instance = new Get_Mailing_Address();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Get_Mailing_Address()
        {
            address = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Get_Mailing_Address Instance
        {
            get { return instance; }
        }

#region Variables

        string _address;

        /// <summary>
        /// Gets or sets the value of variable address.
        /// </summary>
        [TestVariable("738e03b2-bf65-40a4-89a5-30882d7985af")]
        public string address
        {
            get { return _address; }
            set { _address = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Get Value", "Getting attribute 'InnerText' from item 'EBXApplication.MainTab.EbxSubSessioniFrame.Mailing_Address_Value' and assigning its value to variable 'address'.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.Mailing_Address_ValueInfo, new RecordItemIndex(0));
            address = repo.EBXApplication.MainTab.EbxSubSessioniFrame.Mailing_Address_Value.Element.GetAttributeValueText("InnerText");
            Delay.Milliseconds(0);
            
            Trim();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "User", "Mailing Address:", new RecordItemIndex(2));
            
            Report.Log(ReportLevel.Info, "User", address, new RecordItemIndex(3));
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
