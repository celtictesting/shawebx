﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.CustomerAddressTab
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Enter_Your_Address_Random recording.
    /// </summary>
    [TestModule("9dfe36a5-9100-4427-9dd4-5b0103771db6", ModuleType.Recording, 1)]
    public partial class Enter_Your_Address_Random : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static Enter_Your_Address_Random instance = new Enter_Your_Address_Random();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Enter_Your_Address_Random()
        {
            address = "555";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Enter_Your_Address_Random Instance
        {
            get { return instance; }
        }

#region Variables

        string _address;

        /// <summary>
        /// Gets or sets the value of variable address.
        /// </summary>
        [TestVariable("6be31a72-598f-4523-b288-2fe3f8c0d9c0")]
        public string address
        {
            get { return _address; }
            set { _address = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Invoke action", "Invoking WaitForDocumentLoaded() on item 'EBXApplication'.", repo.EBXApplication.SelfInfo, new RecordItemIndex(0));
            repo.EBXApplication.Self.WaitForDocumentLoaded();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication' at LowerRight.", repo.EBXApplication.SelfInfo, new RecordItemIndex(1));
            repo.EBXApplication.Self.Click(Location.LowerRight);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.MainTab.EbxSubSessioniFrame.Enter_Your_Address_Field' at Center.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.Enter_Your_Address_FieldInfo, new RecordItemIndex(2));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.Enter_Your_Address_Field.Click();
            Delay.Milliseconds(0);
            
            CreateRandomAddress();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$address' with focus on 'EBXApplication.MainTab.EbxSubSessioniFrame.Enter_Your_Address_Field'.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.Enter_Your_Address_FieldInfo, new RecordItemIndex(4));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.Enter_Your_Address_Field.PressKeys(address);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Wait", "Waiting 5s for the attribute 'Visible' to equal the specified value 'True'. Associated repository item: 'EBXApplication.MainTab.EbxSubSessioniFrame.Random_Address'", repo.EBXApplication.MainTab.EbxSubSessioniFrame.Random_AddressInfo, new RecordItemIndex(5));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.Random_AddressInfo.WaitForAttributeEqual(5000, "Visible", "True");
            
            //Report.Log(ReportLevel.Info, "Delay", "Waiting for 300ms.", new RecordItemIndex(6));
            //Delay.Duration(300, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Move item 'EBXApplication.MainTab.EbxSubSessioniFrame.Random_Address' at Center.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.Random_AddressInfo, new RecordItemIndex(7));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.Random_Address.MoveTo();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.MainTab.EbxSubSessioniFrame.Random_Address' at Center.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.Random_AddressInfo, new RecordItemIndex(8));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.Random_Address.Click();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Get Value", "Getting attribute 'Value' from item 'EBXApplication.MainTab.EbxSubSessioniFrame.Enter_Your_Address_Field' and assigning its value to variable 'address'.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.Enter_Your_Address_FieldInfo, new RecordItemIndex(9));
            address = repo.EBXApplication.MainTab.EbxSubSessioniFrame.Enter_Your_Address_Field.Element.GetAttributeValueText("Value");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "User", "Address Entered:", new RecordItemIndex(10));
            
            Report.Log(ReportLevel.Info, "User", address, new RecordItemIndex(11));
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
