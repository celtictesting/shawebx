﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.CustomerAddressTab
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The CaliforniaAddress recording.
    /// </summary>
    [TestModule("df90a6d5-a8b2-4efc-a0cd-9101339b220b", ModuleType.Recording, 1)]
    public partial class CaliforniaAddress : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static CaliforniaAddress instance = new CaliforniaAddress();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public CaliforniaAddress()
        {
            PhysicalAddress = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static CaliforniaAddress Instance
        {
            get { return instance; }
        }

#region Variables

        /// <summary>
        /// Gets or sets the value of variable PhysicalAddress.
        /// </summary>
        [TestVariable("ce916436-babe-4648-b018-9f314c04e011")]
        public string PhysicalAddress
        {
            get { return repo.PhysicalAddress; }
            set { repo.PhysicalAddress = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Keyboard", "Key 'Ctrl+A' Press with focus on 'EBXApplication.MainTab.EbxSubSessioniFrame.Address_Field_Value'.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.Address_Field_ValueInfo, new RecordItemIndex(0));
            Keyboard.PrepareFocus(repo.EBXApplication.MainTab.EbxSubSessioniFrame.Address_Field_Value);
            Keyboard.Press(System.Windows.Forms.Keys.A | System.Windows.Forms.Keys.Control, Keyboard.DefaultScanCode, Keyboard.DefaultKeyPressTime, 1, true);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key 'Delete' Press with focus on 'EBXApplication.MainTab.EbxSubSessioniFrame.Address_Field_Value'.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.Address_Field_ValueInfo, new RecordItemIndex(1));
            Keyboard.PrepareFocus(repo.EBXApplication.MainTab.EbxSubSessioniFrame.Address_Field_Value);
            Keyboard.Press(System.Windows.Forms.Keys.Delete, Keyboard.DefaultScanCode, Keyboard.DefaultKeyPressTime, 1, true);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 300ms.", new RecordItemIndex(2));
            Delay.Duration(300, false);
            
            UserCodeMethod(repo.EBXApplication.CustomerAddressTab.EbxSubSessioniFrame.Address_Field_ValueInfo);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$PhysicalAddress' with focus on 'EBXApplication.MainTab.EbxSubSessioniFrame.Address_Field_Value'.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.Address_Field_ValueInfo, new RecordItemIndex(4));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.Address_Field_Value.PressKeys(PhysicalAddress);
            Delay.Milliseconds(20);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Return}' with focus on 'EBXApplication.MainTab.EbxSubSessioniFrame.Address_Field_Value'.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.Address_Field_ValueInfo, new RecordItemIndex(5));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.Address_Field_Value.PressKeys("{Return}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 300ms.", new RecordItemIndex(6));
            Delay.Duration(300, false);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Down}{Down}{Down}' with focus on 'EBXApplication.MainTab.EbxSubSessioniFrame.Address_Field_Value'.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.Address_Field_ValueInfo, new RecordItemIndex(7));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.Address_Field_Value.PressKeys("{Down}{Down}{Down}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Return}' with focus on 'EBXApplication.MainTab.EbxSubSessioniFrame.Address_Field_Value'.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.Address_Field_ValueInfo, new RecordItemIndex(8));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.Address_Field_Value.PressKeys("{Return}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 300ms.", new RecordItemIndex(9));
            Delay.Duration(300, false);
            
            Report.Log(ReportLevel.Info, "Get Value", "Getting attribute 'Value' from item 'EBXApplication.CustomerAddressTab.EbxSubSessioniFrame.Address_Field_Value' and assigning its value to variable 'PhysicalAddress'.", repo.EBXApplication.CustomerAddressTab.EbxSubSessioniFrame.Address_Field_ValueInfo, new RecordItemIndex(10));
            PhysicalAddress = repo.EBXApplication.CustomerAddressTab.EbxSubSessioniFrame.Address_Field_Value.Element.GetAttributeValueText("Value");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "User", PhysicalAddress, new RecordItemIndex(11));
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.Address_Type_DropDown_Arrow' at Center.", repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.Address_Type_DropDown_ArrowInfo, new RecordItemIndex(12));
            repo.EBXApplication.CustomerContactsTab.EbxSubSessioniFrame.Address_Type_DropDown_Arrow.Click();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.CustomerAddressTab.EbxSubSessioniFrame.PhysicalAddress' at Center.", repo.EBXApplication.CustomerAddressTab.EbxSubSessioniFrame.PhysicalAddressInfo, new RecordItemIndex(13));
            repo.EBXApplication.CustomerAddressTab.EbxSubSessioniFrame.PhysicalAddress.Click();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.MainTab.EbxSubSessioniFrame.Save_And_Close_Button1' at Center.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.Save_And_Close_Button1Info, new RecordItemIndex(14));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.Save_And_Close_Button1.Click();
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
