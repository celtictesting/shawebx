﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// Your custom recording code should go in this file.
// The designer will only add methods to this file, so your custom code won't be overwritten.
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;

namespace ShawIndustries.Scripts.a.@__EBX.CustomerAddressTab
{
    public partial class UpdateMailingAddress
    {
        /// <summary>
        /// This method gets called right after the recording has been started.
        /// It can be used to execute recording specific initialization code.
        /// </summary>
        private void Init()
        {
            // Your recording specific initialization code goes here.
        }

        public void UserCodeMethod1(RepoItemInfo trtaginfo)
        {
            var ddrandom = new Random();
        	/*
			var tdlist = new List<int>{0,1,2,3,4,5,6,7,8,9,10};
			int index = ddrandom.Next(14,24);
			MailingAddress = "ebx_ISS_Item_" + index;
			*/
			MailingAddress = ddrandom.Next(100, 999).ToString();
			Report.Info("MailingAddress Number: " + MailingAddress);
        }

    }
}
