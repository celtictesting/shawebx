﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.Validation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Validate_Customer_Ref_Number recording.
    /// </summary>
    [TestModule("8ebae8fe-ef87-492b-adb7-4b12a278d4cc", ModuleType.Recording, 1)]
    public partial class Validate_Customer_Ref_Number : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static Validate_Customer_Ref_Number instance = new Validate_Customer_Ref_Number();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Validate_Customer_Ref_Number()
        {
            customerReferenceNumber = "101 - ETHAN ALLEN                                       ";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Validate_Customer_Ref_Number Instance
        {
            get { return instance; }
        }

#region Variables

        /// <summary>
        /// Gets or sets the value of variable customerReferenceNumber.
        /// </summary>
        [TestVariable("716a3249-751d-4e96-b5b5-6988de646a61")]
        public string customerReferenceNumber
        {
            get { return repo.CustomerReferenceNumber; }
            set { repo.CustomerReferenceNumber = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Invoke action", "Invoking WaitForDocumentLoaded() on item 'EBXApplication'.", repo.EBXApplication.SelfInfo, new RecordItemIndex(0));
            repo.EBXApplication.Self.WaitForDocumentLoaded();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeContains (TagValue>$customerReferenceNumber) on item 'EBXApplication.MainTab.CustomerReferenceField'.", repo.EBXApplication.MainTab.CustomerReferenceFieldInfo, new RecordItemIndex(1));
            Validate.AttributeContains(repo.EBXApplication.MainTab.CustomerReferenceFieldInfo, "TagValue", customerReferenceNumber);
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
