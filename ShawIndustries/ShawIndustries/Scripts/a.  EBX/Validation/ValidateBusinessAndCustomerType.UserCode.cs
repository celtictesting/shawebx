﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// Your custom recording code should go in this file.
// The designer will only add methods to this file, so your custom code won't be overwritten.
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;

namespace ShawIndustries.Scripts.a.@__EBX.Validation
{
	public partial class ValidateBusinessAndCustomerType
	{
		/// <summary>
		/// This method gets called right after the recording has been started.
		/// It can be used to execute recording specific initialization code.
		/// </summary>
		private void Init()
		{
			// Your recording specific initialization code goes here.
		}

		public void Validate_INTERNAL(RepoItemInfo divtagInfo)
		{
			string ValInternal;
			
			//            Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (InnerText=$CustomerType) on item 'divtagInfo'.", divtagInfo);
			//            Validate.AttributeEqual(divtagInfo, "InnerText", CustomerType, null, false);
			ValInternal = repo.EBXApplication.MainTab.EbxSubSessioniFrame.INTERNAL.InnerText.Trim();
			
			if (ValInternal == CustomerType)
			{
				Report.Log(ReportLevel.Success, " Validation", "(Optional Action)\r\nValidating AttributeEqual (InnerText=$CustomerType) on item 'divtagInfo'.", divtagInfo);
			}
		}

        public void Validate_INTERNInternal(RepoItemInfo divtagInfo)
        {
        	string ValBusinessType;
        	 
        	ValBusinessType = repo.EBXApplication.MainTab.EbxSubSessioniFrame.INTERNInternal.InnerText.Trim();
        	if (ValBusinessType == BusinessType)
        	{
            Report.Log(ReportLevel.Success, "Validation", "(Optional Action)\r\nValidating AttributeEqual (InnerText=$BusinessType) on item 'divtagInfo'.", divtagInfo);
        	}
        }

	}
}
