﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.Validation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The ValidateClaimsAreaDropdown recording.
    /// </summary>
    [TestModule("7c11ad09-c143-4576-8e45-e72e4474918a", ModuleType.Recording, 1)]
    public partial class ValidateClaimsAreaDropdown : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static ValidateClaimsAreaDropdown instance = new ValidateClaimsAreaDropdown();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public ValidateClaimsAreaDropdown()
        {
            tableName = "ClaimsArea";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static ValidateClaimsAreaDropdown Instance
        {
            get { return instance; }
        }

#region Variables

        /// <summary>
        /// Gets or sets the value of variable tableName.
        /// </summary>
        [TestVariable("90ff324a-8307-42a1-9bfa-71a0ecee107c")]
        public string tableName
        {
            get { return repo.tableName; }
            set { repo.tableName = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Invoke action", "Invoking WaitForDocumentLoaded() on item 'EBXApplication'.", repo.EBXApplication.SelfInfo, new RecordItemIndex(0));
            repo.EBXApplication.Self.WaitForDocumentLoaded();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'EBXApplication.MainTab.ClaimsAreaIcon'.", repo.EBXApplication.MainTab.ClaimsAreaIconInfo, new RecordItemIndex(1));
            Validate.Exists(repo.EBXApplication.MainTab.ClaimsAreaIconInfo);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key 'Next' Press.", new RecordItemIndex(2));
            Keyboard.Press(System.Windows.Forms.Keys.Next, Keyboard.DefaultScanCode, Keyboard.DefaultKeyPressTime, 1, true);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking EnsureVisible() on item 'EBXApplication.MainTab.ClaimsAreaIcon'.", repo.EBXApplication.MainTab.ClaimsAreaIconInfo, new RecordItemIndex(3));
            repo.EBXApplication.MainTab.ClaimsAreaIcon.EnsureVisible();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.MainTab.ClaimsAreaIcon' at Center.", repo.EBXApplication.MainTab.ClaimsAreaIconInfo, new RecordItemIndex(4));
            repo.EBXApplication.MainTab.ClaimsAreaIcon.Click();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Move item 'EBXApplication.MainTab.DropDownOptionsExcludNotDefined' at Center.", repo.EBXApplication.MainTab.DropDownOptionsExcludNotDefinedInfo, new RecordItemIndex(5));
            repo.EBXApplication.MainTab.DropDownOptionsExcludNotDefined.MoveTo();
            Delay.Milliseconds(0);
            
            UserCodeCollection.RandonDropDownSelection.scrollPageDownSecs(ValueConverter.ArgumentFromString<int>("seconds", "20"));
            Delay.Milliseconds(0);
            
            UserCodeCollection.SQLDBUserCodes.getClaimsAreaTableDB(repo.EBXApplication.MainTab.DropDownOptionsExcludNotDefinedInfo, tableName);
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
