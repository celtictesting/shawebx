﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.Mailings_Tab
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The AddOneMailingEmail recording.
    /// </summary>
    [TestModule("8a00cef0-560a-44ce-8a4b-2180559a9d21", ModuleType.Recording, 1)]
    public partial class AddOneMailingEmail : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static AddOneMailingEmail instance = new AddOneMailingEmail();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public AddOneMailingEmail()
        {
            emailAddress = "AutomationTest@testing";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static AddOneMailingEmail Instance
        {
            get { return instance; }
        }

#region Variables

        string _emailAddress;

        /// <summary>
        /// Gets or sets the value of variable emailAddress.
        /// </summary>
        [TestVariable("fed81944-f85f-47f3-b61c-4cbb18633ef5")]
        public string emailAddress
        {
            get { return _emailAddress; }
            set { _emailAddress = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.MainTab.EbxSubSessioniFrame.Add_Email_Plus_Icon' at Center.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.Add_Email_Plus_IconInfo, new RecordItemIndex(0));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.Add_Email_Plus_Icon.Click();
            Delay.Milliseconds(0);
            
            CreateEmailAddress();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.MainTab.EbxSubSessioniFrame.Email_Address_Field_in_Mailings' at Center.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.Email_Address_Field_in_MailingsInfo, new RecordItemIndex(2));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.Email_Address_Field_in_Mailings.Click();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$emailAddress' with focus on 'EBXApplication.MainTab.EbxSubSessioniFrame.Email_Address_Field_in_Mailings'.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.Email_Address_Field_in_MailingsInfo, new RecordItemIndex(3));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.Email_Address_Field_in_Mailings.PressKeys(emailAddress);
            Delay.Milliseconds(20);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
