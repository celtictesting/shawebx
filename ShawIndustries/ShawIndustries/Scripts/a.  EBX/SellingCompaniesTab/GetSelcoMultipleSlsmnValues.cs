﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/4/2022
 * Time: 10:51 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of GetSelcoMultipleSlsmnValues.
    /// </summary>
    [TestModule("E9659A1B-C865-4190-A6F8-A8725316BFB9", ModuleType.UserCode, 1)]
    public class GetSelcoMultipleSlsmnValues : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public GetSelcoMultipleSlsmnValues()
        {
            // Do not delete - a parameterless constructor is required!
        }

  
  string _DB2SellCustNbr = "";
  [TestVariable("ea246791-9601-41b6-baa4-68f5180179fb")]
  public string DB2SellCustNbr
  {
  	get { return _DB2SellCustNbr; }
  	set { _DB2SellCustNbr = value; }
  }
        
        
        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
            
           	DBConnector dbConnector = new DBConnector();

    		string query = "SELECT a.CUST_NBR, COUNT(b.CUST_NBR), RAND() as IDX FROM SHXP.CUSTOMER_MASTER a JOIN SHXP.CUSTOMER_SLSMN b ON a.CUST_NBR = b.CUST_NBR where CUST_NAME LIKE '%WHITESTONE%' AND CUST_STATUS NOT LIKE 'D' GROUP BY a.CUST_NBR HAVING Count(b.CUST_NBR) >1 ORDER BY IDX FETCH FIRST ROW ONLY";
    		
    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToDB2DataBase(query);
      		
    		SellingCompany referenceTables = new SellingCompany(dt);
    		
    		DB2SellCustNbr = referenceTables.InventoryTable[0].ToString();
    		
			Report.Info("Info", "Random DB2 Customer Number for Selco Testing:  " + DB2SellCustNbr);
            
        }
    }
}
