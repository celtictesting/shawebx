﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 4/22/2022
 * Time: 11:07 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of GetRandomSelcoCustNbr.
    /// </summary>
    [TestModule("FB70D366-4742-41A5-892A-5E7D2D0CB1AF", ModuleType.UserCode, 1)]
    public class GetRandomSelcoCustNbr : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public GetRandomSelcoCustNbr()
        {
            // Do not delete - a parameterless constructor is required!
        }


string _DB2SellCustNbr = "";
[TestVariable("7e120b2a-fb2c-42b8-a022-714fba0af6a4")]
public string DB2SellCustNbr
{
	get { return _DB2SellCustNbr; }
	set { _DB2SellCustNbr = value; }
}
        
        
        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
           	DBConnector dbConnector = new DBConnector();

//    		string query = "SELECT CUST_NBR, RAND() as IDX FROM SHXP.CUSTOMER_MASTER where SELCO is not NULL AND CUST_STATUS NOT LIKE 'D' ORDER BY IDX FETCH FIRST ROW ONLY";
    		string query = "SELECT cm.CUST_NBR, RAND() as IDX FROM SHXP.CUSTOMER_MASTER cm JOIN SHXP.CUSTOMER_SLSMN cs ON cm.CUST_NBR =cs.CUST_NBR where CUST_STATUS NOT LIKE 'D' GROUP BY cm.CUST_NBR HAVING count(cs.SELCO)>'1' AND count(cs.SELCO)<'12' ORDER BY IDX FETCH FIRST ROW ONLY";    		

    		Report.Info("SQL Query Used: " + query);
    		DataTable dt = dbConnector.ConnectToDB2DataBase(query);
      		
    		SellingCompany referenceTables = new SellingCompany(dt);
    		
    		DB2SellCustNbr = referenceTables.InventoryTable[0].ToString();
    		
			Report.Info("Info", "Random DB2 Customer Number for Selco Testing:  " + DB2SellCustNbr);

            
        }
    }
}
