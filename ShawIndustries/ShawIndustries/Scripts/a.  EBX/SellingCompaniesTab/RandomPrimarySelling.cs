﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.SellingCompaniesTab
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The RandomPrimarySelling recording.
    /// </summary>
    [TestModule("2c113f6a-8cdc-4c39-950d-4ce921499476", ModuleType.Recording, 1)]
    public partial class RandomPrimarySelling : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static RandomPrimarySelling instance = new RandomPrimarySelling();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public RandomPrimarySelling()
        {
            PrimarySelling = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static RandomPrimarySelling Instance
        {
            get { return instance; }
        }

#region Variables

        /// <summary>
        /// Gets or sets the value of variable PrimarySelling.
        /// </summary>
        [TestVariable("efe4882f-caac-4437-9c41-90829104a678")]
        public string PrimarySelling
        {
            get { return repo.PrimarySelling; }
            set { repo.PrimarySelling = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse scroll Vertical by -240 units.", new RecordItemIndex(0));
            Mouse.ScrollWheel(-240);
            Delay.Milliseconds(300);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking Focus() on item 'EBXApplication.SellingCompanyTab.SalesPrimarySellingDropdownIcon'.", repo.EBXApplication.SellingCompanyTab.SalesPrimarySellingDropdownIconInfo, new RecordItemIndex(1));
            repo.EBXApplication.SellingCompanyTab.SalesPrimarySellingDropdownIcon.Focus();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking PerformClick() on item 'EBXApplication.SellingCompanyTab.SalesPrimarySellingDropdownIcon'.", repo.EBXApplication.SellingCompanyTab.SalesPrimarySellingDropdownIconInfo, new RecordItemIndex(2));
            repo.EBXApplication.SellingCompanyTab.SalesPrimarySellingDropdownIcon.PerformClick();
            Delay.Milliseconds(0);
            
            //Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.SellingCompanyTab.SalesPrimarySellingDropdownIcon' at Center.", repo.EBXApplication.SellingCompanyTab.SalesPrimarySellingDropdownIconInfo, new RecordItemIndex(3));
            //repo.EBXApplication.SellingCompanyTab.SalesPrimarySellingDropdownIcon.Click();
            //Delay.Milliseconds(0);
            
            UserCodeCollection.RandonDropDownSelection.SelectRandomItemFromDropDown(repo.EBXApplication.MainTab.DropDownOptionsExcludNotDefinedInfo);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Get Value", "Getting attribute 'TagValue' from item 'EBXApplication.SellingCompanyTab.SalesPrimarySellingField' and assigning its value to variable 'PrimarySelling'.", repo.EBXApplication.SellingCompanyTab.SalesPrimarySellingFieldInfo, new RecordItemIndex(5));
            PrimarySelling = repo.EBXApplication.SellingCompanyTab.SalesPrimarySellingField.Element.GetAttributeValueText("TagValue");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "User", PrimarySelling, new RecordItemIndex(6));
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
