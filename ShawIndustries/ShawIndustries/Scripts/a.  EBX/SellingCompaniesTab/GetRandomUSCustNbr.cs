﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/19/2022
 * Time: 3:34 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of GetRandomUSCustNbr.
    /// </summary>
    [TestModule("4B83B035-D4E2-448C-91CE-5A5AE2055EEE", ModuleType.UserCode, 1)]
    public class GetRandomUSCustNbr : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public GetRandomUSCustNbr()
        {
            // Do not delete - a parameterless constructor is required!
        }

string _DB2SellCustNbr = "";
[TestVariable("d06cf27d-3ccb-49aa-944d-850571e553e4")]
public string DB2SellCustNbr
{
	get { return _DB2SellCustNbr; }
	set { _DB2SellCustNbr = value; }
}

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;

           	DBConnector dbConnector = new DBConnector();

    		string query = "SELECT a.CUST_NBR, RAND() as IDX FROM SHXP.CUSTOMER_MASTER a JOIN SHXP.CUSTOMER_SLSMN b on a.CUST_NBR = b.CUST_NBR where a.CNTRY_CODE in ('0', '0.01', '0.11') AND a.CUST_STATUS NOT LIKE 'D' ORDER BY IDX FETCH FIRST ROW ONLY";

    		Report.Info("SQL Query Used: " + query);
    		DataTable dt = dbConnector.ConnectToDB2DataBase(query);
      		
    		SellingCompany referenceTables = new SellingCompany(dt);
    		
    		DB2SellCustNbr = referenceTables.InventoryTable[0].ToString();
    		
			Report.Info("Info", "Random DB2 Customer Number for Selco Testing:  " + DB2SellCustNbr);

                        
        }
    }
}
