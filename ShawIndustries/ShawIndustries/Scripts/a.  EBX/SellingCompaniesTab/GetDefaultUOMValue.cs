﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/20/2022
 * Time: 10:52 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of GetUSDefaultUOM.
    /// </summary>
    [TestModule("71C28880-8162-4F06-A218-8C3951D62C3E", ModuleType.UserCode, 1)]
    public class GetDefaultUOMValue : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public GetDefaultUOMValue()
        {
            // Do not delete - a parameterless constructor is required!
        }

string _DefaultUOM = "";
[TestVariable("0811ebd1-f7cc-4cc3-adcb-338614cff4cf")]
public string DefaultUOM
{
	get { return _DefaultUOM; }
	set { _DefaultUOM = value; }
}

string _SellingCompanyCode = "";
[TestVariable("44034b26-b1ab-404b-aa1d-29fb34e69576")]
public string SellingCompanyCode
{
	get { return _SellingCompanyCode; }
	set { _SellingCompanyCode = value; }
}

string _SelcoCountryCode = "";
[TestVariable("985dcf8c-8b7c-4f97-ae1c-9b8562a99c0f")]
public string SelcoCountryCode
{
	get { return _SelcoCountryCode; }
	set { _SelcoCountryCode = value; }
}

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
            DBConnector dbConnector = new DBConnector();
    		string query = "Select unitOfMeasure_ from EBX_Replicated_Ref_DefaultUnitOfMeasure where sellingCompany_='" + SellingCompanyCode + "' and countryCode_='" + SelcoCountryCode + "'";

    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
     		
    	    string MyReader = dt.Rows.Count.ToString();
    	   
     	    if (MyReader == "0")
     	    	{
     	    	Report.Info("Info", "The Selco " + SellingCompanyCode + " is not available for the country code " + SelcoCountryCode + " in the Default Unit of Measure table");
    			DefaultUOM = "1";    			
    			Report.Info("Info", "Default UOM Value set to:  " + DefaultUOM);

     	    	}
     	    else
    	    {    		
    			SellingCompany referenceTables = new SellingCompany(dt);
    			DefaultUOM = referenceTables.InventoryTable[0].ToString();
    			Report.Info("Info", "Default UOM Value:  " + DefaultUOM);
     	    }
        }
    }
}
