﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/4/2022
 * Time: 12:40 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;
using System.Globalization;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of VerifyDB2CustomerSlsmn2.
    /// </summary>
    [TestModule("6FB8FDCA-A49B-4F54-AD88-F6952EB8D7F4", ModuleType.UserCode, 1)]
    public class VerifyDB2CustomerSlsmn2 : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public VerifyDB2CustomerSlsmn2()
        {
            // Do not delete - a parameterless constructor is required!
        }

        
        string _DB2SellCustNbr = "";
        [TestVariable("7af3129e-5a3e-466b-905f-20f3b72df1f2")]
        public string DB2SellCustNbr
        {
        	get { return _DB2SellCustNbr; }
        	set { _DB2SellCustNbr = value; }
        }
        
        string _DB2PrimarySelco = "";
        [TestVariable("256a8fe5-8dd4-416a-8085-f1abf99e84f1")]
        public string DB2PrimarySelco
        {
        	get { return _DB2PrimarySelco; }
        	set { _DB2PrimarySelco = value; }
        }
        
        string _DB2UpdatedPrimarySelco = "";
        [TestVariable("e43f1f88-3d6b-4a81-bdf0-f49b94cda0dc")]
        public string DB2UpdatedPrimarySelco
        {
        	get { return _DB2UpdatedPrimarySelco; }
        	set { _DB2UpdatedPrimarySelco = value; }
        }
        
        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;

           	DBConnector dbConnector = new DBConnector();            
			string query = "SELECT count(*) FROM SHXP.CUSTOMER_SLSMN where CUST_NBR = '" + DB2SellCustNbr + "' and SELCO = '" + DB2PrimarySelco.Split('-')[0].TrimEnd() + "'";
    		
    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToDB2DataBase(query);    		
    		SellingCompany referenceTables = new SellingCompany(dt);
    		
    		string QueryResults = referenceTables.InventoryTable[0].ToString();
    		
    		if (QueryResults=="1")
    				{
					Report.Success("Success","Row count is:  " + QueryResults + ".  Selco value " + DB2PrimarySelco.Split('-')[0].TrimEnd() + " exists in SHXP.CUSTOMER_SLSMN.");
					}
				else
					{
					Report.Failure("Failure","Row count is:  " + QueryResults + ".  Selco value " + DB2PrimarySelco.Split('-')[0].TrimEnd() + " not available in SHXP.CUSTOMER_SLSMN." );
					}							            
        }
    }
}
