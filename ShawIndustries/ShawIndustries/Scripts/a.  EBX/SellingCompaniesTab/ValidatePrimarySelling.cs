﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.SellingCompaniesTab
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The ValidatePrimarySelling recording.
    /// </summary>
    [TestModule("33e4b432-0561-441c-8f4d-ddd4d0e8d80b", ModuleType.Recording, 1)]
    public partial class ValidatePrimarySelling : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static ValidatePrimarySelling instance = new ValidatePrimarySelling();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public ValidatePrimarySelling()
        {
            TerritoryNumber = "yourValue";
            TerritoryCustomerName = "yourValue";
            PrimaryTerritory = "yourValue";
            ScreenTerritory = "yourValue";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static ValidatePrimarySelling Instance
        {
            get { return instance; }
        }

#region Variables

        string _TerritoryCustomerName;

        /// <summary>
        /// Gets or sets the value of variable TerritoryCustomerName.
        /// </summary>
        [TestVariable("277d6ba5-221a-42a5-9c7b-20d78a0d4393")]
        public string TerritoryCustomerName
        {
            get { return _TerritoryCustomerName; }
            set { _TerritoryCustomerName = value; }
        }

        string _ScreenTerritory;

        /// <summary>
        /// Gets or sets the value of variable ScreenTerritory.
        /// </summary>
        [TestVariable("f1612681-749d-498a-af54-043a9568003a")]
        public string ScreenTerritory
        {
            get { return _ScreenTerritory; }
            set { _ScreenTerritory = value; }
        }

        /// <summary>
        /// Gets or sets the value of variable TerritoryNumber.
        /// </summary>
        [TestVariable("e3da1437-dd91-4f8a-959b-6b96e42aef31")]
        public string TerritoryNumber
        {
            get { return repo.TerritoryNumber; }
            set { repo.TerritoryNumber = value; }
        }

        /// <summary>
        /// Gets or sets the value of variable PrimaryTerritory.
        /// </summary>
        [TestVariable("84a58153-d333-4ad4-8fd7-051d0b239a2d")]
        public string PrimaryTerritory
        {
            get { return repo.PrimaryTerritory; }
            set { repo.PrimaryTerritory = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            UserCodeMethod(repo.EBXApplication.SellingCompanyTab.PrimaryTerritoryInfo);
            Delay.Milliseconds(0);
            
            //Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'EBXApplication.SellingCompanyTab.PrimaryTerritory'.", repo.EBXApplication.SellingCompanyTab.PrimaryTerritoryInfo, new RecordItemIndex(1));
            //Validate.Exists(repo.EBXApplication.SellingCompanyTab.PrimaryTerritoryInfo);
            //Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Get Value", "Getting attribute 'InnerText' from item 'EBXApplication.SellingCompanyTab.PrimaryTerritory' and assigning its value to variable 'ScreenTerritory'.", repo.EBXApplication.SellingCompanyTab.PrimaryTerritoryInfo, new RecordItemIndex(2));
            ScreenTerritory = repo.EBXApplication.SellingCompanyTab.PrimaryTerritory.Element.GetAttributeValueText("InnerText");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "User", ScreenTerritory, new RecordItemIndex(3));
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
