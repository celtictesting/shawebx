﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/24/2022
 * Time: 2:52 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;
using System.Globalization;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of VerifyDB2SellingCompanyNotAdded.
    /// </summary>
    [TestModule("A8BD6C79-4648-466F-9BFD-E82D2B0B0E38", ModuleType.UserCode, 1)]
    public class VerifyDB2SellingCompanyNotAdded : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public VerifyDB2SellingCompanyNotAdded()
        {
            // Do not delete - a parameterless constructor is required!
        }

string _DB2SellCustNbr = "";
[TestVariable("a339bc9a-1bf6-4b1c-9550-b713565c00f6")]
public string DB2SellCustNbr
{
	get { return _DB2SellCustNbr; }
	set { _DB2SellCustNbr = value; }
}

string _SelcoSelected = "";
[TestVariable("9a59aa91-eb87-4bd7-afe3-47389fb72ed3")]
public string SelcoSelected
{
	get { return _SelcoSelected; }
	set { _SelcoSelected = value; }
}

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
            
            Delay.Duration(60000);
           	DBConnector dbConnector = new DBConnector();

    		string query = "SELECT * FROM SHXP.CUSTOMER_SLSMN where CUST_NBR = '" + DB2SellCustNbr + "' and SELCO = '" + SelcoSelected + "'";
    		
    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToDB2DataBase(query);
    		
    	    string MyReader = dt.Rows.Count.ToString();
    	   
     	    if (MyReader == "0")
     	    	{
     	    	Report.Failure("Failure", "The Selco " + SelcoSelected + " is not available for Customer " + DB2SellCustNbr + " in the DB2 Table - SHXP.CUSTOMER_SLSMN");
     	    	}
     	    else
     	    {   
     	    	Report.Success("Success", "The Selco " + SelcoSelected + " is available for Customer " + DB2SellCustNbr + " in the DB2 Table - SHXP.CUSTOMER_SLSMN");     	    
     	    }
            
        }
    }
}
