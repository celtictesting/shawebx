﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 2/8/2022
 * Time: 10:32 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of GetRandomSellingCompanyNumber.
    /// </summary>
    [TestModule("541A8797-F0E7-4D29-B01D-701CDD97927A", ModuleType.UserCode, 1)]
    public class GetRandomSellingCompanyNumber : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public GetRandomSellingCompanyNumber()
        {
            // Do not delete - a parameterless constructor is required!
        }

  
  string _SellingCompanyCode = "";
  [TestVariable("277d3416-16b8-4f00-a2da-c5d3c610f85a")]
  public string SellingCompanyCode
  {
  	get { return _SellingCompanyCode; }
  	set { _SellingCompanyCode = value; }
  }
        
        
        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
            DBConnector dbConnector = new DBConnector();
    		string query = "SELECT TOP 1 a.SellingCompany_sellingCompanyCode_ from EBX_Replicated_Ref_SellingCompany a join EBX_Replicated_Ref_TerritoryManage b on a.SellingCompany_territory_ = b.TerritoryManager_code where b.TerritoryManager_name !=' ' Order By NEWID()";

    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
      		
    		SellingCompany referenceTables = new SellingCompany(dt);

    		SellingCompanyCode = referenceTables.InventoryTable[0].ToString();
    		
			Report.Info("Info", "Random Selling Company Code:  " + SellingCompanyCode);

        }
    }
}
