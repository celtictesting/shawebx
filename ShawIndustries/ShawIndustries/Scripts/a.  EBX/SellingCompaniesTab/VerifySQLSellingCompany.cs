﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 4/21/2022
 * Time: 12:17 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;
using System.Globalization;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of VerifySQLSellingCompany.
    /// </summary>
    [TestModule("F69DF071-BFD9-4976-A919-6FE11B5B1B56", ModuleType.UserCode, 1)]
    public class VerifySQLSellingCompany : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public VerifySQLSellingCompany()
        {
            // Do not delete - a parameterless constructor is required!
        }

  
  string _CustomerNumber = "";
  [TestVariable("fc808675-1d2e-4c03-9126-ad6cf5169e36")]
  public string CustomerNumber
  {
  	get { return _CustomerNumber; }
  	set { _CustomerNumber = value; }
  }
  
  string _TerritoryInfo = "";
  [TestVariable("fd1e53d8-965c-48dc-86b5-863ddd8f1ace")]
  public string TerritoryInfo
  {
  	get { return _TerritoryInfo; }
  	set { _TerritoryInfo = value; }
  }
        
     
        string _SellCompanyInfo = "";
        [TestVariable("8788d928-e4f1-490f-8cc8-9316086966b1")]
        public string SellCompanyInfo
        {
        	get { return _SellCompanyInfo; }
        	set { _SellCompanyInfo = value; }
        }

        
        string _UOMInfo = "";
        [TestVariable("2ed37d9f-96f1-4c4d-b310-0378d7f70e62")]
        public string UOMInfo
        {
        	get { return _UOMInfo; }
        	set { _UOMInfo = value; }
        }
        

string _TermsInfo = "";
[TestVariable("3135ee91-9743-49a6-96b9-9d536c10beda")]
public string TermsInfo
{
	get { return _TermsInfo; }
	set { _TermsInfo = value; }
}

string _ReasonCodeInfo = "";
[TestVariable("1322f099-15af-4abe-b239-2a3c63fd0e99")]
public string ReasonCodeInfo
{
	get { return _ReasonCodeInfo; }
	set { _ReasonCodeInfo = value; }
}

string _SellCoExpDate = "";
[TestVariable("4ecddde6-ca1e-4890-91ea-0ffabbb03fe2")]
public string SellCoExpDate
{
	get { return _SellCoExpDate; }
	set { _SellCoExpDate = value; }
}

string _SellingCompanyCode = "";
[TestVariable("fa34bc4c-81d4-454e-a885-4164ffc6fd9e")]
public string SellingCompanyCode
{
	get { return _SellingCompanyCode; }
	set { _SellingCompanyCode = value; }
}

        
        string _SelectedTerritoryInfo = "";
        [TestVariable("44a29885-da8f-4d1c-9eab-e24dabcd6f39")]
        public string SelectedTerritoryInfo
        {
        	get { return _SelectedTerritoryInfo; }
        	set { _SelectedTerritoryInfo = value; }
        }
        
        
        string _TerrUOM = "";
        [TestVariable("350bec1b-07b6-4c78-ad7c-716b27804484")]
        public string TerrUOM
        {
        	get { return _TerrUOM; }
        	set { _TerrUOM = value; }
        }
        
   
   string _TerrEXP = "";
   [TestVariable("f7f06e95-406c-44b2-9e85-48b5a4fae236")]
   public string TerrEXP
   {
   	get { return _TerrEXP; }
   	set { _TerrEXP = value; }
   }
        
        
        string _TerrReasonCode = "";
        [TestVariable("8a47922f-0769-4899-b9c8-e44931ff9a3c")]
        public string TerrReasonCode
        {
        	get { return _TerrReasonCode; }
        	set { _TerrReasonCode = value; }
        }
    
    string _TerrTerms = "";
    [TestVariable("a8ce6d7a-b6e6-4858-ad99-59ba732e0950")]
    public string TerrTerms
    {
    	get { return _TerrTerms; }
    	set { _TerrTerms = value; }
    }
        
        
        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
           	DBConnector dbConnector = new DBConnector();
    		string query = "Select customer_, territory_, sellingCompany_, uom_, terms_, reasonCode_, expirationDate from dbo.EBX_Replicated_CustomerSellingCompany where customer_ = '" + CustomerNumber + "'";

    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
      		
    		SQLSellingCompany referenceTables = new SQLSellingCompany(dt);

    		 CustomerNumber = referenceTables.InventoryTable[0].ToString();
     		 TerritoryInfo = referenceTables.InventoryTable[1].ToString();
    		 SellCompanyInfo = referenceTables.InventoryTable[2].ToString();
    		 UOMInfo = referenceTables.InventoryTable[3].ToString();
     		 TermsInfo = referenceTables.InventoryTable[4].ToString();
    		 ReasonCodeInfo = referenceTables.InventoryTable[5].ToString();
      		 SellCoExpDate = referenceTables.InventoryTable[6].ToString();
      		 

				Validate.AreEqual(TerritoryInfo, SelectedTerritoryInfo.Split('-')[0].TrimEnd());
				Validate.AreEqual(SellCompanyInfo, SellingCompanyCode);
				Validate.AreEqual(UOMInfo, TerrUOM.Split('-')[0].TrimEnd());
				Validate.AreEqual(TermsInfo, TerrTerms.Split('-')[0].TrimEnd());
				Validate.AreEqual(ReasonCodeInfo.Replace(" ",String.Empty), TerrReasonCode.Split('-')[0].TrimEnd());
	
				System.DateTime ExpDate = System.DateTime.Parse(SellCoExpDate);
				string MyFormattedDate = ExpDate.ToString("MM/dd/yyyy");
				Validate.AreEqual(MyFormattedDate, TerrEXP);				
        }
    }
}             
 
