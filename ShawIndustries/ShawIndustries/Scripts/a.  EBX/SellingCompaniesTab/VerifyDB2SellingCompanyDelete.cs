﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 4/26/2022
 * Time: 2:05 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;
using System.Globalization;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of VerifyDB2SellingCompanyDelete.
    /// </summary>
    [TestModule("29FEFBCC-CACD-4853-AB91-16E20F1A4EE7", ModuleType.UserCode, 1)]
    public class VerifyDB2SellingCompanyDelete : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public VerifyDB2SellingCompanyDelete()
        {
            // Do not delete - a parameterless constructor is required!
        }

string _DB2SellCustNbr = "";
[TestVariable("441d2198-ee1c-4f7f-a797-233e791d9dbb")]
public string DB2SellCustNbr
{
	get { return _DB2SellCustNbr; }
	set { _DB2SellCustNbr = value; }
}

string _sellingCompany = "";
[TestVariable("4dcfb3c9-e9b0-4e14-9fd0-156b786eaea4")]
public string sellingCompany
{
	get { return _sellingCompany; }
	set { _sellingCompany = value; }
}

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
            Delay.Duration(60000);
           	DBConnector dbConnector = new DBConnector();

    		string query = "SELECT count(*) FROM SHXP.CUSTOMER_SLSMN where CUST_NBR = '" + DB2SellCustNbr + "' and SELCO = '" + sellingCompany + "'";
    		
    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToDB2DataBase(query);    		
    		SellingCompany referenceTables = new SellingCompany(dt);
    		
    		string QueryResults = referenceTables.InventoryTable[0].ToString();
    		
    		if (QueryResults=="0")
    				{
					Report.Success("Success","Row count is:  " + QueryResults + ".  Selco successfully removed from SHXP.CUSTOMER_SLSMN.");
					}
				else
					{
					Report.Failure("Failure","Row count is:  " + QueryResults + ".  Selco value still exists in SHXP.CUSTOMER_SLSMN." );
					}			
    		
            
        }
    }
}
