﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/24/2022
 * Time: 9:17 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;
using System.Globalization;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of VerifyDB2SellingCompanyNotAdded.
    /// </summary>
    [TestModule("589FECBE-92A0-4569-AA11-1F5C682E0F3E", ModuleType.UserCode, 1)]
    public class VerifyDB2SellingCompanyNotUpdated : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public VerifyDB2SellingCompanyNotUpdated()
        {
            // Do not delete - a parameterless constructor is required!
        }

string _DB2PrimarySelco = "";
[TestVariable("fafe53fb-6e31-404e-8a7f-49a0315d6499")]
public string DB2PrimarySelco
{
	get { return _DB2PrimarySelco; }
	set { _DB2PrimarySelco = value; }
}

string _DB2SellCustNbr = "";
[TestVariable("995768b2-9764-4ae0-a47e-5be78cea2b5e")]
public string DB2SellCustNbr
{
	get { return _DB2SellCustNbr; }
	set { _DB2SellCustNbr = value; }
}

string _DistrictInfo = "";
[TestVariable("bb3e1fc5-012c-48a7-b81f-73e4a3568dcd")]
public string DistrictInfo
{
	get { return _DistrictInfo; }
	set { _DistrictInfo = value; }
}

string _DistrictSelected = "";
[TestVariable("722fd0e4-c047-47a2-a1c6-93b74e4b867e")]
public string DistrictSelected
{
	get { return _DistrictSelected; }
	set { _DistrictSelected = value; }
}

string _PriceTypeInfo = "";
[TestVariable("51e445a7-1664-4dde-965e-57ce9f988261")]
public string PriceTypeInfo
{
	get { return _PriceTypeInfo; }
	set { _PriceTypeInfo = value; }
}

string _RegionInfo = "";
[TestVariable("022ef6f5-4eae-4327-a7ba-bb5cd344594a")]
public string RegionInfo
{
	get { return _RegionInfo; }
	set { _RegionInfo = value; }
}

string _RegionSelected = "";
[TestVariable("8f6f64ef-7c2f-4bfd-adfc-f88f5d8ee75d")]
public string RegionSelected
{
	get { return _RegionSelected; }
	set { _RegionSelected = value; }
}

string _SelcoSelected = "";
[TestVariable("50ed9a78-e7dd-4f67-a0b3-bed503a401c5")]
public string SelcoSelected
{
	get { return _SelcoSelected; }
	set { _SelcoSelected = value; }
}

string _SellCoExpDate = "";
[TestVariable("584a8e37-ba94-4714-bbd7-df5bab299861")]
public string SellCoExpDate
{
	get { return _SellCoExpDate; }
	set { _SellCoExpDate = value; }
}

string _TermsInfo = "";
[TestVariable("171232ba-69b4-4fd3-9ffb-83038df75dfd")]
public string TermsInfo
{
	get { return _TermsInfo; }
	set { _TermsInfo = value; }
}

string _TerrEXP = "";
[TestVariable("48f1efca-f007-475d-b6fc-27c0ddbae6ec")]
public string TerrEXP
{
	get { return _TerrEXP; }
	set { _TerrEXP = value; }
}

string _TerrTerms = "";
[TestVariable("85cbd888-994e-4259-a2cb-21af47c309eb")]
public string TerrTerms
{
	get { return _TerrTerms; }
	set { _TerrTerms = value; }
}

string _TerrUOM = "";
[TestVariable("0fb13016-fa66-4218-b845-e35bf381cae2")]
public string TerrUOM
{
	get { return _TerrUOM; }
	set { _TerrUOM = value; }
}

string _TerritoryInfo = "";
[TestVariable("f763b98e-0fc0-485e-ae07-d6af3b31ed54")]
public string TerritoryInfo
{
	get { return _TerritoryInfo; }
	set { _TerritoryInfo = value; }
}

string _TerritorySelected = "";
[TestVariable("c59d85fb-e8ad-41c7-b86c-a896bf1b85f3")]
public string TerritorySelected
{
	get { return _TerritorySelected; }
	set { _TerritorySelected = value; }
}

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
            
            Delay.Duration(60000);
           	DBConnector dbConnector = new DBConnector();

    		string query = "SELECT SELCO, REGION, DISTRICT, TERRITORY, TERMS_CODE, PRICE_TYPE, DATE FROM SHXP.CUSTOMER_SLSMN where CUST_NBR = '" + DB2SellCustNbr + "' and SELCO = '" + SelcoSelected + "'";
    		
    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToDB2DataBase(query);
    		
    	    string MyReader = dt.Rows.Count.ToString();
    	   
     	    if (MyReader == "0")
     	    	{
     	    	Report.Success("Success", "The Selco " + SelcoSelected + " is not available for Customer " + DB2SellCustNbr + " in the DB2 Table - SHXP.CUSTOMER_SLSMN");
     	    	}
     	    else
    	    {    		
    		
    		SQLSellingCompany referenceTables = new SQLSellingCompany(dt);
    		
    		DB2PrimarySelco = referenceTables.InventoryTable[0].ToString();
     		RegionInfo = referenceTables.InventoryTable[1].ToString();
    		DistrictInfo = referenceTables.InventoryTable[2].ToString();
    		TerritoryInfo = referenceTables.InventoryTable[3].ToString();
     		TermsInfo = referenceTables.InventoryTable[4].ToString();
    		PriceTypeInfo = referenceTables.InventoryTable[5].ToString();
      		SellCoExpDate = referenceTables.InventoryTable[6].ToString();

      		Report.Info("Info", "For the Territory validation -  Database:   " + TerritoryInfo + " Application:   " + TerritorySelected.Split('-')[0].TrimEnd());
    		Validate.AreEqual(TerritoryInfo, TerritorySelected.Split('-')[0].TrimEnd());

      		Report.Info("Info", "For the District validation -  Database:   " + DistrictInfo + " Application:   " + DistrictSelected.Split('-')[0].TrimEnd());      		
			Validate.AreEqual(DistrictInfo, DistrictSelected.Split('-')[0].TrimEnd());  

      		Report.Info("Info", "For the District validation -  Database:   " + RegionInfo + " Application:   " + RegionSelected.Split('-')[0].TrimEnd());      					
      		Validate.AreEqual(RegionInfo, RegionSelected.Split('-')[0].TrimEnd());


			Report.Info("Info", "For the Terms validation -  Database:   " + TermsInfo + " Application:   " + TerrTerms.Split('-')[0].TrimEnd());
			Validate.AreEqual(TermsInfo, TerrTerms.Split('-')[0].TrimEnd());
			
	   		Report.Info("Info", "For the UOM validation -  Database:   " + PriceTypeInfo + " Application:   " + TerrUOM.Split('-')[0].TrimEnd());	
			Validate.AreEqual(PriceTypeInfo, TerrUOM.Split('-')[0].TrimEnd());
			
			if (SellCoExpDate.Length==6)
				{
				SellCoExpDate = SellCoExpDate.Replace(",","");
				string ExpYear = SellCoExpDate.Substring(4,2);
				string ExpDay = SellCoExpDate.Substring(2,2);				
				string ExpMonth =  SellCoExpDate.Substring(0,2);
				string ExpValue =  (ExpMonth + "/" +ExpDay+"/" +ExpYear);  
				System.DateTime ExpDate = System.DateTime.Parse(ExpValue);
				string MyFormattedDate = (ExpDate.ToString("MM/dd/yyyy"));
			
				if (MyFormattedDate != TerrEXP)
					{
					Report.Success("Success"," Date values do not match.  EBX application:   " + TerrEXP +  "    Database:   " + MyFormattedDate);
					}
				else
					{
					Report.Failure("Failure", " Date values not match.  EBX application:   " + TerrEXP +  "    Database:   " + MyFormattedDate);
					}				
				}
			else
				{
				SellCoExpDate = SellCoExpDate.Replace(",","");
				string ExpYear = SellCoExpDate.Substring(3,2);
				string ExpDay = SellCoExpDate.Substring(1,2);
				string ExpMonth =  SellCoExpDate.Substring(0,1);
				string ExpValue =  (ExpMonth + "/" +ExpDay+"/" +ExpYear);   
				System.DateTime ExpDate = System.DateTime.Parse(ExpValue);
				string MyFormattedDate = (ExpDate.ToString("MM/dd/yyyy"));
				
				if (MyFormattedDate != TerrEXP)
					{
					Report.Success("Success"," Date values do not match.  EBX application:   " + TerrEXP +  "    Database:   " + MyFormattedDate);
					}
				else
					{
					Report.Failure("Failure", " Date values not match.  EBX application:   " + TerrEXP +  "    Database:   " + MyFormattedDate);
					}				
				}
     	    }
            
        }
    }
}
