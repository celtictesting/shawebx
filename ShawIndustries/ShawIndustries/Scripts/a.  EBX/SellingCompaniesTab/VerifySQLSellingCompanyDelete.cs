﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/6/2022
 * Time: 4:52 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;
using System.Globalization;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of VerifySQLSellingCompanyDelete.
    /// </summary>
    [TestModule("75B1FCCC-7210-45C5-9786-46444185F687", ModuleType.UserCode, 1)]
    public class VerifySQLSellingCompanyDelete : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public VerifySQLSellingCompanyDelete()
        {
            // Do not delete - a parameterless constructor is required!
        }


string _CustomerNumber = "";
[TestVariable("3695464d-11e6-4539-9bed-11d4ec657804")]
public string CustomerNumber
{
	get { return _CustomerNumber; }
	set { _CustomerNumber = value; }
}
  
  string _sellingCompany = "";
  [TestVariable("5e4b3899-7dd1-465d-ad2e-620d6828cfc6")]
  public string sellingCompany
  {
  	get { return _sellingCompany; }
  	set { _sellingCompany = value; }
  }
        
        
        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
           	DBConnector dbConnector = new DBConnector();

    		string query = "SELECT count(*) FROM dbo.EBX_Replicated_CustomerSellingCompany where customer_ = '" + CustomerNumber + "' and sellingCompany_ = '" + sellingCompany + "'";
    		
    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);    		
    		SellingCompany referenceTables = new SellingCompany(dt);
    		
    		string QueryResults = referenceTables.InventoryTable[0].ToString();
    		
    		if (QueryResults=="0")
    				{
					Report.Success("Success","Row count is:  " + QueryResults + ".  Selco successfully removed from EBX_Replicated_CustomerSellingCompany.");
					}
				else
					{
					Report.Failure("Failure","Row count is:  " + QueryResults + ".  Selco value still exists in EBX_Replicated_CustomerSellingCompany." );
					}			
    		
        }
    }
}
