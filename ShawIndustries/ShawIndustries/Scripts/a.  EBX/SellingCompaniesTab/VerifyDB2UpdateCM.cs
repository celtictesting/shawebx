﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/19/2022
 * Time: 12:07 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;
using System.Globalization;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of VerifyDB2UpdateCM.
    /// </summary>
    [TestModule("007CB6BE-CE45-41FC-9DB6-20D242875955", ModuleType.UserCode, 1)]
    public class VerifyDB2UpdateCM : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public VerifyDB2UpdateCM()
        {
            // Do not delete - a parameterless constructor is required!
        }

        string _TerrTerms = "";
        [TestVariable("fd61cb1d-2a9f-4ede-96fc-948a35ee8fe5")]
        public string TerrTerms
        {
        	get { return _TerrTerms; }
        	set { _TerrTerms = value; }
        }
        
        string _DB2SellCustNbr = "";
        [TestVariable("c8f103e5-a7c7-4f73-be2c-c2b643bb97c0")]
        public string DB2SellCustNbr
        {
        	get { return _DB2SellCustNbr; }
        	set { _DB2SellCustNbr = value; }
        }
        
        string _TermsInfo = "";
        [TestVariable("fcf96226-f6dd-4f7f-8ece-91a2638a1abb")]
        public string TermsInfo
        {
        	get { return _TermsInfo; }
        	set { _TermsInfo = value; }
        }
        
        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
            Delay.Duration(60000);
           	DBConnector dbConnector = new DBConnector();

    		string query = "SELECT TERMS_CODE FROM SHXP.CUSTOMER_MASTER where CUST_NBR = '" + DB2SellCustNbr + "'";
    		
    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToDB2DataBase(query);    		
    		
    		SellingCompany referenceTables = new SellingCompany(dt);
    		
    		TermsInfo = referenceTables.InventoryTable[0].ToString();
    		Report.Info("Info", "For the Terms validation -  Database:   " + TermsInfo + " Application:   " + TerrTerms.Split('-')[0].TrimEnd());
			Validate.AreEqual(TermsInfo, TerrTerms.Split('-')[0].TrimEnd());


        }
    }
}
