﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/20/2022
 * Time: 11:20 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of VerifyDB2DefaultUOM.
    /// </summary>
    [TestModule("0CFC8907-9C6F-4517-8476-B6D5692F2D7D", ModuleType.UserCode, 1)]
    public class VerifyDB2DefaultUOM : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public VerifyDB2DefaultUOM()
        {
            // Do not delete - a parameterless constructor is required!
        }

string _DefaultUOM = "";
[TestVariable("d7527ab8-938e-4467-b4cc-ad9f090904ec")]
public string DefaultUOM
{
	get { return _DefaultUOM; }
	set { _DefaultUOM = value; }
}

string _DB2SellCustNbr = "";
[TestVariable("9528d66c-3840-4803-bc37-dd7dd6e302fa")]
public string DB2SellCustNbr
{
	get { return _DB2SellCustNbr; }
	set { _DB2SellCustNbr = value; }
}

string _SellingCompanyCode = "";
[TestVariable("8a004b96-2461-4217-89c7-f7af2eae0e4c")]
public string SellingCompanyCode
{
	get { return _SellingCompanyCode; }
	set { _SellingCompanyCode = value; }
}

string _PriceTypeInfo = "";
[TestVariable("361c8dee-2ba9-4a13-b5e6-0c82e5bbb18b")]
public string PriceTypeInfo
{
	get { return _PriceTypeInfo; }
	set { _PriceTypeInfo = value; }
}

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
            Delay.Duration(60000);   
           	DBConnector dbConnector = new DBConnector();
           	
    		string query = "SELECT PRICE_TYPE FROM SHXP.CUSTOMER_SLSMN where CUST_NBR = '" + DB2SellCustNbr + "' and SELCO = '" + SellingCompanyCode + "'";
    		
    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToDB2DataBase(query);
    		
    		SellingCompany referenceTables = new SellingCompany(dt);
   		
      		PriceTypeInfo = referenceTables.InventoryTable[0].ToString();
      		
      		Report.Info("Info", "For the UOM validation -  Database:   " + PriceTypeInfo + " Default Value:   " + DefaultUOM);	
			Validate.AreEqual(PriceTypeInfo, DefaultUOM);

            
        }
    }
}
