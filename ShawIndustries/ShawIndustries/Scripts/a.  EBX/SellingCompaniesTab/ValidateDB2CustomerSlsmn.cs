﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/4/2022
 * Time: 12:15 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */


using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;
using System.Globalization;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of ValidateDB2CustomerSlsmn.
    /// </summary>
    [TestModule("AD7D86EB-673E-44EB-89C7-6073D7727807", ModuleType.UserCode, 1)]
    public class ValidateDB2CustomerSlsmn : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public ValidateDB2CustomerSlsmn()
        {
            // Do not delete - a parameterless constructor is required!
        }


string _DB2SellCustNbr = "";
[TestVariable("af625467-65d2-41c2-8887-7cb8816eeb8b")]
public string DB2SellCustNbr
{
	get { return _DB2SellCustNbr; }
	set { _DB2SellCustNbr = value; }
}
        
        
        string _DB2PrimarySelco = "";
        [TestVariable("8736cc54-a5aa-4a80-83f2-56e05127045a")]
        public string DB2PrimarySelco
        {
        	get { return _DB2PrimarySelco; }
        	set { _DB2PrimarySelco = value; }
        }
        
        string _DB2UpdatedPrimarySelco = "";
        [TestVariable("a7492a52-aa9c-4f5e-97bd-6cb66bdde95c")]
        public string DB2UpdatedPrimarySelco
        {
        	get { return _DB2UpdatedPrimarySelco; }
        	set { _DB2UpdatedPrimarySelco = value; }
        }
        
        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
           	DBConnector dbConnector = new DBConnector();

    		string query = "SELECT count(*) FROM SHXP.CUSTOMER_SLSMN where CUST_NBR = '" + DB2SellCustNbr + "' and SELCO = '" + DB2UpdatedPrimarySelco.Split('-')[0].TrimEnd() + "'";
    		
    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToDB2DataBase(query);    		
    		SellingCompany referenceTables = new SellingCompany(dt);
    		
    		string QueryResults = referenceTables.InventoryTable[0].ToString();
    		
    		if (QueryResults=="0")
    				{
					Report.Success("Success","Row count is:  " + QueryResults + ".  Primary Selco value " + DB2UpdatedPrimarySelco.Split('-')[0].TrimEnd() + " not available in SHXP.CUSTOMER_SLSMN.");
					}
				else
					{
					Report.Failure("Failure","Row count is:  " + QueryResults + ".  Primary Selco value " + DB2UpdatedPrimarySelco.Split('-')[0].TrimEnd() + "  still exists in SHXP.CUSTOMER_SLSMN." );
					}			
 				
        }
    }
}
