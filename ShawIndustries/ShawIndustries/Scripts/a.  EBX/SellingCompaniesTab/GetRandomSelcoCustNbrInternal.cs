﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/5/2022
 * Time: 9:27 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of GetRandomSelcoCustNbrInternal.
    /// </summary>
    [TestModule("9ACE37B8-17B9-42D3-AD73-DA8081FCD08A", ModuleType.UserCode, 1)]
    public class GetRandomSelcoCustNbrInternal : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public GetRandomSelcoCustNbrInternal()
        {
            // Do not delete - a parameterless constructor is required!
        }

   string _DB2SellCustNbr = "";
   [TestVariable("f18bea6f-4c68-4f6b-a7f9-9b722edec3c1")]
   public string DB2SellCustNbr
   {
   	get { return _DB2SellCustNbr; }
   	set { _DB2SellCustNbr = value; }
   }
        
        
        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
            
           	DBConnector dbConnector = new DBConnector();

    		string query = "SELECT CUST_NBR, RAND() as IDX FROM SHXP.CUSTOMER_MASTER where SELCO is not NULL AND CUST_NAME LIKE 'SHAW TEST%' AND CUST_STATUS NOT LIKE 'D' AND KWIC NOT LIKE 'IVR%' ORDER BY IDX FETCH FIRST ROW ONLY";
    		
    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToDB2DataBase(query);
      		
    		SellingCompany referenceTables = new SellingCompany(dt);
    		
    		DB2SellCustNbr = referenceTables.InventoryTable[0].ToString();
    		
			Report.Info("Info", "Random DB2 Customer Number for Selco Testing:  " + DB2SellCustNbr);
            
        }
    }
}
