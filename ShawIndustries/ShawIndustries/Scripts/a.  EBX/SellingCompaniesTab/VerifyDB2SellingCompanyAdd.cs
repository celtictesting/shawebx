﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 4/22/2022
 * Time: 1:57 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;
using System.Globalization;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of VerifyDB2SellingCompanyAdd.
    /// </summary>
    [TestModule("688ACF23-2895-447E-B532-09DFC6B8CE89", ModuleType.UserCode, 1)]
    public class VerifyDB2SellingCompanyAdd : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public VerifyDB2SellingCompanyAdd()
        {
            // Do not delete - a parameterless constructor is required!
        }
        
        string _DB2SellCustNbr = "";
        [TestVariable("e46ec186-efec-49d6-af3e-c22e47d5b5cc")]
        public string DB2SellCustNbr
        {
        	get { return _DB2SellCustNbr; }
        	set { _DB2SellCustNbr = value; }
        }
        
 
 string _DB2PrimarySelco = "";
 [TestVariable("48313432-d068-4d4d-a0e9-a51f43a0e121")]
 public string DB2PrimarySelco
 {
 	get { return _DB2PrimarySelco; }
 	set { _DB2PrimarySelco = value; }
 }
        
        
        string _RegionInfo = "";
        [TestVariable("4928f468-fc0b-44e2-a542-0368afba22ce")]
        public string RegionInfo
        {
        	get { return _RegionInfo; }
        	set { _RegionInfo = value; }
        }
        
        
        string _DistrictInfo = "";
        [TestVariable("7c162c79-0379-45a9-9d8a-ca571262a3a0")]
        public string DistrictInfo
        {
        	get { return _DistrictInfo; }
        	set { _DistrictInfo = value; }
        }
 
        
        string _TerritoryInfo = "";
        [TestVariable("e7f743bf-6616-4b8e-ac6b-1c1e76661d9d")]
        public string TerritoryInfo
        {
        	get { return _TerritoryInfo; }
        	set { _TerritoryInfo = value; }
        }
        
        
        string _TermsInfo = "";
        [TestVariable("863a5896-aae2-4aed-ab70-d78be118d45b")]
        public string TermsInfo
        {
        	get { return _TermsInfo; }
        	set { _TermsInfo = value; }
        }
        
        
        string _PriceTypeInfo = "";
        [TestVariable("1d07fdeb-d4b6-4f3a-8807-11592d3980aa")]
        public string PriceTypeInfo
        {
        	get { return _PriceTypeInfo; }
        	set { _PriceTypeInfo = value; }
        }
        
     
        string _SellCoExpDate = "";
        [TestVariable("bc676956-2bb0-404a-8aa0-a13b69c7d760")]
        public string SellCoExpDate
        {
        	get { return _SellCoExpDate; }
        	set { _SellCoExpDate = value; }
        }
 

string _TerritorySelected = "";
[TestVariable("691ae3df-aab5-4dec-80a9-8fcc2585efe8")]
public string TerritorySelected
{
	get { return _TerritorySelected; }
	set { _TerritorySelected = value; }
}

string _SelcoSelected = "";
[TestVariable("f24d2d21-3921-4b35-a71a-e74282b50805")]
public string SelcoSelected
{
	get { return _SelcoSelected; }
	set { _SelcoSelected = value; }
}

string _RegionSelected = "";
[TestVariable("0a61daa0-ba6e-4ecb-a46d-6776a8fd6608")]
public string RegionSelected
{
	get { return _RegionSelected; }
	set { _RegionSelected = value; }
}

string _DistrictSelected = "";
[TestVariable("96de67f8-9123-4fcc-acd0-eb0f2b2c77d1")]
public string DistrictSelected
{
	get { return _DistrictSelected; }
	set { _DistrictSelected = value; }
}

string _TerrUOM = "";
[TestVariable("7c3667cf-129f-4cff-989d-2df29132972b")]
public string TerrUOM
{
	get { return _TerrUOM; }
	set { _TerrUOM = value; }
}

string _TerrTerms = "";
[TestVariable("e95ff748-8ab9-4eda-b2d7-f209eaabc028")]
public string TerrTerms
{
	get { return _TerrTerms; }
	set { _TerrTerms = value; }
}

string _TerrEXP = "";
[TestVariable("dd5d01ed-e7dd-4d19-8760-5e24b4fe5539")]
public string TerrEXP
{
	get { return _TerrEXP; }
	set { _TerrEXP = value; }
}

        
        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
            Delay.Duration(60000);
           	DBConnector dbConnector = new DBConnector();

    		string query = "SELECT SELCO, REGION, DISTRICT, TERRITORY, TERMS_CODE, PRICE_TYPE, DATE FROM SHXP.CUSTOMER_SLSMN where CUST_NBR = '" + DB2SellCustNbr + "' and SELCO = '" + SelcoSelected + "'";
    		
    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToDB2DataBase(query);
    		
    	    string MyReader = dt.Rows.Count.ToString();
    	   
     	    if (MyReader == "0")
     	    	{
     	    	Report.Failure("Fail", "The Selco " + SelcoSelected + " is not available for Customer " + DB2SellCustNbr + " in the DB2 Table - SHXP.CUSTOMER_SLSMN");
     	    	}
     	    else
    	    {    		
    		
    		SQLSellingCompany referenceTables = new SQLSellingCompany(dt);
    		
    		DB2PrimarySelco = referenceTables.InventoryTable[0].ToString();
     		RegionInfo = referenceTables.InventoryTable[1].ToString();
    		DistrictInfo = referenceTables.InventoryTable[2].ToString();
    		TerritoryInfo = referenceTables.InventoryTable[3].ToString();
     		TermsInfo = referenceTables.InventoryTable[4].ToString();
    		PriceTypeInfo = referenceTables.InventoryTable[5].ToString();
      		SellCoExpDate = referenceTables.InventoryTable[6].ToString();

      		Report.Info("Info", "For the Territory validation -  Database:   " + TerritoryInfo + " Application:   " + TerritorySelected.Split('-')[0].TrimEnd());
    		Validate.AreEqual(TerritoryInfo, TerritorySelected.Split('-')[0].TrimEnd());

      		Report.Info("Info", "For the District validation -  Database:   " + DistrictInfo + " Application:   " + DistrictSelected.Split('-')[0].TrimEnd());      		
			Validate.AreEqual(DistrictInfo, DistrictSelected.Split('-')[0].TrimEnd());  

      		Report.Info("Info", "For the District validation -  Database:   " + RegionInfo + " Application:   " + RegionSelected.Split('-')[0].TrimEnd());      					
      		Validate.AreEqual(RegionInfo, RegionSelected.Split('-')[0].TrimEnd());


			Report.Info("Info", "For the Terms validation -  Database:   " + TermsInfo + " Application:   " + TerrTerms.Split('-')[0].TrimEnd());
			Validate.AreEqual(TermsInfo, TerrTerms.Split('-')[0].TrimEnd());
			
	   		Report.Info("Info", "For the UOM validation -  Database:   " + PriceTypeInfo + " Application:   " + TerrUOM.Split('-')[0].TrimEnd());	
			Validate.AreEqual(PriceTypeInfo, TerrUOM.Split('-')[0].TrimEnd());
			
			if (SellCoExpDate.Length==6)
				{
				SellCoExpDate = SellCoExpDate.Replace(",","");
				string ExpYear = SellCoExpDate.Substring(4,2);
				string ExpDay = SellCoExpDate.Substring(2,2);				
				string ExpMonth =  SellCoExpDate.Substring(0,2);
				string ExpValue =  (ExpMonth + "/" +ExpDay+"/" +ExpYear);  
				System.DateTime ExpDate = System.DateTime.Parse(ExpValue);
				string MyFormattedDate = (ExpDate.ToString("MM/dd/yyyy"));
			
				if (MyFormattedDate != TerrEXP)
					{
					Report.Success("Success"," Date values do not match.  EBX application:   " + TerrEXP +  "    Database:   " + MyFormattedDate);
					}
				else
					{
					Report.Failure("Failure", " Date values not match.  EBX application:   " + TerrEXP +  "    Database:   " + MyFormattedDate);
					}				
				}
			else
				{
				SellCoExpDate = SellCoExpDate.Replace(",","");
				string ExpYear = SellCoExpDate.Substring(3,2);
				string ExpDay = SellCoExpDate.Substring(1,2);
				string ExpMonth =  SellCoExpDate.Substring(0,1);
				string ExpValue =  (ExpMonth + "/" +ExpDay+"/" +ExpYear);   
				System.DateTime ExpDate = System.DateTime.Parse(ExpValue);
				string MyFormattedDate = (ExpDate.ToString("MM/dd/yyyy"));
				
				if (MyFormattedDate != TerrEXP)
					{
					Report.Success("Success"," Date values do not match.  EBX application:   " + TerrEXP +  "    Database:   " + MyFormattedDate);
					}
				else
					{
					Report.Failure("Failure", " Date values not match.  EBX application:   " + TerrEXP +  "    Database:   " + MyFormattedDate);
					}				
				}
     	    }

        }
    }
}
