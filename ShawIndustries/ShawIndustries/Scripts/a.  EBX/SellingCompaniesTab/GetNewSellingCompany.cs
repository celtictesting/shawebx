﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/20/2022
 * Time: 10:11 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of GetNewSellingCompany.
    /// </summary>
    [TestModule("DC754303-768A-4E1E-A953-1E078BB3F091", ModuleType.UserCode, 1)]
    public class GetNewSellingCompany : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public GetNewSellingCompany()
        {
            // Do not delete - a parameterless constructor is required!
        }

string _SellingCompanyCode = "";
[TestVariable("318d4118-047f-4a11-9732-5917da1c2ac7")]
public string SellingCompanyCode
{
	get { return _SellingCompanyCode; }
	set { _SellingCompanyCode = value; }
}

string _MySelcos = "";
[TestVariable("0f36017a-8e87-4c80-8393-f33cb012e858")]
public string MySelcos
{
	get { return _MySelcos; }
	set { _MySelcos = value; }
}

string _PrimarySellingInfo = "";
[TestVariable("09829d6b-10a7-4193-9181-1f6dc9cc0583")]
public string PrimarySellingInfo
{
	get { return _PrimarySellingInfo; }
	set { _PrimarySellingInfo = value; }
}

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;

            DBConnector dbConnector = new DBConnector();
    		string query = "SELECT TOP 1 a.SellingCompany_sellingCompanyCode_ from EBX_Replicated_Ref_SellingCompany a join EBX_Replicated_Ref_TerritoryManage b on a.SellingCompany_territory_ = b.TerritoryManager_code where b.TerritoryManager_name !=' ' and a.SellingCompany_sellingCompanyCode_ not in (" + PrimarySellingInfo + "," + MySelcos + ") Order By NEWID()";

    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
      		
    		SellingCompany referenceTables = new SellingCompany(dt);

    		SellingCompanyCode = referenceTables.InventoryTable[0].ToString();
    		
			Report.Info("Info", "Random Selling Company Code:  " + SellingCompanyCode);
            
            
        }
    }
}
