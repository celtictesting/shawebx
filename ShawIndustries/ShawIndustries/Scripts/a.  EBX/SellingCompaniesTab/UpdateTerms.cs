﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.SellingCompaniesTab
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The UpdateTerms recording.
    /// </summary>
    [TestModule("b742098c-233b-4f94-b97f-7fcfc1fc9a8e", ModuleType.Recording, 1)]
    public partial class UpdateTerms : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static UpdateTerms instance = new UpdateTerms();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public UpdateTerms()
        {
            TerrTerms = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static UpdateTerms Instance
        {
            get { return instance; }
        }

#region Variables

        string _TerrTerms;

        /// <summary>
        /// Gets or sets the value of variable TerrTerms.
        /// </summary>
        [TestVariable("d4bab5bd-32bd-42fd-ba49-edae5e1ef01f")]
        public string TerrTerms
        {
            get { return _TerrTerms; }
            set { _TerrTerms = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.SellingCompanyTab.TermsField' at Center.", repo.EBXApplication.SellingCompanyTab.TermsFieldInfo, new RecordItemIndex(0));
            repo.EBXApplication.SellingCompanyTab.TermsField.Click();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key 'Ctrl+A' Press with focus on 'EBXApplication.SellingCompanyTab.TermsField'.", repo.EBXApplication.SellingCompanyTab.TermsFieldInfo, new RecordItemIndex(1));
            Keyboard.PrepareFocus(repo.EBXApplication.SellingCompanyTab.TermsField);
            Keyboard.Press(System.Windows.Forms.Keys.A | System.Windows.Forms.Keys.Control, Keyboard.DefaultScanCode, Keyboard.DefaultKeyPressTime, 1, true);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key 'Delete' Press with focus on 'EBXApplication.SellingCompanyTab.TermsField'.", repo.EBXApplication.SellingCompanyTab.TermsFieldInfo, new RecordItemIndex(2));
            Keyboard.PrepareFocus(repo.EBXApplication.SellingCompanyTab.TermsField);
            Keyboard.Press(System.Windows.Forms.Keys.Delete, Keyboard.DefaultScanCode, Keyboard.DefaultKeyPressTime, 1, true);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Down}{Down}' with focus on 'EBXApplication.SellingCompanyTab.TermsField'.", repo.EBXApplication.SellingCompanyTab.TermsFieldInfo, new RecordItemIndex(3));
            repo.EBXApplication.SellingCompanyTab.TermsField.PressKeys("{Down}{Down}");
            Delay.Milliseconds(0);
            
            UserCodeCollection.RandonDropDownSelection.SelectRandomItemFromDropDown(repo.EBXApplication.SellingCompanyTab.TermsDropdownValuesExclNotDefinedInfo);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Get Value", "Getting attribute 'TagValue' from item 'EBXApplication.SellingCompanyTab.TermsField' and assigning its value to variable 'TerrTerms'.", repo.EBXApplication.SellingCompanyTab.TermsFieldInfo, new RecordItemIndex(5));
            TerrTerms = repo.EBXApplication.SellingCompanyTab.TermsField.Element.GetAttributeValueText("TagValue");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "User", TerrTerms, new RecordItemIndex(6));
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
