﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.SellingCompaniesTab
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The RandomSellingCompanyAssociation recording.
    /// </summary>
    [TestModule("c71269d7-e51a-4da1-9cab-923ad99389a4", ModuleType.Recording, 1)]
    public partial class RandomSellingCompanyAssociation : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static RandomSellingCompanyAssociation instance = new RandomSellingCompanyAssociation();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public RandomSellingCompanyAssociation()
        {
            SellCompanyCode = "";
            SelectedTerritoryInfo = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static RandomSellingCompanyAssociation Instance
        {
            get { return instance; }
        }

#region Variables

        string _SelectedTerritoryInfo;

        /// <summary>
        /// Gets or sets the value of variable SelectedTerritoryInfo.
        /// </summary>
        [TestVariable("616ca88d-053c-4827-8933-894c9fd782a2")]
        public string SelectedTerritoryInfo
        {
            get { return _SelectedTerritoryInfo; }
            set { _SelectedTerritoryInfo = value; }
        }

        /// <summary>
        /// Gets or sets the value of variable index.
        /// </summary>
        [TestVariable("1f125598-52a9-4e27-91d3-192b73cf37ec")]
        public string index
        {
            get { return repo.index; }
            set { repo.index = value; }
        }

        /// <summary>
        /// Gets or sets the value of variable SellCompanyCode.
        /// </summary>
        [TestVariable("25e207a8-5dd4-485c-b8b4-25c9b347e1ba")]
        public string SellCompanyCode
        {
            get { return repo.SellCompanyCode; }
            set { repo.SellCompanyCode = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.SellingCompanyTab.SellingCompanyFilterIcon' at Center.", repo.EBXApplication.SellingCompanyTab.SellingCompanyFilterIconInfo, new RecordItemIndex(0));
            repo.EBXApplication.SellingCompanyTab.SellingCompanyFilterIcon.Click();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking Select() on item 'EBXApplication.CMMS.EbxLegacyComponent.SellingCompanyCodeFilterOption'.", repo.EBXApplication.CMMS.EbxLegacyComponent.SellingCompanyCodeFilterOptionInfo, new RecordItemIndex(1));
            repo.EBXApplication.CMMS.EbxLegacyComponent.SellingCompanyCodeFilterOption.Select();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$SellCompanyCode' with focus on 'EBXApplication.SellingCompanyTab.SellCompanyText'.", repo.EBXApplication.SellingCompanyTab.SellCompanyTextInfo, new RecordItemIndex(2));
            repo.EBXApplication.SellingCompanyTab.SellCompanyText.PressKeys(SellCompanyCode);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Return}' with focus on 'EBXApplication.SellingCompanyTab.SellCompanyText'.", repo.EBXApplication.SellingCompanyTab.SellCompanyTextInfo, new RecordItemIndex(3));
            repo.EBXApplication.SellingCompanyTab.SellCompanyText.PressKeys("{Return}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.SellingCompanyTab.Dev2SearchApply' at Center.", repo.EBXApplication.SellingCompanyTab.Dev2SearchApplyInfo, new RecordItemIndex(4));
            repo.EBXApplication.SellingCompanyTab.Dev2SearchApply.Click();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Wait", "Waiting 1m to exist. Associated repository item: 'EBXApplication.SellingCompanyTab.SellingCompanyCheckbox'", repo.EBXApplication.SellingCompanyTab.SellingCompanyCheckboxInfo, new ActionTimeout(60000), new RecordItemIndex(5));
            repo.EBXApplication.SellingCompanyTab.SellingCompanyCheckboxInfo.WaitForExists(60000);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'EBXApplication.SellingCompanyTab.SellCoCodeColumnInfo'.", repo.EBXApplication.SellingCompanyTab.SellCoCodeColumnInfoInfo, new RecordItemIndex(6));
            Validate.Exists(repo.EBXApplication.SellingCompanyTab.SellCoCodeColumnInfoInfo);
            Delay.Milliseconds(0);
            
            Mouse_Click_Checkbox(repo.EBXApplication.SellingCompanyTab.SellingCompanyCheckboxInfo);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.SellingCompanyTab.AssociateButtonFilterScreen' at Center.", repo.EBXApplication.SellingCompanyTab.AssociateButtonFilterScreenInfo, new RecordItemIndex(8));
            repo.EBXApplication.SellingCompanyTab.AssociateButtonFilterScreen.Click();
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
