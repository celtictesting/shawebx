﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/4/2022
 * Time: 11:27 AM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;
using System.Globalization;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of VerifyDB2CustomerMaster.
    /// </summary>
    [TestModule("AB287F45-D90A-4F4D-BE67-9D6021823184", ModuleType.UserCode, 1)]
    public class VerifyDB2CustomerMaster : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public VerifyDB2CustomerMaster()
        {
            // Do not delete - a parameterless constructor is required!
        }

 
 string _DB2SellCustNbr = "";
 [TestVariable("df914f10-4559-4cec-b89c-a2bca32ad134")]
 public string DB2SellCustNbr
 {
 	get { return _DB2SellCustNbr; }
 	set { _DB2SellCustNbr = value; }
 }
 
 string _DB2PrimarySelco = "";
 [TestVariable("18df8b85-8ebe-4eff-905e-124170f9fc13")]
 public string DB2PrimarySelco
 {
 	get { return _DB2PrimarySelco; }
 	set { _DB2PrimarySelco = value; }
 }
 
 string _DB2UpdatedPrimarySelco = "";
 [TestVariable("6708f164-6c87-4616-bb83-56e8cd1dd3fa")]
 public string DB2UpdatedPrimarySelco
 {
 	get { return _DB2UpdatedPrimarySelco; }
 	set { _DB2UpdatedPrimarySelco = value; }
 }
        
        
        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
 
            Delay.Duration(60000);
           	DBConnector dbConnector = new DBConnector();

    		string query = "SELECT SELCO FROM SHXP.CUSTOMER_MASTER where CUST_NBR = '" + DB2SellCustNbr + "' ";
    		
    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToDB2DataBase(query);
    		SellingCompany referenceTables = new SellingCompany(dt);
    		
    		var DB2SelcoValue = referenceTables.InventoryTable[0].ToString();  
    		
    		Validate.AreEqual(DB2SelcoValue,DB2UpdatedPrimarySelco.Split('-')[0].TrimEnd());
    		
  		
            
        }
    }
}
