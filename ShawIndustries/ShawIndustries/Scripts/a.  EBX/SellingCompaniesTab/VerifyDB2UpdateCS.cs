﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/19/2022
 * Time: 12:16 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;
using System.Globalization;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of VerifyDB2UpdateCS.
    /// </summary>
    [TestModule("D37021ED-25A5-4F82-B74B-D3DF2EF4DACB", ModuleType.UserCode, 1)]
    public class VerifyDB2UpdateCS : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public VerifyDB2UpdateCS()
        {
            // Do not delete - a parameterless constructor is required!
        }

string _TerrTerms = "";
[TestVariable("358cd622-3b0c-49b4-b2da-e585f30ba4eb")]
public string TerrTerms
{
	get { return _TerrTerms; }
	set { _TerrTerms = value; }
}

string _DB2SellCustNbr = "";
[TestVariable("255fc3f7-539f-4063-a192-441123d27081")]
public string DB2SellCustNbr
{
	get { return _DB2SellCustNbr; }
	set { _DB2SellCustNbr = value; }
}

string _TermsInfo = "";
[TestVariable("23fc21bc-2bb8-4669-a097-68202ec924f2")]
public string TermsInfo
{
	get { return _TermsInfo; }
	set { _TermsInfo = value; }
}

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
			DBConnector dbConnector = new DBConnector();

    		string query = "SELECT DISTINCT TERMS_CODE FROM SHXP.CUSTOMER_SLSMN where CUST_NBR = '" + DB2SellCustNbr + "'";
    		
    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToDB2DataBase(query);
    		
    		SellingCompany referenceTables = new SellingCompany(dt);
    		
    		TermsInfo = referenceTables.InventoryTable[0].ToString();
    		Report.Info("Info", "For the Terms validation -  Database:   " + TermsInfo + " Application:   " + TerrTerms.Split('-')[0].TrimEnd());
			Validate.AreEqual(TermsInfo, TerrTerms.Split('-')[0].TrimEnd());
    		

        }
    }
}
