﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/20/2022
 * Time: 12:02 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of GetRandomCACustNbr.
    /// </summary>
    [TestModule("25F08968-D21B-45AB-909E-8B4BB68E4F28", ModuleType.UserCode, 1)]
    public class GetRandomCACustNbr : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public GetRandomCACustNbr()
        {
            // Do not delete - a parameterless constructor is required!
        }

string _DB2SellCustNbr = "";
[TestVariable("3b0bfcce-dfb5-43df-bf15-d0df9ae27de1")]
public string DB2SellCustNbr
{
	get { return _DB2SellCustNbr; }
	set { _DB2SellCustNbr = value; }
}

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            

           	DBConnector dbConnector = new DBConnector();

    		string query = "SELECT a.CUST_NBR, RAND() as IDX FROM SHXP.CUSTOMER_MASTER a JOIN SHXP.CUSTOMER_SLSMN b on a.CUST_NBR = b.CUST_NBR where a.CNTRY_CODE in ('0.31', '0.32', '0.33', '0.34') AND a.CUST_STATUS NOT LIKE 'D' ORDER BY IDX FETCH FIRST ROW ONLY";

    		Report.Info("SQL Query Used: " + query);
    		DataTable dt = dbConnector.ConnectToDB2DataBase(query);
      		
    		SellingCompany referenceTables = new SellingCompany(dt);
    		
    		DB2SellCustNbr = referenceTables.InventoryTable[0].ToString();
    		
			Report.Info("Info", "Random DB2 Customer Number for Selco Testing:  " + DB2SellCustNbr);
            
        }
    }
}
