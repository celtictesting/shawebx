﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.SellingCompaniesTab
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The GetPrimarySellingCompanyInfo recording.
    /// </summary>
    [TestModule("0a776a45-db97-4f16-8513-ed1f08dcdf32", ModuleType.Recording, 1)]
    public partial class GetPrimarySellingCompanyInfo : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static GetPrimarySellingCompanyInfo instance = new GetPrimarySellingCompanyInfo();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public GetPrimarySellingCompanyInfo()
        {
            PrimarySellingInfo = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static GetPrimarySellingCompanyInfo Instance
        {
            get { return instance; }
        }

#region Variables

        /// <summary>
        /// Gets or sets the value of variable PrimarySellingInfo.
        /// </summary>
        [TestVariable("84a17273-7cd7-4f7e-abbe-eb51cd58ec64")]
        public string PrimarySellingInfo
        {
            get { return repo.PrimarySellingInfo; }
            set { repo.PrimarySellingInfo = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Wait", "Waiting 30s to exist. Associated repository item: 'EBXApplication.SellingCompanyTab.SalesPrimarySellingField'", repo.EBXApplication.SellingCompanyTab.SalesPrimarySellingFieldInfo, new ActionTimeout(30000), new RecordItemIndex(0));
            repo.EBXApplication.SellingCompanyTab.SalesPrimarySellingFieldInfo.WaitForExists(30000);
            
            Get_value_SalesPrimarySellingField(repo.EBXApplication.SellingCompanyTab.SalesPrimarySellingFieldInfo);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "User", PrimarySellingInfo, new RecordItemIndex(2));
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
