﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/12/2022
 * Time: 12:32 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;
using System.Globalization;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of VerifyDB2SelcoAddDate.
    /// </summary>
    [TestModule("87B287FF-BBA7-4B4F-8645-2CBCC95EB4DD", ModuleType.UserCode, 1)]
    public class VerifyDB2SelcoAddDate : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public VerifyDB2SelcoAddDate()
        {
            // Do not delete - a parameterless constructor is required!
        }

string _SellCompanyCode = "";
[TestVariable("1680353d-cb9a-47fb-8287-e7fa443c3a6a")]
public string SellCompanyCode
{
	get { return _SellCompanyCode; }
	set { _SellCompanyCode = value; }
}

string _DB2SellCustNbr = "";
[TestVariable("247a7c33-c732-46ec-bb56-d390f127a861")]
public string DB2SellCustNbr
{
	get { return _DB2SellCustNbr; }
	set { _DB2SellCustNbr = value; }
}

string _SellCoExpDate = "";
[TestVariable("a23c7c81-1b5b-4cdc-bdf0-0061c3746f5b")]
public string SellCoExpDate
{
	get { return _SellCoExpDate; }
	set { _SellCoExpDate = value; }
}

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
           	DBConnector dbConnector = new DBConnector();

    		string query = "SELECT DATE FROM SHXP.CUSTOMER_SLSMN where CUST_NBR = '" + DB2SellCustNbr + "' and SELCO = '" + SellCompanyCode + "'";
    		
    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToDB2DataBase(query);
    		
    		
    		SellingCompany referenceTables = new SellingCompany(dt);
    		
      		SellCoExpDate = referenceTables.InventoryTable[0].ToString();

		
			if (SellCoExpDate.Length==6)
				{
				SellCoExpDate = SellCoExpDate.Replace(",","");
				string ExpYear = SellCoExpDate.Substring(4,2);
				string ExpDay = SellCoExpDate.Substring(2,2);				
				string ExpMonth =  SellCoExpDate.Substring(0,2);
				string ExpValue =  (ExpMonth + "/" +ExpDay+"/" +ExpYear);  
				System.DateTime ExpDate = System.DateTime.Parse(ExpValue);
				string MyFormattedDate = (ExpDate.ToString("MM/dd/yyyy"));
			
				if (MyFormattedDate == System.DateTime.Now.ToString("MM/dd/yyyy"))
					{
					Report.Success("Success"," Date values matches the current date.  Database has:   " + MyFormattedDate);
					}
				else
					{
					Report.Failure("Failure", " Date value does not match today's date.  Database has:    " + MyFormattedDate);
					}				
				}
			else
				{
				SellCoExpDate = SellCoExpDate.Replace(",","");
				string ExpYear = SellCoExpDate.Substring(3,2);
				string ExpDay = SellCoExpDate.Substring(1,2);
				string ExpMonth =  SellCoExpDate.Substring(0,1);
				string ExpValue =  (ExpMonth + "/" +ExpDay+"/" +ExpYear);   
				System.DateTime ExpDate = System.DateTime.Parse(ExpValue);
				string MyFormattedDate = (ExpDate.ToString("MM/dd/yyyy"));
				
			
				if (MyFormattedDate == System.DateTime.Now.ToString("MM/dd/yyyy"))
					{
					Report.Success("Success"," Date values matches the current date.  Database has:   " + MyFormattedDate);
					}
				else
					{
					Report.Failure("Failure", " Date value does not match today's date.  Database has:    " + MyFormattedDate);
					}				
				}
     	    }
            
        }
    }