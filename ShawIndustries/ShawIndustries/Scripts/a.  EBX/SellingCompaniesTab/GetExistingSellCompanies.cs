﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 5/19/2022
 * Time: 3:50 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of GetExistingSellCompanies.
    /// </summary>
    [TestModule("BF740804-3FE3-48B4-84EB-27DD3AACB895", ModuleType.UserCode, 1)]
    public class GetExistingSellCompanies : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public GetExistingSellCompanies()
        {
            // Do not delete - a parameterless constructor is required!
        }

string _DB2SellCustNbr = "";
[TestVariable("f3eecbd8-6793-45d4-8d25-ac3f86cd865d")]
public string DB2SellCustNbr
{
	get { return _DB2SellCustNbr; }
	set { _DB2SellCustNbr = value; }
}

string _MySelcos = "";
[TestVariable("362fc33c-6ffb-438c-8eb6-62537512e6fe")]
public string MySelcos
{
	get { return _MySelcos; }
	set { _MySelcos = value; }
}

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
   			DBConnector dbConnector = new DBConnector();
    		
    		string query = "SELECT SELCO from SHXP.CUSTOMER_SLSMN where CUST_NBR = '" + DB2SellCustNbr + "'" ;  		
    		
    		Report.Info("SQL Query: " + query);
    		
    		DataTable dt = dbConnector.ConnectToDB2DataBase(query);
    		
    		SellCoReferenceTable SellCompanyInfo = new SellCoReferenceTable(dt);
    		
    		string MyData = "";
    		MySelcos = "";
     		
    		foreach (string item in SellCompanyInfo.InventoryTable)
       		{
    			MyData= string.Join(",",item);
    			MySelcos = MySelcos + MyData + ",";
    		}
    		MySelcos= MySelcos.Substring(0,MySelcos.Length-1);
    		Report.Info(MySelcos);
    		
 //   		List<SellCoReferenceTable> dBSellingCompanyList = new List<SellCoReferenceTable>();
    		
//    		foreach(DataRow row in dt.Rows)
//			{
//    			SellCoReferenceTable SellCoReferenceTable = 
//    				new SellCoReferenceTable(row["Selco"].ToString());
//				dBSellingCompanyList.Add(SellCoReferenceTable.InventoryTable.Selco);
//			} 
//
// 		string Myitems = string.Join(",",dBSellingCompanyList);
//  		Report.Info(Myitems);
        }
    }
}
