﻿/*
 * Created by Ranorex
 * User: debohom
 * Date: 2/11/2022
 * Time: 4:32 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
 
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using ShawIndustries.Model;
using ShawIndustries.Service;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.__EBX.SellingCompaniesTab
{
    /// <summary>
    /// Description of GetRandomTerritoryCodeSalesforce.
    /// </summary>
    [TestModule("F43E3CA6-D849-4282-B20F-444E8512B0AC", ModuleType.UserCode, 1)]
    public class GetRandomTerritoryCodeSalesforce : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public GetRandomTerritoryCodeSalesforce()
        {
            // Do not delete - a parameterless constructor is required!
        }

string _SellCompanyTerritory = "";
[TestVariable("df42ff51-5063-4e11-a91d-d7ddb94945e7")]
public string SellCompanyTerritory
{
	get { return _SellCompanyTerritory; }
	set { _SellCompanyTerritory = value; }
}


string _CustomerNameField = "";
[TestVariable("82462e6a-1ef9-41a4-a7c3-fc4b70ef90ea")]
public string CustomerNameField
{
	get { return _CustomerNameField; }
	set { _CustomerNameField = value; }
}


        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
            
            DBConnector dbConnector = new DBConnector();
    		string query = "SELECT TOP 1  a.SellingCompany_territory_ from EBX_Replicated_Ref_SellingCompany a join EBX_Replicated_Ref_TerritoryManage b on a.SellingCompany_territory_ = b.TerritoryManager_code where b.TerritoryManager_name = '" + CustomerNameField + "' Order By NEWID()"; 

    		Report.Info("SQL Query Used: " + query);    		
    		DataTable dt = dbConnector.ConnectToSQLDataBase(query);
      		
    		SellingCompany referenceTables = new SellingCompany(dt);

    		SellCompanyTerritory = referenceTables.InventoryTable[0].ToString();
    		
			Report.Info("Info", "Random Selling Company Territory:  " + SellCompanyTerritory);         
            
            
            
        }
    }
}
