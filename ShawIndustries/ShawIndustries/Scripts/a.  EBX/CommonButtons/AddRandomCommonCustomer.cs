﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.CommonButtons
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The AddRandomCommonCustomer recording.
    /// </summary>
    [TestModule("0dd99ded-b6d1-4b68-8188-b13cb6ffda41", ModuleType.Recording, 1)]
    public partial class AddRandomCommonCustomer : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static AddRandomCommonCustomer instance = new AddRandomCommonCustomer();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public AddRandomCommonCustomer()
        {
            CustomCustomerCode = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static AddRandomCommonCustomer Instance
        {
            get { return instance; }
        }

#region Variables

        /// <summary>
        /// Gets or sets the value of variable CustomCustomerCode.
        /// </summary>
        [TestVariable("db950727-5cc6-429b-ae4a-1b544a5a911d")]
        public string CustomCustomerCode
        {
            get { return repo.CustomCustomerCode; }
            set { repo.CustomCustomerCode = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.MainTab.EbxSubSessioniFrame.CommonCustomerIcon' at Center.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.CommonCustomerIconInfo, new RecordItemIndex(0));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.CommonCustomerIcon.Click();
            Delay.Milliseconds(0);
            
            CustomCustomerCode = UserCodeCollection.RandonDropDownSelection.GetRandonNumber("ebx_ISS_Item_", ValueConverter.ArgumentFromString<int>("lowNumber", "1"), ValueConverter.ArgumentFromString<int>("highNumber", "15"));
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.MainTab.EbxSubSessioniFrame.CustomCustomerCodeRandomOption' at Center.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.CustomCustomerCodeRandomOptionInfo, new RecordItemIndex(2));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.CustomCustomerCodeRandomOption.Click();
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
