﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.MainTab
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The RandomlySelectCurrencyCode recording.
    /// </summary>
    [TestModule("0342dda9-728b-43ab-ba1c-11a6bb61347c", ModuleType.Recording, 1)]
    public partial class RandomlySelectCurrencyCode : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static RandomlySelectCurrencyCode instance = new RandomlySelectCurrencyCode();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public RandomlySelectCurrencyCode()
        {
            rowId = "ebx_Node_6";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static RandomlySelectCurrencyCode Instance
        {
            get { return instance; }
        }

#region Variables

        /// <summary>
        /// Gets or sets the value of variable labelName.
        /// </summary>
        [TestVariable("7c9932e6-009f-4398-a061-ad9c8c6cb950")]
        public string labelName
        {
            get { return repo.labelName; }
            set { repo.labelName = value; }
        }

        /// <summary>
        /// Gets or sets the value of variable rowId.
        /// </summary>
        [TestVariable("3e767745-e516-48ef-a5d0-5825adfff979")]
        public string rowId
        {
            get { return repo.rowId; }
            set { repo.rowId = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            rowId = UserCodeCollection.SelectorByTextLabel.SelectInputField(repo.EBXApplication.MainTab.EbxSubSessioniFrame.LabelNameElelementInfo, "Currency Code");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.MainTab.EbxSubSessioniFrame.CenterOfTable' at Center.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.CenterOfTableInfo, new RecordItemIndex(1));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.CenterOfTable.Click();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse scroll Vertical by 360 units.", new RecordItemIndex(2));
            Mouse.ScrollWheel(360);
            Delay.Milliseconds(300);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.MainTab.EbxSubSessioniFrame.genericDropdownArrow' at Center.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.genericDropdownArrowInfo, new RecordItemIndex(3));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.genericDropdownArrow.Click();
            Delay.Milliseconds(0);
            
            UserCodeCollection.RandonDropDownSelection.SelectRandomItemFromDropDown(repo.EBXApplication.MainTab.EbxSubSessioniFrame.ListDownDownOptionsEBXInfo);
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
