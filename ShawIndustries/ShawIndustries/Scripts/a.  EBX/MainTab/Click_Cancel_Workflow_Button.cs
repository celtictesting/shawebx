﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.MainTab
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Click_Cancel_Workflow_Button recording.
    /// </summary>
    [TestModule("993161c8-8f92-47ea-b939-af23c538dd0a", ModuleType.Recording, 1)]
    public partial class Click_Cancel_Workflow_Button : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static Click_Cancel_Workflow_Button instance = new Click_Cancel_Workflow_Button();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Click_Cancel_Workflow_Button()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Click_Cancel_Workflow_Button Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.CustomerDataEntryPerspectivePage.Cancel_Workflow_Button' at Center.", repo.EBXApplication.CustomerDataEntryPerspectivePage.Cancel_Workflow_ButtonInfo, new RecordItemIndex(0));
            repo.EBXApplication.CustomerDataEntryPerspectivePage.Cancel_Workflow_Button.Click();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Wait", "Waiting 30s to exist. Associated repository item: 'EBXApplication.CMMS.EbxLegacyComponent.Cancel_Workflow_Dialog_Text'", repo.EBXApplication.CMMS.EbxLegacyComponent.Cancel_Workflow_Dialog_TextInfo, new ActionTimeout(30000), new RecordItemIndex(1));
            repo.EBXApplication.CMMS.EbxLegacyComponent.Cancel_Workflow_Dialog_TextInfo.WaitForExists(30000);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Visible='True') on item 'EBXApplication.CMMS.EbxLegacyComponent.Cancel_Workflow_Dialog_Text'.", repo.EBXApplication.CMMS.EbxLegacyComponent.Cancel_Workflow_Dialog_TextInfo, new RecordItemIndex(2));
            Validate.AttributeEqual(repo.EBXApplication.CMMS.EbxLegacyComponent.Cancel_Workflow_Dialog_TextInfo, "Visible", "True");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
