﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.MainTab.Validation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The ValidateCustomerName recording.
    /// </summary>
    [TestModule("78dacd80-9add-49c2-ae40-f550ffa072ab", ModuleType.Recording, 1)]
    public partial class ValidateCustomerName : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static ValidateCustomerName instance = new ValidateCustomerName();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public ValidateCustomerName()
        {
            rowId = "";
            customerName = "John Doe";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static ValidateCustomerName Instance
        {
            get { return instance; }
        }

#region Variables

        string _customerName;

        /// <summary>
        /// Gets or sets the value of variable customerName.
        /// </summary>
        [TestVariable("bc77c065-ecb8-4b84-89da-7fd92e34344b")]
        public string customerName
        {
            get { return _customerName; }
            set { _customerName = value; }
        }

        /// <summary>
        /// Gets or sets the value of variable labelName.
        /// </summary>
        [TestVariable("7c9932e6-009f-4398-a061-ad9c8c6cb950")]
        public string labelName
        {
            get { return repo.labelName; }
            set { repo.labelName = value; }
        }

        /// <summary>
        /// Gets or sets the value of variable rowId.
        /// </summary>
        [TestVariable("952cc6ca-39d7-4de2-8669-fd5b3e09e1c3")]
        public string rowId
        {
            get { return repo.rowId; }
            set { repo.rowId = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            //rowId = UserCodeCollection.SelectorByTextLabel.SelectInputField(repo.EBXApplication.MainTab.EbxSubSessioniFrame.LabelNameElelementInfo, "Customer Name");
            //Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking WaitForDocumentLoaded() on item 'EBXApplication'.", repo.EBXApplication.SelfInfo, new RecordItemIndex(1));
            repo.EBXApplication.Self.WaitForDocumentLoaded();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (TagValue=$customerName) on item 'EBXApplication.MainTab.CustomerName'.", repo.EBXApplication.MainTab.CustomerNameInfo, new RecordItemIndex(2));
            Validate.AttributeEqual(repo.EBXApplication.MainTab.CustomerNameInfo, "TagValue", customerName);
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
