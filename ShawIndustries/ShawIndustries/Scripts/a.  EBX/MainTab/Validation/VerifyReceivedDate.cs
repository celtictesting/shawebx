﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.MainTab.Validation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The VerifyReceivedDate recording.
    /// </summary>
    [TestModule("efef200a-fe46-46f1-8add-950433a7bfeb", ModuleType.Recording, 1)]
    public partial class VerifyReceivedDate : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static VerifyReceivedDate instance = new VerifyReceivedDate();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public VerifyReceivedDate()
        {
            rowId = "ebx_Node_17";
            Date = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static VerifyReceivedDate Instance
        {
            get { return instance; }
        }

#region Variables

        string _Date;

        /// <summary>
        /// Gets or sets the value of variable Date.
        /// </summary>
        [TestVariable("c7b3dc0e-ad48-4478-a210-ab46b3cf8abc")]
        public string Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        /// <summary>
        /// Gets or sets the value of variable labelName.
        /// </summary>
        [TestVariable("7c9932e6-009f-4398-a061-ad9c8c6cb950")]
        public string labelName
        {
            get { return repo.labelName; }
            set { repo.labelName = value; }
        }

        /// <summary>
        /// Gets or sets the value of variable rowId.
        /// </summary>
        [TestVariable("41dceedb-67dd-4caf-b07a-64472d46ef90")]
        public string rowId
        {
            get { return repo.rowId; }
            set { repo.rowId = value; }
        }

        /// <summary>
        /// Gets or sets the value of variable iterationNumber.
        /// </summary>
        [TestVariable("a6eb9e63-5378-4ba0-adb2-5490b3d9ad6b")]
        public string iterationNumber
        {
            get { return repo.iterationNumber; }
            set { repo.iterationNumber = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            rowId = UserCodeCollection.SelectorByTextLabel.SelectInputField(repo.EBXApplication.MainTab.EbxSubSessioniFrame.LabelNameElelementInfo, "Received Date");
            Delay.Milliseconds(0);
            
            Date = UserCodeCollection.SelectorByTextLabel.GetDateFromThreeFields(repo.EBXApplication.MainTab.EbxSubSessioniFrame.CalendarFieldsInfo);
            Delay.Milliseconds(0);
            
            UserCodeCollection.DataValidationCodes.ValidateDateMatchCurrentDate(Date);
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
