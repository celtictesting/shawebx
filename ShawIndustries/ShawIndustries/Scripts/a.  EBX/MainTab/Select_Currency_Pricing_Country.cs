﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.MainTab
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Select_Currency_Pricing_Country recording.
    /// </summary>
    [TestModule("2865c351-1da4-4353-8aee-b1e7da225c82", ModuleType.Recording, 1)]
    public partial class Select_Currency_Pricing_Country : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static Select_Currency_Pricing_Country instance = new Select_Currency_Pricing_Country();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Select_Currency_Pricing_Country()
        {
            PriceCountry = "";
            PricingCountry = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Select_Currency_Pricing_Country Instance
        {
            get { return instance; }
        }

#region Variables

        /// <summary>
        /// Gets or sets the value of variable PriceCountry.
        /// </summary>
        [TestVariable("08a7643b-a206-4f08-ae42-1d574e3887aa")]
        public string PriceCountry
        {
            get { return repo.PriceCountry; }
            set { repo.PriceCountry = value; }
        }

        /// <summary>
        /// Gets or sets the value of variable PricingCountry.
        /// </summary>
        [TestVariable("2a39f8ba-2718-4cf0-8a45-1fe631bf26be")]
        public string PricingCountry
        {
            get { return repo.PricingCountry; }
            set { repo.PricingCountry = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            UserCodeMethod(repo.EBXApplication.MainTab.EbxSubSessioniFrame.PricingCountryFieldInfo);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.MainTab.EbxSubSessioniFrame.PricingCountryField' at Center.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.PricingCountryFieldInfo, new RecordItemIndex(1));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.PricingCountryField.Click();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{LControlKey down}{Akey}{LControlKey up}{Delete}'.", new RecordItemIndex(2));
            Keyboard.Press("{LControlKey down}{Akey}{LControlKey up}{Delete}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Down}' with focus on 'EBXApplication.MainTab.EbxSubSessioniFrame.PricingCountryField'.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.PricingCountryFieldInfo, new RecordItemIndex(3));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.PricingCountryField.PressKeys("{Down}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$PricingCountry' with focus on 'EBXApplication.MainTab.EbxSubSessioniFrame.PricingCountryField'.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.PricingCountryFieldInfo, new RecordItemIndex(4));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.PricingCountryField.PressKeys(PricingCountry);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 300ms.", new RecordItemIndex(5));
            Delay.Duration(300, false);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Enter}{Tab down}'.", new RecordItemIndex(6));
            Keyboard.Press("{Enter}{Tab down}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1s.", new RecordItemIndex(7));
            Delay.Duration(1000, false);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
