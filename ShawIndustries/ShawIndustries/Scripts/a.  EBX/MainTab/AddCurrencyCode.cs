﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.MainTab
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The AddCurrencyCode recording.
    /// </summary>
    [TestModule("7d20cb8c-7991-4ee9-af23-c8c55941c6a0", ModuleType.Recording, 1)]
    public partial class AddCurrencyCode : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static AddCurrencyCode instance = new AddCurrencyCode();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public AddCurrencyCode()
        {
            CurrencyCodeValue = "";
            PriceCountry = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static AddCurrencyCode Instance
        {
            get { return instance; }
        }

#region Variables

        /// <summary>
        /// Gets or sets the value of variable CurrencyCodeValue.
        /// </summary>
        [TestVariable("283f592d-35b3-4056-aa6e-0fb87a5262fa")]
        public string CurrencyCodeValue
        {
            get { return repo.CurrencyCodeValue; }
            set { repo.CurrencyCodeValue = value; }
        }

        /// <summary>
        /// Gets or sets the value of variable PriceCountry.
        /// </summary>
        [TestVariable("2039ea36-b0d3-4e03-8875-0d1b4f23f268")]
        public string PriceCountry
        {
            get { return repo.PriceCountry; }
            set { repo.PriceCountry = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Delay", "Waiting for 2s.", new RecordItemIndex(0));
            Delay.Duration(2000, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.MainTab.EbxSubSessioniFrame.CurrencyCodeIcon' at Center.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.CurrencyCodeIconInfo, new RecordItemIndex(1));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.CurrencyCodeIcon.Click();
            Delay.Milliseconds(0);
            
            UserCodeMethod(repo.EBXApplication.MainTab.EbxSubSessioniFrame.CurrencyCodeFieldInfo);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.MainTab.EbxSubSessioniFrame.CurrencyCodeDropdown' at Center.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.CurrencyCodeDropdownInfo, new RecordItemIndex(3));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.CurrencyCodeDropdown.Click();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Get Value", "Getting attribute 'TagValue' from item 'EBXApplication.MainTab.EbxSubSessioniFrame.CurrencyCodeField' and assigning its value to variable 'CurrencyCodeValue'.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.CurrencyCodeFieldInfo, new RecordItemIndex(4));
            CurrencyCodeValue = repo.EBXApplication.MainTab.EbxSubSessioniFrame.CurrencyCodeField.Element.GetAttributeValueText("TagValue");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "User", CurrencyCodeValue, new RecordItemIndex(5));
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
