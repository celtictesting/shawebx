﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.FinishTab
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The SelectFinishTabInternalCustomer recording.
    /// </summary>
    [TestModule("b198a40f-7475-4a7b-a6f0-c1a0a33a6dfc", ModuleType.Recording, 1)]
    public partial class SelectFinishTabInternalCustomer : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static SelectFinishTabInternalCustomer instance = new SelectFinishTabInternalCustomer();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public SelectFinishTabInternalCustomer()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static SelectFinishTabInternalCustomer Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.FinishTab.EbxSubSessioniFrame.FinishTab' at Center.", repo.EBXApplication.FinishTab.EbxSubSessioniFrame.FinishTabInfo, new RecordItemIndex(0));
            repo.EBXApplication.FinishTab.EbxSubSessioniFrame.FinishTab.Click();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.FinishTab.EbxSubSessioniFrame.EbxSmallIconInternal' at Center.", repo.EBXApplication.FinishTab.EbxSubSessioniFrame.EbxSmallIconInternalInfo, new RecordItemIndex(1));
            repo.EBXApplication.FinishTab.EbxSubSessioniFrame.EbxSmallIconInternal.Click();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'EBXApplication.FinishTab.EbxSubSessioniFrame.SellingCompanyIsMandatory'.", repo.EBXApplication.FinishTab.EbxSubSessioniFrame.SellingCompanyIsMandatoryInfo, new RecordItemIndex(2));
            Validate.Exists(repo.EBXApplication.FinishTab.EbxSubSessioniFrame.SellingCompanyIsMandatoryInfo);
            Delay.Milliseconds(0);
            
            //Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'EBXApplication.FinishTab.EbxSubSessioniFrame.FieldTerritoryIsMandatory'.", repo.EBXApplication.FinishTab.EbxSubSessioniFrame.FieldTerritoryIsMandatoryInfo, new RecordItemIndex(3));
            //Validate.Exists(repo.EBXApplication.FinishTab.EbxSubSessioniFrame.FieldTerritoryIsMandatoryInfo);
            //Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
