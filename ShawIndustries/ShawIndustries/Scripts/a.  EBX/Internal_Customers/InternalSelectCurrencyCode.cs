﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace ShawIndustries.Scripts.a.@__EBX.Internal_Customers
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The InternalSelectCurrencyCode recording.
    /// </summary>
    [TestModule("70bf8f33-1474-4d71-b55c-6ead1952ec17", ModuleType.Recording, 1)]
    public partial class InternalSelectCurrencyCode : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::ShawIndustries.ShawIndustriesRepository repository.
        /// </summary>
        public static global::ShawIndustries.ShawIndustriesRepository repo = global::ShawIndustries.ShawIndustriesRepository.Instance;

        static InternalSelectCurrencyCode instance = new InternalSelectCurrencyCode();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public InternalSelectCurrencyCode()
        {
            CurrencyCodeValue = "";
            PricingCountry = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static InternalSelectCurrencyCode Instance
        {
            get { return instance; }
        }

#region Variables

        /// <summary>
        /// Gets or sets the value of variable CurrencyCodeValue.
        /// </summary>
        [TestVariable("9f950f25-60ea-4265-b5f9-2a4de52575a1")]
        public string CurrencyCodeValue
        {
            get { return repo.CurrencyCodeValue; }
            set { repo.CurrencyCodeValue = value; }
        }

        /// <summary>
        /// Gets or sets the value of variable PricingCountry.
        /// </summary>
        [TestVariable("fc778d6d-4f82-4e3c-a6fb-46c43323453d")]
        public string PricingCountry
        {
            get { return repo.PricingCountry; }
            set { repo.PricingCountry = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Down}' with focus on 'EBXApplication'.", repo.EBXApplication.SelfInfo, new RecordItemIndex(0));
            repo.EBXApplication.Self.PressKeys("{Down}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{LControlKey down}{Akey}{LControlKey up}{Delete}'.", new RecordItemIndex(1));
            Keyboard.Press("{LControlKey down}{Akey}{LControlKey up}{Delete}");
            Delay.Milliseconds(0);
            
            UserCodeMethod(repo.EBXApplication.MainTab.EbxSubSessioniFrame.CurrencyCodeFieldInfo);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'EBXApplication.MainTab.EbxSubSessioniFrame.InternalCurrencyCodeDropdown' at Center.", repo.EBXApplication.MainTab.EbxSubSessioniFrame.InternalCurrencyCodeDropdownInfo, new RecordItemIndex(3));
            repo.EBXApplication.MainTab.EbxSubSessioniFrame.InternalCurrencyCodeDropdown.Click();
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
